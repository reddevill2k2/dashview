const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const dotenv = require('dotenv');
const env = dotenv.config().parsed;
const webpack = require('webpack');

module.exports = {
    mode: process.env.NODE_ENV ? process.env.NODE_ENV : 'development', // "production" | "development" | "none",
    entry: {
        bundle: './src/index.tsx',
    },
    output: {
        path: path.resolve(__dirname, './build'),
        filename: '[contenthash]-[name].js',
        publicPath: '/'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html',
            favicon: './src/assets/favicon.ico',
            hash: true,
            inject: true,
        }),
        new MiniCssExtractPlugin({
            filename: 'style.css',
            chunkFilename: '[contenthash]-[name].css',
        }),
        new webpack.ProvidePlugin({
            Buffer: ['buffer', 'Buffer'],
            process: 'process/browser',
            $: 'jquery',
            jQuery: 'jquery',
            'window.$': 'jquery',
            'window.$jquery': 'jquery',
            '_': 'lodash',
        }),
        new webpack.EnvironmentPlugin(['NODE_ENV']),
        new CleanWebpackPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
        }),
    ],
    optimization: {
        splitChunks: {
            chunks: 'all',
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.s?css$/,
                    chunks: 'all',
                    minChunks: 1,
                    reuseExistingChunk: true,
                    enforce: true,
                },
            },
        },
        removeAvailableModules: false,
        removeEmptyChunks: false,
    },
    devServer: {
        port: process.env.PORT, // default 1900
        open: true,
        historyApiFallback: true,
        compress: true,
        static: './',
        hot: true,
        client: {
            logging: 'none'
        }
    },
    devtool: process.env.NODE_ENV === 'production' ? false : 'eval',
    resolve: {
        extensions: ['.js','.jsx','.tsx','*','.scss','.css'],
        modules: ['node_modules'],
        alias: {
            '@components': path.resolve(__dirname, './src/components'),
            '@actions': path.resolve(__dirname, './src/actions'),
            '@apis': path.resolve(__dirname, './src/apis'),
            '@assets': path.resolve(__dirname, './src/assets'),
            '@commons': path.resolve(__dirname, './src/commons'),
            '@constants': path.resolve(__dirname, './src/constants'),
            '@containers': path.resolve(__dirname, './src/containers'),
            '@helpers': path.resolve(__dirname, './src/helpers'),
            '@hook': path.resolve(__dirname, './src/hook'),
            '@reducers': path.resolve(__dirname, './src/reducers'),
            '@redux': path.resolve(__dirname, './src/redux'),
            '@sagas': path.resolve(__dirname, './src/sagas'),
            '@routes': path.resolve(__dirname, './src/routes'),
            '@utils': path.resolve(__dirname, './src/utils'),
        },
    },
    module:{
        rules:[
            {
                test: /\.(ts|tsx)?$/,
                exclude: /node_modules/,
                use:[
                    {
                        loader:'babel-loader',
                        options: {
                            'presets': [
                                ['env', {modules: false}]
                            ],
                        }
                    }
                ]
            },
            {
                test: /\.(ts|tsx)?$/,
                use: {
                    loader: 'ts-loader',
                    options: {
                        transpileOnly: true,
                    },
                },
                exclude: /node_modules/,
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                    }
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            },
            {
                loader: 'file-loader',
                test: /\.svg$|\.woff$|\.woff2$|\.eot$|\.ttf$|\.wav$|\.mp3$|\.ico$/,
                options: {
                    outputPath: 'assets/fonts',
                },
                exclude: /node_modules/, 
            },
            {
                test: /\.s[ac]ss$|\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
        ]
    },
};