import React from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid, IconButton } from '@mui/material';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import imageProduct1 from '@assets/images/img_product_1.png';
import { imageOverviewUser3, imageProduct2, imageProduct3, imageProduct4, iconMessage } from '@constants/imageAssets';
import _ from 'lodash';
import NumberFormat from 'react-number-format';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import imageOverviewUser1 from '@assets/images/img_overview_user_1.png';
import imageOverviewUser2 from '@assets/images/img_overview_user_2.png';
import iconHeartLight from '@assets/icons/heart/light.svg';
import iconLinkLight from '@assets/icons/link/light.svg';
import {v4 as uuidv4} from 'uuid';
import IconSVG from '@helpers/IconSVG';

interface IComment {
    title: string,
    handleChangeSelectTabOverview?: ()=> void,
    selectTabOverView?: number,
}

function PopularElement(props: IComment & WithStyles<typeof styles>) {
    const { classes, title, ...otherProps} = props;

    const listPopularDummyData = [
        {
            id: 1,
            image: imageOverviewUser1,
            username: '@ethel',
            name: 'ethel',
            message: 'Gate work 😊',
            time: '0.5h',
            category: 'Smiles – 3D icons',
        },
        {
            id: 2,
            image: imageOverviewUser2,
            username: '@jaz.designer',
            name: 'Jazmyn',
            message: 'Simple work',
            time: '4h',
            category: 'Smiles – 3D icons',
        },
        {
            id: 3,
            image: imageOverviewUser3,
            username: '@ethel',
            name: 'ethel',
            message: 'How can I buy only the design?',
            time: '8h',
            category: 'Fleet - Travel shopping',
        },
    ];
    return (
        <Box
            component={'div'}
            className={classes.commentPage}
        >
            <TitleElement bg={COLORS.orangeLight}>
                {title}
            </TitleElement>
            <Box
                className={classes.listComments}
            >
                <Box className="render-list">
                    {!_.isEmpty(listPopularDummyData) && listPopularDummyData?.map((comment: {
                        id: number,
                        image: string,
                        username: string,
                        name: string,
                        message: string,
                        time: string,
                        category: string,
                    }, index: number) => (
                        <React.Fragment  key={uuidv4()}>
                            <Box
                                display={'flex'}
                                alignItems={'start'}
                                className='item-comment'>
                                <div className="icon-user">
                                    <img src={comment.image}
                                        alt="" />
                                </div>
                                <a href=''
                                    onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> e.preventDefault()}
                                    className="info-user">
                                    <Box component={'div'}
                                        width="100%">
                                        <Box display='flex'
                                            justifyContent='space-between'>
                                            <Box display={'flex'}
                                                component={'div'}>
                                                <b>{comment.name}</b>
                                                {comment.username}
                                            </Box>
                                            <Text caption1
                                                color={COLORS.shades75}>{comment.time}</Text>
                                        </Box>
                                        <Box display={'flex'}
                                            component={'div'}>
                                            <div className='on'>On</div>
                                            <b>{comment.category}</b>
                                        </Box>
                                        <Box>
                                            {comment.message}
                                        </Box>
                                    </Box>
                                    <Box component={'div'}
                                        className={classes.groupReaction}>
                                        <IconButton className='reaction-icon'>
                                            <IconSVG  icon={iconMessage}
                                                fill={COLORS.grey4}
                                                small
                                                title="" />
                                        </IconButton>
                                        <IconButton className='reaction-icon'>
                                            <IconSVG icon={iconHeartLight}
                                                fill={COLORS.grey4}
                                                small
                                                title="" />
                                        </IconButton>
                                        <IconButton className='reaction-icon'>
                                            <IconSVG icon={iconLinkLight}
                                                fill={COLORS.grey4}
                                                small
                                                title="" />
                                        </IconButton>
                                    </Box>
                                </a>
                            </Box>
                            <Divider className='divider'/>
                        </React.Fragment>
                    ))}
                    <Box className={classes.buttonAll}>
                        <ButtonTransparent>
                      View All
                        </ButtonTransparent>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(PopularElement);