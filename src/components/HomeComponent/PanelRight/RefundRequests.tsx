import React from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid, IconButton } from '@mui/material';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import imageProduct1 from '@assets/images/img_product_1.png';
import { iconBasketLight, imageProduct2, imageProduct3, imageProduct4 } from '@constants/imageAssets';
import _ from 'lodash';
import NumberFormat from 'react-number-format';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { v4 as uuidv4 } from 'uuid';
import IconSVG from '@helpers/IconSVG';

interface IRefundRequests {
    title: string,
}

function RefundRequests(props: IRefundRequests & WithStyles<typeof styles>) {
    const { classes, title,...otherProps} = props;

   
    return (
        <Box
            component={'div'}
            className={classes.refundRequestPage}
        >
            <TitleElement bg={COLORS.orangeLight}>
                {title}
            </TitleElement>
            <Box display={'flex'}
                className='content'>
                <IconButton className='icon-basket'>
                    <IconSVG icon={iconBasketLight}/>
                </IconButton>
                <Text bodyM1
                    color={COLORS.grey4}>
              You have <b>52 open refund requests</b> to action. This includes <b>8 new requests.</b> 👀
                </Text>
            </Box>
            <ButtonTransparent className={classes.buttonTransparent}>
            Review refund requests
            </ButtonTransparent>
        </Box>
    );
}

export default withStyles(styles)(RefundRequests);