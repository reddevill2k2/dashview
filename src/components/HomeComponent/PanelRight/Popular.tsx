import React from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid } from '@mui/material';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import imageProduct1 from '@assets/images/img_product_1.png';
import { imageProduct2, imageProduct3, imageProduct4 } from '@constants/imageAssets';
import _ from 'lodash';
import NumberFormat from 'react-number-format';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { v4 as uuidv4 } from 'uuid';

interface IPopular {
    handleChangeSelectTabOverview?: ()=> void,
    selectTabOverView?: number,
    title: string,
}

function PopularElement(props: IPopular & WithStyles<typeof styles>) {
    const { classes, title,...otherProps} = props;

    const listProductDummyData = [
        {
            id: 1,
            image: imageProduct1,
            title: 'Crypter - NFT UI kit',
            price: 259011,
            status: 'Active',
        },
        {
            id: 2,
            image: imageProduct2,
            title: 'Bento Matte 3D illustration 1.0',
            price: 5901,
            status: 'Deactive',
        },
        {
            id: 3,
            image: imageProduct3,
            title: 'Excellent material 3D chair',
            price: 1200321,
            status: 'Active',
        },
        {
            id: 4,
            image: imageProduct4,
            title: 'Fleet - travel shopping kit',
            price: 80001,
            status: 'Deactive',
        },
    ];
    return (
        <Box
            component={'div'}
            className={classes.popularPage}
        >
            <TitleElement bg={COLORS.blueLight}>
                {title}
            </TitleElement>
            <Box
                className={classes.listProducts}
            >
                <Box className="box-title">
                    <Text caption1
                        color={COLORS.grey4}>Products</Text>
                    <Text caption1
                        color={COLORS.grey4}>Earning</Text>
                </Box>
                <Divider/>
                <Box className="render-list">
                    {!_.isEmpty(listProductDummyData) && listProductDummyData?.map((product: {
                        id: number,
                        title: string,
                        price: number,
                        status: string,
                        image: string,
                    }, index: number) => (
                        <a href=''
                            onClick={(e: any)=> e.preventDefault()}
                            key={uuidv4()}
                            className='product'>
                            <Box className={clsx(classes.dFlex, 'product-start')}>
                                <Box className="box-image">
                                    <img src={product.image}
                                        alt={product.title} />
                                </Box>
                                <Text base1
                                    className='title'
                                    color={COLORS.grey7}>{product.title}</Text>
                            </Box>
                            <Box className='price'>
                                <Text className="quanity"
                                    baseB1
                                    color={COLORS.grey7}>
                                   
                                    <NumberFormat
                                        thousandsGroupStyle="thousand"
                                        value= {product.price}
                                        prefix="$"
                                        decimalSeparator="."
                                        displayType="text"
                                        type="text"
                                        thousandSeparator={true}
                                        allowNegative={true} />
                                </Text>
                                <Box className={clsx(product.status === 'Active'
                                    ? classes.buttonActive : classes.buttonDeActive)}>
                                    <Text className="status"
                                        caption2
                                        color={COLORS.grey7}>
                                        {product.status}
                                    </Text>
                                </Box>
                            </Box>
                        </a>
                    ))}
                    <Box className={classes.buttonAll}>
                        <ButtonTransparent>
                      All Products
                        </ButtonTransparent>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(PopularElement);