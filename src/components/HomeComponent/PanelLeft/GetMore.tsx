import React from 'react';
import { WithStyles, withStyles } from '@mui/styles';

import styles from './styles';
import { Box, Grid, colors } from '@mui/material';
import TitleElement from '@helpers/TitleElement';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { iconFacebook, iconInstagram } from '@constants/imageAssets';
import iconTwitter from '@assets/icons/twitter/filled.svg';
import IconSVG from '@helpers/IconSVG';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';

interface IProoTips {
    title: string;
}

function GetMoreElement(props: IProoTips & WithStyles<typeof styles>) {
    const { classes, title, ...otherProps } = props;

    const socialNetworks = [
        {
            title: 'facebook',
            icon: iconFacebook,
            url: 'https://facebook.com/'
        },
        {
            title: 'twitter',
            icon: iconTwitter,
            url: 'https://twitter.com/'
        },
        {
            title: 'instagram',
            icon: iconInstagram,
            url: 'https://www.instagram.com/'
        },
    ];

    return (
        <Box className={classes.getMorePage}>
            <TitleElement bg={COLORS.blueLight}>
                {title}
            </TitleElement>
            <Box className="content">
                <Text bodyM1
                    color={COLORS.grey4}>
            50% of new customers explore products because the author sharing 
            the work on the social media network. Gain your earnings right now! 🔥
                </Text>
            </Box>
            <Grid container
                spacing={3}
                className={'social-list'}
                justifyContent={'space-between'}>
                {!_.isEmpty(socialNetworks) && socialNetworks.map((item: {icon: any, url: string, title: string})=> {
                    return (
                        <Grid item
                            key={uuidv4()}
                            md={4}
                            xs={12}>
                            <ButtonTransparent isElement
                                className='social-button'>
                                <IconSVG width={24}
                                    height={24}
                                    icon={item.icon}/>
                                <Text button1
                                    color={COLORS.grey7}>
                                    {item.title}
                                </Text>
                            </ButtonTransparent>
                        </Grid>
                    );
                })}
                
            </Grid>
        </Box>
    );
}

export default withStyles(styles)(GetMoreElement);
