import React from 'react';
import { COLORS } from '@constants/colors';
import {
    chartLineOverview,
    iconActivityFilled,
    iconArrowForward,
    iconInfo,
    iconShoppingBag,
} from '@constants/imageAssets';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Page from '@helpers/Page';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, CardMedia, Grid, IconButton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    Title,
    Tooltip,
    Legend,
    PointElement,
    LineElement,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import {faker} from '@faker-js/faker';

import styles from './styles';
import { formatMoney } from '../../../utils/index';
import TooltipCustom from '@helpers/TooltipCustom';

interface IOverview {
    title: string;
    overviewUserDummyData?: { id: number; name: string; url: string }[];
    selectTabOverView?: number;
    handleChangeSelectTabOverview?: (value: number) => void ;
}

function OverviewElement(props: IOverview & WithStyles<typeof styles>) {
    const {
        title,
        classes,
        handleChangeSelectTabOverview,
        selectTabOverView,
        overviewUserDummyData,
        ...otherProps
    } = props;
    const dataSelectOverView = [
        {
            label: 'All time',
            value: 'All time',
        },
        {
            label: 'In a year',
            value: 'In a year',
        },
        {
            label: 'Per month',
            value: 'Per month',
        },
    ];

    ChartJS.register(
        CategoryScale,
        LinearScale,
        PointElement,
        LineElement,
        Title,
        Tooltip,
        Legend,
    );

    const labels = ['Apr', 'May', 'Jun', 'July', 'Aug', 'Sep'];

    const data = React.useMemo(()=> ({
        labels,
        datasets: [
            {
                label: 'Earning',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 2000 })),
                lineTension: 0.4,
                borderColor: COLORS.blue,
                borderWidth: 4,
                pointRadius: 0,
            },
        ],
    }), []);

    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false,
            },
            crosshair: false,
        },
        scales: {
            y: {
                grid: {
                    display: true,
                },
                ticks: {
                    stepSize: 500,
                    callback: (label:any, index: any, labels:any) => {
                        return formatMoney.format(label);
                    }
                }
            },
            x: {
                grid: {
                    display: false,
                }
            }
        }
    };

    return (
        <Box className={classes.overviewPage}
            component="div">
            <Grid className={classes.boxHeader}
                container>
                <Grid item>
                    <TitleElement bg={COLORS.orangeLight}
                        color={COLORS.grey7}>
            Overview
                    </TitleElement>
                </Grid>
                <Grid component="div"
                    item>
                    <SelectCustom data={dataSelectOverView}
                        defaultValues="All time" />
                </Grid>
            </Grid>
            <Grid className={classes.boxBody}
                container>
                <Grid
                    item
                    md={6}
                    xs={12}
                    className={clsx('box-tab', { active: selectTabOverView === 1 })}
                    onClick={() => handleChangeSelectTabOverview && handleChangeSelectTabOverview(1)}
                >
                    <Box className={classes.dFlex}>
                        <Box className="box-image tab-1">
                            <img src={iconShoppingBag} />
                        </Box>
                        <Box className="content">
                            <Box className="box-subtitle">
                                <Text caption1
                                    color={COLORS.grey4}>
                  Customers
                                </Text>
                                <TooltipCustom title={'small description'}>
                                    <img alt=""
                                        src={iconInfo} />
                                </TooltipCustom>
                            </Box>
                            <Text color={COLORS.grey7}
                                h2>
                1024
                            </Text>
                        </Box>
                    </Box>
                    <Box className="timeline down"
                        component="div">
                        <Text caption2>37.8%</Text>
                    </Box>
                </Grid>
                <Grid
                    className={clsx('box-tab', { active: selectTabOverView === 2 })}
                    item
                    md={6}
                    onClick={()=> handleChangeSelectTabOverview && handleChangeSelectTabOverview(2)}
                    xs={12}
                >
                    <Box className={classes.dFlex}>
                        <Box className="box-image tab-2">
                            <img src={iconActivityFilled} />
                        </Box>
                        <Box className="content">
                            <Box className="box-subtitle">
                                <Text caption1
                                    color={COLORS.grey4}>
                  Incom
                                </Text>
                                <TooltipCustom title={'small description'}>
                                    <img alt=""
                                        src={iconInfo} />
                                </TooltipCustom>
                            </Box>
                            <Text color={COLORS.grey7}
                                h2>
                256k
                            </Text>
                        </Box>
                    </Box>
                    <Box className="timeline up"
                        component="div">
                        <Text caption2>12.8%</Text>
                    </Box>
                </Grid>
            </Grid>
            {selectTabOverView === 1 && (
                <Box className={classes.contentTabLeft}
                    component="div">
                    <Box className="box-message">
                        <Text className="text">
              Welcome <b>857 customers</b> with a personal message 😎
                        </Text>
                        <ButtonTransparent>Send message</ButtonTransparent>
                    </Box>
                    <Grid className="box-users"
                        container>
                        {!_.isEmpty(overviewUserDummyData) &&
              overviewUserDummyData?.map(
                  (
                      item: {
                          id: number;
                          name: string;
                          url: string;
                      },
                      index: number
                  ) => (
                      <Grid key={uuidv4()}
                          className="user"
                          item
                          md={3}
                          xs={6}>
                          <Box>
                              <img alt=""
                                  src={item.url} />
                              <Text caption1
                                  center
                                  color={COLORS.grey6}>
                                  {item.name}
                              </Text>
                          </Box>
                      </Grid>
                  )
              )}
                        <Grid className="view-all"
                            item
                            md={3}
                            xs={6}>
                            <IconButton className="box-arrow">
                                <img alt=""
                                    src={iconArrowForward} />
                            </IconButton>
                            <Text
                                caption1
                                center
                                className="view-all-text"
                                color={COLORS.grey6}
                            >
                View all
                            </Text>
                        </Grid>
                    </Grid>
                </Box>
            )}
            {selectTabOverView === 2 && (
                <Box className={classes.contentTabRight}
                    component="div">
                    <Line options={options}
                        data={data} />
                </Box>
            )}
        </Box>
    );
}

export default withStyles(styles)(OverviewElement);
