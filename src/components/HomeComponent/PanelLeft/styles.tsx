import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    dFlex: {
        display: 'flex',
    },
    contentTabLeft: {
        height: 250,
        '& .box-message': {
            marginBottom: 24,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            '& .text': {
                fontSize: 15,
                color: COLORS.grey7,
                maxWidth: 250,
            },
            '& .button': {
                cursor: 'pointer',
                padding: '12px 20px',
                borderRadius: 12,
                border: `2px solid ${COLORS.grey3}`,
                '& .button-text': {
                    fontWeight: 700,
                    fontSize: 15,
                    color: COLORS.grey7
                }
            }
        },
        '& .box-users': {
            padding: '32px 0',
            display: 'flex',
            '& .user': {
                display: 'flex',
                justifyContent: 'center',
                '& img': {
                    width: 64,
                    height: 64,
                    display: 'inline-block',
                    marginBottom: 12,
                    transition: 'all .3s ease',
                    cursor: 'pointer',
                    '&:hover': {
                        transform: 'scale(1.05)'
                    },
                }
            },
            '& .view-all': {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                '& .box-arrow': {
                    backgroundColor: COLORS.grey3,
                    width: 64,
                    height: 64,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: '50%',
                    transition: 'all .3s ease',
                    cursor: 'pointer',
                    '&:hover': {
                        backgroundColor: '#dfdbdb',
                    },
                    '& img': {
                        width: 24,
                        height: 24,
                        
                    }
                },
                '&-text': {
                    marginTop: 12,
                }
                
            }
        }
    },
    panelLeft: {},
    panelRight: {},
    boxTitle: {
        display: 'flex',
        alignItems: 'center',
    },
    overviewPage: {
        overflow: 'hidden',

        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: 8,
        // boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    },
    boxHeader: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    boxBody: {
        margin: '32px 0',
        borderRadius: 20,
        backgroundColor: COLORS.grey2,
        padding: 12,
        display: 'flex',
        '& .box-tab': {
            display: 'flex',
            justifyContent: 'space-between',
            borderRadius: 12,
            padding: 20,
            cursor: 'pointer',
            transition: 'all .4s ease',
            backgroundColor: 'transparent',
            '&.active': {
                backgroundColor: COLORS.grey1,
            }
        },
        '& .box-image': {
            width: 40,
            height: 40,
            borderRadius: 40,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 16,
            '&.tab-1': {
                backgroundColor: COLORS.violetLight,
            },
            '&.tab-2': {
                backgroundColor: COLORS.blueLight,
            },
            '& img': {
                width: 24,
                height: 24,
            }
        },
        '& .content': {
            
            '& .box-subtitle': {
                display: 'flex',
                alignItems: 'center',
                '& img': {
                    width: 13,
                    height: 13,
                    display:'inline-block',
                    marginLeft: 5,
                }
            }
        },
        '& .timeline': {
            padding: 5,
            borderRadius: 8,
            backgroundColor: 'transparent',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 'fit-content',
            height: 'fit-content',
            '&.down': {
                backgroundColor: 'rgba(255, 231, 228, 1)',
                color: COLORS.red,
            },
            '&.up': {
                backgroundColor: 'rgba(252, 252, 252, 1)',
                color: COLORS.green,
            },
        }
    },
    boxSelect: {
        borderRadius: 12,
        border: `2px solid ${COLORS.grey3}`,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        padding: 8,
        outlineOffset: -2,
        borderImage: 'none',
        outline: 'transparent',
        color: COLORS.grey4,
        fontWeight: 600,
        fontSize: 14,
    },
    rock: {
        borderRadius: 4,
        width: 16,
        height: 32,
        background: COLORS.orangeLight,
        marginRight: 16,
    },
    productViews: {
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: 8,
        marginTop: 8,
        // boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        '& .render-chart': {
            height: 320,
            marginTop: 32,
            '& img': {
                width: '100%',
                objectFit: 'cover',
            }
        }
    },
    contentTabRight: {
        height: 250,
    },
    chartLineImage: {
        objectFit: 'contain',
        width: '100%',
        height: 'auto',
        marginTop: 32,
    },
    propTipsPage: {
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: 8,
        // boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        marginTop: 8,
        marginBottom: 8,
        '& .box-title': {

        },
        '& .desc-title': {
            marginTop: 32,
        }
        , '& .list-tips': {
            marginTop: 48,
        },
        '& .box-icon': {
            width: 64,
            height: 64,
            borderRadius: 64,
            marginRight: 12,
            alignItems: 'center',
            justifyContent: 'center',
            border: `2px solid ${COLORS.grey3}`,
            '& img': {
                width: 20,
                height: 20,
                objectFit: 'contain',
            }
        },
        '& .info-title': {
            marginBottom: 8,
        },
        '& .info-tip': {
           
            '& .info-label': {
                padding: '4px 8px',
                borderRadius: 4,
                marginRight: 4,
                color: COLORS.grey7,
                fontSize: 12,
                lineHeight: '16px',
                fontWeight: 'bold',
                cursor: 'pointer',
            },
            '& .info-time': {
                border: `1px solid ${COLORS.grey3}`,
                height: 24,
                alignItems: 'center',
                padding: '2px 4px',
                borderRadius: 4,
                '& img': {
                    width: 20,
                    height: 20,
                    marginRight: 4,
                    objectFit: 'contain',
                }
            }
        }
    },
    boxRenderItem: {
        marginBottom: '32px !important',
        alignItems: 'center !important',
    },
    getMorePage: {
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: 8,
        // boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        marginTop: 8,
        marginBottom: 8,
        '& .content': {
            marginTop: 32,
        },
        '& .social-list': {
            marginTop: 12,
            '& .social-button': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: COLORS.grey4,
                '& .icon-svg': {
                    marginRight: 4,
                  
                }
            },
        }
    }
});

export default styles;