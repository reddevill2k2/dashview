import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
    
    },
    ProductPage:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: '24px 24px 2px',
        marginBottom: 8,
        width: '100%',
        '& .products-tabs': {
            '& .button-tab': {
                height: 40,
                borderRadius: '4px',
                padding: '8px !important',
                backgroundColor: 'transparent',
                color: COLORS.grey4,
                transition: 'all .3s ease',
                '& .text': {
                    color: `${COLORS.grey4} !important`,
                },
                '&.active': {
                    color: COLORS.grey7,
                    backgroundColor: COLORS.white,
                    boxShadow:`0px 4px 8px -4px ${COLORS.shades40}`,
                    '& .icon-svg': {
                        color: `${COLORS.grey7} !important`,
                    }
                }
            },
        },
        '& .table-products':{
            marginTop: 32,

            '& .products-title:hover': {
                color:`${COLORS.blue} !important`,
            },
            '& .products-image':{
                marginRight: 20,
            },
            '& .box-icon':{
                opacity: 0,
                visibility: 'hidden',
                transition: 'all .3s ease',
                '& *~*':{
                    marginLeft: 16,
                },
                '& .icon':{
                    transition: 'all .3s ease',
                    '&:hover':{
                        backgroundColor: COLORS.white,
                        fill: COLORS.blue,
                    },
                    '&:hover .icon-svg': {
                        color: COLORS.blue,
                    },
                }
            },
            '& tbody > tr:hover .box-icon':{
                opacity: 1,
                visibility: 'visible',
            }
        },
    },

    MessageComponent:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: '24px 24px 20px',
        margin: '32px 0 -40px',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& .content':{
            display: 'flex',
            alignItems: 'center',
        },
        '& .social-button':{
            marginRight:'16px',
            '&:hover':{
                backgroundColor: COLORS.red,
                border:`2px solid ${COLORS.red}`,
            },
            '&:hover .button-text':{
                color: `${COLORS.white} !important`,  
            },
            '&:hover .icon-svg':{
                color: `${COLORS.white} !important`,

            }
        }
    },
});

export default styles;