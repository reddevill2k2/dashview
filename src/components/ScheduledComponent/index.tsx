import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import Products from './Products';
import Message from './Message';

interface ScheduledProps {
    title: string,
    dataTableProducts: any,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    searchProduct: string,
    classes:any,

}

function ScheduledComponent(props: ScheduledProps) {
    const {title, classes, ...otherProps} = props;
    return (
        <Page title={title} 
            {...otherProps}>
            <Products title={'Products'}
                {...otherProps} />
            <Message {...otherProps} />
        </Page>
    );
}

export default withStyles(styles)(ScheduledComponent);