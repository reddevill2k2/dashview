import React, { useMemo } from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid, IconButton } from '@mui/material';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import Button from '@helpers/Button';
import { v4 as uuidv4 } from 'uuid';
import TableCustom from '@helpers/TableCustom';
import { Column } from 'react-table';
import LabelColor from '@helpers/LabelColor';
import { iconListFilled, iconGridLight, iconCalendar, iconEdit, iconTrash, iconClock } from '@constants/imageAssets';
import { truncateString } from './../../utils/index';

interface IProductList {
    title: string,
    tabActiveProduct: number,
    dataTableProducts: any,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    searchProduct: string,
    classes:any,
}

function ProductList(props: IProductList & WithStyles<typeof styles>) {
    const { classes, title,
        tabActiveProduct, 
        dataTableProducts,
        handleSearchProduct, 
        searchProduct,
        ...otherProps
    } = props;

    const columnsProducts: Column[] = useMemo(()=> [
        {
            Header: 'Product',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        alignItems={'center'}
                        sx={{cursor:'pointer'}}>
                        <Box className="products-image">
                            <img src={original.products.previewsm}
                                alt={original.products.title}
                                width={80}
                                height={80} />
                        </Box>
                        <Box className="products-content info"
                            sx={{maxWidth:180}}>
                            <Text color={COLORS.grey7}
                                className="products-title"
                                baseB1>
                                {truncateString(original.products.title,32)}
                            </Text>
                            <Text color={COLORS.grey4}
                                caption1>
                                {truncateString(original.products.link,25)}
                            </Text>
                        </Box>
                    </Box>
                );
            },
        },
        {
            Header: 'Price',
            accessor: 'price',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={`$${original.price}`}
                    bg={original.price > 0 ? COLORS.greenNgoc : COLORS.grey3} />);
            }
        },
        {
            Header: 'Scheduled for',
            minWidth: 300,
            width: 'max-content',
            accessor: 'scheduledFor',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box component={'div'}
                        className="scheduled-for"
                        sx={{display: 'flex', alignItems:'center',justifyContent: 'space-between'}} >

                        <Text color={COLORS.grey4}
                            caption1>
                            {original.scheduledFor}
                        </Text>
                        <Box component={'div'}
                            className="box-icon"
                            sx={{display:'flex',alignItems:'center'}}>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                            >
                                <IconSVG small
                                    icon={iconCalendar}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                            >
                                <IconSVG small
                                    icon={iconEdit}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                            >
                                <IconSVG small
                                    icon={iconTrash}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                        </Box>
                    </Box>
                );
            }
        },
     
     
    ], []);

    const renderTabProducts = () => {
        return (
            <TableCustom className='table-products'
                data={dataTableProducts}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                columns={columnsProducts}/>
        );
    };

    return (
        <Box
            component={'div'}
            className={classes.ProductPage}
        >
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <Box className="content-left"
                    display={'flex'}
                    alignItems={'center'}>

                    <TitleElement bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                    <Input placeholder='Search product'
                        style={{marginLeft:24}}
                        search={searchProduct}
                        handleSearch={handleSearchProduct}
                    />
                </Box>
            </Box>
            {
                renderTabProducts()
            }
        </Box>
    );
}

export default withStyles(styles)(ProductList);