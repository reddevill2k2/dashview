import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import Message from './Message';
import Products from './Products';

interface ReladsedProps {
    classes:any,
    title: string,
    titlePage?: string,
    tabActiveProduct: number,
    dataTableProducts: any,
    handleChangeTabActiveProduct: (index: number) =>void,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleChangePaginationProducts: ()=> void,
    handleSelectRows: (rows:any) => void,
    handleDeleteProducts: (selectedRow:any) => void,
    handlePublishProducts:(selectedRow:any) => void,
    searchProduct: string,
    selectedRow:any,
    pagination: number,
    isLoadingMore:boolean,

}

function ReleasedComponent(props: ReladsedProps) {
    const {title, titlePage,classes,  ...otherProps} = props;
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <Products title={'Products'}
                {...otherProps} />
            <Message title={title}
                {...otherProps} />
        </Page>
    );
}

export default withStyles(styles)(ReleasedComponent);