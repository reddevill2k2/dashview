import React from 'react';
import {withStyles,WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, IconButton } from '@mui/material';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { iconCheckAll, iconTrash } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import Button from '@helpers/Button';
import _ from 'lodash';

interface MessageProps {
    title: string,
    classes?: any,
    tabActiveProduct: number,
    selectedRow: any,
    handleDeleteProducts: (selectedRow:any) => void,
    handlePublishProducts:(selectedRow:any) => void,

}

function MessageComponent(props: MessageProps & WithStyles<typeof styles>) {
    const {
        title, 
        classes, 
        selectedRow,
        handleDeleteProducts,
        handlePublishProducts, 
        ...otherProps
    } = props;
    
    return (
        <React.Fragment>
            <Box className={classes.MessageComponent + (_.size(selectedRow) > 0 ? ' active' :'')} >
                <Box component={'div'}
                    className='content'>
                    <IconButton className='icon-basket'>
                        <IconSVG icon={iconCheckAll}/>
                    </IconButton>
                    <Text caption1
                        color={COLORS.grey4}>
                        <b> {_.size(selectedRow)} products </b>selected
                    </Text>
                </Box>
                <Box sx={{display: 'flex', alignItems:'center'}}>
                    <ButtonTransparent isElement
                        onClick ={() =>{
                            handleDeleteProducts(selectedRow);
                        }}
                        className='social-button'>
                        <Text button1
                            className='button-text'
                            color={COLORS.red}
                            sx={{marginRight:'4px'}}>
                        Deleted
                        </Text>
                        <IconSVG width={24}
                            height={24}
                            icon={iconTrash}
                            fill={COLORS.red}/>
                    </ButtonTransparent>
                    <Button 
                        onClick ={() => {
                            handlePublishProducts(selectedRow);}}
                        color={'primary'}
                    >Publish</Button>
                </Box>
            </Box>
        </React.Fragment>

    );
}

export default withStyles(styles)(MessageComponent);