import { COLORS } from '@constants/colors';
import { iconArrowUpDown, iconMessages, iconProfile,iconMouse,iconInfoFilled } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import LabelColor from '@helpers/LabelColor';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import TooltipCustom from '@helpers/TooltipCustom';
import { Box, Divider, Grid } from '@mui/material';
import { withStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';
import { v4 as uuidv4 } from 'uuid';

interface Insights {
    title: string;
    phone?: number; // optional
    classes?: any;
}

function Insights(props: Insights) {
    const dataSelectOverView = [
        {
            label: 'Last 7 days',
            value: 'Last 7 days',
        },
        {
            label: 'Last 14 days',
            value: 'Last 14 days',
        },
        {
            label: 'Last 21 days',
            value: 'Last 21 days',
        },
    ];
    const listStaticOverView = [
        {
            icon: iconProfile,
            title: 'People reached',
            numberStatic: '256K',
            mode: 'up',
            numeral: '41.0%',
            titleMode: 'this week',
        },
        {
            icon: iconArrowUpDown,
            title: 'Engagement',
            numberStatic: '1.2x',
            numeral: '41.0%',
            mode: 'up',
            titleMode: 'this week',
        },
        {
            icon: iconMessages,
            title: 'Comments',
            numberStatic: '128',
            mode: 'down',
            numeral: '41.0%',
            titleMode: 'this week',
        },
        {
            icon: iconMouse,
            title: 'Link clicks',
            numberStatic: '80',
            mode: 'up',
            numeral: '41.0%',
            titleMode: 'this week',
        },
    ];
    const { title, classes, ...otherProps } = props;
    return (
        <Box component={'div'}
            className={classes.overReview}>
            <Box className={classes.header}
                component={'div'}>
                <Box className={classes.subTitle}>
                    <Box className='overReviewBlockColor insights'></Box>
                    <Text titleSB1>Insights</Text>
                </Box>
                <Box >
                    <SelectCustom data={dataSelectOverView}
                        defaultValues="Last 7 days" />
                </Box>
            </Box>
            <Grid className={classes.content}
                container
                spacing={0}>
                {listStaticOverView.map((item, index) => (
                    <React.Fragment key={uuidv4()}>
                        <Grid
                            key={index}
                            item
                            component={'div'}
                            className="item"
                            xs={2.22}
                        >
                            <Box component={'div'}
                                className="itemImg">
                                <IconSVG icon={item.icon}
                                    fill={COLORS.black}/>
                            </Box>
                            <Box component={'div'}
                                className="itemContent">
                                <Text caption1
                                    component={'div'}
                                    className="contentText">
                                    {item.title} &nbsp;
                                    <TooltipCustom title={'small description'}>
                                        <img alt=""
                                            src={iconInfoFilled} />
                                    </TooltipCustom>
                                </Text>
                                <Text h2
                                    color={COLORS.black}>{item.numberStatic}</Text>
                                <LabelColor
                                    className="itemProgress" 
                                    mode={item.mode}
                                    titleMode={item.titleMode}
                                    numeral={item.numeral}
                                />
                            </Box>
                        </Grid>
                        {((index+1)%4!==0) && (
                            <React.Fragment key={uuidv4()}>
                                <Divider key={index}
                                    orientation="vertical"
                                    flexItem />
                                <Grid item
                                    xs={1}></Grid>
                            </React.Fragment>	)
                        }
                    </React.Fragment>
                ))}
            </Grid>
        </Box>
    );
}


export default withStyles(styles)(Insights);