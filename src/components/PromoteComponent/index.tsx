import Page from '@helpers/Page';
import { Grid } from '@mui/material';
import { withStyles } from '@mui/styles';
import React from 'react';
import Insights from './Insights';
import RecentPost from './RecentPost';
import styles from './styles';

interface PromoteProps {

    title: string;
    classes?: any;
    titlePage?: string;
    dataTablePost: any,

}

function PromoteComponent(props: PromoteProps) {
    const { title, classes, ...otherProps } = props;
    return (
        <Page title={title}
            {...otherProps}>
            
            <Grid container>
                {/* Insights */}
                <Insights title={'insights'}
                    {...otherProps} />
            </Grid>
            <Grid container>
                <RecentPost title={'recent post'}
                    {...otherProps}
                />
            </Grid>
           
        </Page>
    );
}

export default withStyles(styles)(PromoteComponent);
