import { iconCheckAll } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { COLORS } from '@constants/colors';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import styles from '../styles';
import Button from '@helpers/Button';
import Input from '@helpers/Input';
interface IFooterProps{
    title?:string
}

const Footer = (props:IFooterProps & WithStyles<typeof styles>) => {
    const { classes } = props;
    return (
        <Box className={classes.overReview}
            style = {{padding: '20px 40px',width:'calc(100% - 340px)',position:'fixed',
                right:'0', zIndex:'99',bottom:'0',display:'flex',justifyContent:'space-between',alignItems:'center'}}>
            <Box style={{display:'flex',gap:'14px',alignItems:'center'}}>
                <IconSVG icon={iconCheckAll}/>
                <Text caption1
                    color={COLORS.grey4}>Last saved Oct 4, 2021 - 23:32</Text>
            </Box>
            <Box style={{display:'flex',gap:'8px'}}>
                <ButtonTransparent style={{padding: '8px 16px !important',color:COLORS.grey7}}
                >
                    Save Draft
                </ButtonTransparent>

                <Button color='primary'
                    type='submit'
                >Publish now</Button>

            </Box>

        </Box>
    );
};

export default withStyles(styles)(Footer);