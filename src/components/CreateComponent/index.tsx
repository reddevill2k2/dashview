import Page from '@helpers/Page';
import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import NameAndDescription from './NameAndDescription/index';
import ImageAndCTA from './ImageAndCTA';
import Price from './Price';
import CategoryAndAttribute from './CategoryAndAttribute';
import ProductFile from './ProductFile';
import Discussion from './Discussion';
import { Box } from '@mui/material';
import Footer from './Footer';
import Preview from './Preview';
interface ICreateComponentProps{
    titlePage:string,
    classes: Record<keyof ReturnType<typeof styles>, string>, // optional

}

const CreateComponent = (props: ICreateComponentProps) => {
    const {titlePage,classes ,...otherProps} = props;
    return (
        <Box>
            <Page
                titlePage={titlePage}
                {...otherProps}
            >
                <Box style={{display:'flex',gap:'8px'}}>
                    <Box>
                        <NameAndDescription title='Name & description'/>
                        <ImageAndCTA title='Images & CTA'/>
                        <Price title = 'Price'/>
                        <CategoryAndAttribute title = 'Category & attibutes'/>
                        <ProductFile title='Product files'/>
                        <Discussion title='Discussion'/>
                    </Box>
                    <Box>
                        <Preview title='Preview'/>
                    </Box>
                </Box>
            </Page>
            <Footer/>
        </Box>
    );
};

export default withStyles(styles)(CreateComponent);