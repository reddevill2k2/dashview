import { COLORS } from '@constants/colors';
import DropFile from '@helpers/DropFile';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import SectionTitle from '../SectionTitle';
import styles from '../styles';
interface IProductFileProps{
    title:string
}

const ProductFile = (props:IProductFileProps & WithStyles<typeof styles>) => {
    const { title , classes } = props;
    return (
        <Box className={classes.overReview}
            style = {{padding: '24px'}}>
            <Box className={classes.header}>
                <Box className={classes.subTitle}
                >
                    <Box className='overReviewBlockColor blueLight'></Box>
                    <Box style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
                        <Text titleSB1
                            color={COLORS.grey7}>{title}</Text>
                    </Box>
                </Box>
            </Box>
            <Box className={classes.nameAndDecription}
            >
                <Box>
                    <SectionTitle title='Content'/>
                    <DropFile title='Click or drop files' />
                </Box>
                <Box>
                    <SectionTitle title='Fonts'/>
                    <DropFile title='Click or drop files' />
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ProductFile);