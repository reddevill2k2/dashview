import { COLORS } from '@constants/colors';
import DropFile from '@helpers/DropFile';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import SectionTitle from '../SectionTitle';
import styles from '../styles';
interface ImageAndCTAProps{
    title:string
}

const options = [
    {
        value: 'Purchase now',
        label: 'Purchase now',
    },
    {
        value: 'Purchase tomorrow',
        label: 'Purchase tomorrow',
    },
    {
        value: 'Buy later',
        label: 'Buy later',
    }
];

const ImageAndCTA = (props:ImageAndCTAProps & WithStyles<typeof styles>) => {
    const { title , classes } = props;
    const methods = useFormContext();
    return (
        <Box className={classes.overReview}
            style = {{padding: '24px'}}>
            <Box className={classes.header}>
                <Box className={classes.subTitle}
                >

                    <Box className='overReviewBlockColor blueLight'></Box>
                    <Box style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
                        <Text titleSB1
                            color={COLORS.grey7}>{title}</Text>
                    </Box>

                </Box>
            </Box>
            <Box className={classes.nameAndDecription}
            >
                <Box>
                    <SectionTitle title='Cover images'/>
                    <DropFile {...methods.register('coverImage')}
                        title='Click or drop image' />
                </Box>
                <Box>
                    <SectionTitle title='Dropdown'/>
                    <SelectCustom defaultValues='Purchase now'
                        data={options}
                        {...methods.register('purchase')}
                    />
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ImageAndCTA);