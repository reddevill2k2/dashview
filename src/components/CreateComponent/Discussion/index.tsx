import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import TextEditor from '@helpers/TextEditor';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import SectionTitle from '../SectionTitle';
import styles from '../styles';
interface IDiscusstionProps{
    title:string
}

const Discusstion = (props:IDiscusstionProps & WithStyles<typeof styles>) => {
    const { title , classes } = props;
    const methods = useFormContext();
    const {control} = methods;
    return (
        <Box className={classes.overReview}
            style = {{padding: '24px',marginBottom:'76px'}}>
            <Box className={classes.header}>
                <Box className={classes.subTitle}
                >

                    <Box className='overReviewBlockColor orangeLight'></Box>
                    <Box style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
                        <Text titleSB1
                            color={COLORS.grey7}>{title}</Text>
                    </Box>

                </Box>
            </Box>
            <Box className={classes.nameAndDecription}
            >
                <Box>
                    <SectionTitle title='Message to reviewer'/>
                    <Controller
                        name="discussion"
                        control={control}
                        defaultValue=""
                        render={({ field }) => (
                            <TextEditor onChange={field.onChange}
                                value={field.value}
                                // isHide={isHide}
                            />
                        )}
                    />
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(Discusstion);