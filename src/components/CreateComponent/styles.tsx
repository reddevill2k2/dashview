import {createStyles} from '@mui/styles';
import { COLORS } from '@constants/colors';
import { maxHeight } from '@mui/system';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {

    },
    overReview: {
        backgroundColor:COLORS.grey1,
        width: '100%',
        marginTop: 8,
        height: maxHeight,
        padding: 24,
        borderRadius:8,
        '&.active': {
            marginBottom: '96px',
        },
        '& .overReviewBlockColor': {
            width: 16,
            height: 32,
            borderRadius:4,
            marginRight:16,

            '&.orangeLight':{
                backgroundColor:COLORS.orangeLight,
            },
            '&.blueLight':{
                backgroundColor:COLORS.blueLight,
            },
            '&.violetLight':{
                backgroundColor:COLORS.violetLight,
            },
            '&.greenNgoc':{
                background: COLORS.greenNgoc,
            }
        },
    },
    header: {
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap:'16px'
    },
    subTitle: {
        // padding:'12px',
        display: 'flex',
        flex: 1,
    },
    nameAndDecription: {
        marginTop: '38px',
        '& input':{
            width: '100%',
            padding: '12px'
        },
    }
});

export default styles;