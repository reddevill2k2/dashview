import { COLORS } from '@constants/colors';
import InputPrice from '@helpers/Input/InputPrice';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import SectionTitle from '../SectionTitle';
import styles from '../styles';

interface INameAndDescriptionProps{
    title:string
}

const Price = (props:INameAndDescriptionProps & WithStyles<typeof styles>) => {
    const { title , classes } = props;
    const methods = useFormContext();
    return (
        <Box className={classes.overReview}
            style = {{padding: '24px'}}>
            <Box className={classes.header}>
                <Box className={classes.subTitle}
                >

                    <Box className='overReviewBlockColor greenNgoc'></Box>
                    <Box style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
                        <Text titleSB1
                            color={COLORS.grey7}>{title}</Text>
                    </Box>

                </Box>
            </Box>
            <Box className={classes.nameAndDecription}
            >
                <SectionTitle title='Amount'/>
                <Box style={{position:'relative',width:'100%',height:'48px'}}>
                    <InputPrice {...methods.register('price.amount')}/>
                </Box>
                <SectionTitle title='Allow customers to pay they want'/>
                <Box >
                    <Box style={{display:'flex',
                        justifyContent:'space-around',gap:'12px',
                        borderTop: `1px solid ${COLORS.grey3}`}}>
                        <Box style={{width:'100%'}}>
                            <SectionTitle title='Minimum amount'
                                fontSize='small'/>
                            <Box style={{position:'relative',width:'100%',height:'48px',}}><InputPrice
                                {...methods.register('price.minimumAmount')}/></Box>

                        </Box>
                        <Box style={{width:'100%'}}>
                            <SectionTitle title='Suggested amount'
                                fontSize='small'/>
                            <Box style={{position:'relative',width:'100%',height:'48px',}}><InputPrice
                                {...methods.register('price.suggestedAmount')}/></Box>
                        </Box>
                    </Box>
                </Box>

            </Box>
        </Box>
    );
};

export default withStyles(styles)(Price);