import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Grid } from '@mui/material';
import clsx from 'clsx';
import TotalCustomers from './PanelLeft/TotalCustomers';
import Refund from './PanelRight/Refund';
import TopDevice from './PanelRight/TopDevice';
import TrafficChannel from './PanelLeft/TrafficChannel';
import ActiveCustomers from './PanelLeft/ActiveCustomers';
import TopCountry from './PanelRight/TopCountry';
import Message from './PanelRight/Message';
import NewCustomer from './PanelRight/NewCustomer';
import ShareProducts from './PanelLeft/ShareProducts';

interface OverviewProps {
    title: string,
    phone?: number, // optional
    titlePage?: string,
    classes: any,
    overviewUserDummyData?: {id: number, url: string, name: string}[]
}

function OvervviewComponent(props: OverviewProps) {
    const {title, titlePage,classes, ...otherProps} = props;
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <Grid container 
                spacing={1}
                className={clsx(classes.overviewPage)}>
                <Grid item
                    xs={12}
                    md={8.5}
                    className={classes.panelLeft}>
                    <TotalCustomers title='Total customers'
                        {...otherProps}/>
                    <TrafficChannel title='Traffic channel'
                        {...otherProps}/>
                    <ActiveCustomers title='Active customers'
                        {...otherProps}/>
                    <ShareProducts title='Share your products'
                        {...otherProps}/>
                </Grid>
                <Grid item
                    xs={12}
                    md={3.5}
                    className={classes.panelRight}>
                    <Refund title='Refund requests'
                        {...otherProps} />
                    <TopDevice title='Top device'
                        {...otherProps} />
                    <TopCountry title='Top country'
                        {...otherProps} />
                    <Message title='Message'
                        {...otherProps} />
                    <NewCustomer title='New customer'
                        {...otherProps} />
                </Grid>
            </Grid>
        </Page>
    );
}

export default withStyles(styles)(OvervviewComponent);