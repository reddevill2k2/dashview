import React from 'react';
import { WithStyles, withStyles } from '@mui/styles';

import styles from './styles';
import { Box, Grid, colors, IconButton } from '@mui/material';
import TitleElement from '@helpers/TitleElement';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import iconTwitter from '@assets/icons/twitter/filled.svg';
import IconSVG from '@helpers/IconSVG';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import SelectCustom from '@helpers/SelectCustom';
import moment from 'moment';
import faker from '@faker-js/faker';
import { Chart as ChartJS,
    CategoryScale,
    LinearScale,
    Title,
    Tooltip,
    Legend,
    PointElement,
    LineElement, } from 'chart.js';
import Crosshair from 'chartjs-plugin-crosshair';

import { Line } from 'react-chartjs-2';
import LabelColor from '@helpers/LabelColor';
interface ITotals {
    title: string;
}

const dataSelectTime = [
    {
        label: 'Last 28 days',
        value: 'Last 28 days',
    },
    {
        label: 'Last 14 days',
        value: 'Last 14 days',
    },
    {
        label: 'Last 7 days',
        value: 'Last 7 days',
    },
];

function ActiveCustomers(props: ITotals & WithStyles<typeof styles>) {
    const { classes, title, overviewUserDummyData, ...otherProps } = props;

    ChartJS.register(
        CategoryScale,
        LinearScale,
        Title,
        Tooltip,
        Legend,
        PointElement,
        LineElement,
        Crosshair
        
    );
    const labels =faker.date.betweens('2020-01-01T00:00:00.000Z', '2022-01-01T00:00:00.000Z', 6);
    const data = React.useMemo(() =>({
        labels:labels.map((date:any)=> moment(date).format( 'MMM D')),
        datasets:[
            {
                label: 'Daily',
                data: labels.map(() => faker.datatype.number({ min:10,max:100})),
                lineTension: 0.4,
                borderColor: COLORS.blueLight,
                borderWidth: 4,
                pointRadius: 0,
                hoverRadius:4,
                pointColor: COLORS.blueLight,
                backgroundColor: COLORS.blueLight,
                hoverBackgroundColor: COLORS.blueLight
            },
            {
                label: 'Weekly',
                data: labels.map(() => faker.datatype.number({ min:100,max:300})),
                lineTension: 0.4,
                borderColor: COLORS.greenNgoc,
                borderWidth: 4,
                pointRadius: 0,
                hoverRadius:4,
                pointColor: COLORS.greenNgoc,
                backgroundColor: COLORS.greenNgoc,
                hoverBackgroundColor: COLORS.greenNgoc
            },
            {
                label: 'Monthly',
                data: labels.map(() => faker.datatype.number({ min:300,max:600})),
                lineTension: 0.4,
                borderColor: COLORS.blue,
                borderWidth: 4,
                pointRadius: 0,
                hoverRadius:4,
                pointColor: COLORS.blue,
                backgroundColor: COLORS.blue,
                hoverBackgroundColor: COLORS.blue
            },
        ],
    }),[]);

    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false,
            },
            tooltip: {
                // mode: 'nearest',
                intersect: false,
                positioners:'nearest',
                boxPadding:4,
                titleSpacing:4,
            },

            crosshair: {
                line: {
                    color: COLORS.grey4,
                    width: 1,
                    dashPattern:[10,10]
                },
                snapping:{
                    enabled:true
                },
                sync: {
                    enabled: true,
                    group: 1,
                    suppressTooltips: false
                },
                zoom: {
                    enabled: true,
                    zoomboxBackgroundColor: COLORS.hover.hoverZoomBoxBackground,
                    zoomboxBorderColor: COLORS.hover.hoverChartZoomBox,
                    zoomButtonText: 'Reset Zoom',
                    zoomButtonClass: 'reset-zoom',
                }
            }
        },
        interaction: { mode: 'index' },
        hover: {
            mode: 'index' as const,
            intersect: false,
        },
        scales: {
            x: {
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,
                },
            },
            y: {
                grid: {
                    drawBorder: false,

                },
                ticks: {
                    stepSize: 100,
                    callback: (label:any, index: any, labels:any) => {
                        return label;
                    }
                }
            },
        }
    };


    return (
        <Box component={'div'}
            className={classes.activeCustomers}>
            <Grid container
                className={classes.boxHeader}>
                <Grid item >
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.violetLight}>{title}</TitleElement>
                </Grid>
                <Grid item >
                    <SelectCustom data={dataSelectTime}
                        defaultValues="Last 28 days" />
                </Grid>
            </Grid>
            <Box component={'div'}
                className={classes.activeCustomersChart}
            >
                <Line
                    data={data}
                    options={options}
                    redraw
                />
            </Box>

        </Box>
    );
}

export default withStyles(styles)(ActiveCustomers);
