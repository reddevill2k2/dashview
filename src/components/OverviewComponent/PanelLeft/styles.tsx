import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root:{
    
    },
    totalCustomers: {
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: '8px',
    },
    boxHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    Chart: {
        position: 'relative',
        height: 250,
        cursor: 'crosshair',
        paddingBottom: '24px',
        borderBottom: `1px solid ${COLORS.grey3}`,
        '& .reset-zoom':{
            position: 'absolute',
            top:0,
            right:0,
        }
    },
    chartLineImage: {
        objectFit: 'contain',
        width: '100%',
        height: 'auto',
        marginTop: 32,
    },
    itemBox:{
        margin:'16px 0 32px',
    },
    contentTabLeft: {
        paddingTop: '24px ',
        height: 250,
        '& .box-message': {
            marginBottom: 24,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            '& .text': {
                fontSize: 15,
                color: COLORS.grey7,
                maxWidth: 250,
            },
            '& .button': {
                cursor: 'pointer',
                padding: '12px 20px',
                borderRadius: '12px',
                border: `2px solid ${COLORS.grey3}`,
                '& .button-text': {
                    fontWeight: 700,
                    fontSize: 15,
                    color: COLORS.grey7
                }
            }
        },
        '& .box-users': {
            padding: '32px 0',
            display: 'flex',
            '& .user': {
                display: 'flex',
                justifyContent: 'center',
                '& img': {
                    width: 64,
                    height: 64,
                    display: 'inline-block',
                    marginBottom: 12,
                    transition: 'all .3s ease',
                    cursor: 'pointer',
                    '&:hover': {
                        transform: 'scale(1.05)'
                    },
                }
            },
            '& .view-all': {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                '& .box-arrow': {
                    backgroundColor: COLORS.grey3,
                    width: 64,
                    height: 64,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: '50%',
                    transition: 'all .3s ease',
                    cursor: 'pointer',
                    '&:hover': {
                        backgroundColor: COLORS.hover.hoverPicutre,
                    },
                    '& img': {
                        width: 24,
                        height: 24,
                      
                    }
                },
                '&-text': {
                    marginTop: 12,
                }
              
            }
        }
    },
    TrafficChannelComponent:{
        marginTop: 8,
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: '8px',
    },
    TrafficChannelChart:{
        marginTop: 32,
        
    },
    test:{
        display: 'flex',
        justifyContent: 'space-between'
    },
    activeCustomers:{
        marginTop: 8,
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: '8px',
    },
    activeCustomersChart:{
        marginTop: 32,
        position: 'relative',
        height: 250,
        cursor: 'crosshair',
        '& .reset-zoom':{
            position: 'absolute',
            top:0,
            right:0,
        }
    },
    shareProductsPage:{
        marginTop: '8px',
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: '8px',
    },
    buttonAll: {
        padding: '10px 15px',
        marginTop: 16,
    },
    listShareProducts:{
        borderBottom: `1px solid ${COLORS.grey3}`,
        '& .product-list':{
            borderRadius:'16px',
            overflow: 'hidden',
            padding: '32px 0',
            '& .product-image':{
                position: 'relative',
                '& img':{
                    width:'380px',
                    objectFit:'cover',
                    maxWidth:'100%',

                },
                '&::before': {
                    content: '""',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                    borderRadius:'16px',
                    backgroundColor: 'transparent',
                    transition: 'all .3s ease',
                    zIndex: -1,
                },
                '&:hover::before': {
                    zIndex:1,
                    backgroundColor: 'rgba(70, 70, 70, 0.38)',
                },
            },
            '& .product-content':{
                marginTop:8,
                alignItems: 'center'
            },
        }
    },
    shareModal:{
        paddingTop:'32px',
        '& .social-list':{
            paddingBottom:'24px'
        }
    },
});
export default styles;