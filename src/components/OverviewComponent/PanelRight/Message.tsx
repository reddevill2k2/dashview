import React from 'react';
import {withStyles,WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, IconButton, Divider } from '@mui/material';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { iconBasketLight, imageOverviewUser1, imageOverviewUser2, imageOverviewUser3 } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import {v4 as uuidv4} from 'uuid';
import _ from 'lodash';

interface MessageProps {
    title: string,
    classes?: any,
}
const listMessageDummyData = [
    {
        id: 1,
        image: imageOverviewUser1,
        username: '@username',
        name: 'Winnifred',
        message: 'Message goes here 😊',
        time: '30m',
    },
    {
        id: 2,
        image: imageOverviewUser2,
        username: '@username',
        name: 'Esther',
        message: 'Message goes here 😊',
        time: '1h',
    },
    {
        id: 3,
        image: imageOverviewUser3,
        username: '@username',
        name: 'Leland',
        message: 'Message goes here 😊',
        time: '8h',
    },
    {
        id: 4,
        image: imageOverviewUser1,
        username: '@username',
        name: 'Winnifred',
        message: 'Message goes here 😊',
        time: '30m',
    },
    {
        id: 5,
        image: imageOverviewUser2,
        username: '@username',
        name: 'Esther',
        message: 'Message goes here 😊',
        time: '1h',
    },
    {
        id: 6,
        image: imageOverviewUser3,
        username: '@username',
        name: 'Leland',
        message: 'Message goes here 😊',
        time: '8h',
    },
];
function MessageComponent(props: MessageProps & WithStyles<typeof styles>) {
    const {title, classes, ...otherProps} = props;
    return (
        <Box className={classes.MessageComponent}>
            <TitleElement bg={COLORS.violetLight}>
                {title}
            </TitleElement>
            <Box display={'flex'}
                className={classes.listMessages}>
                <Box className="render-list">
                    {!_.isEmpty(listMessageDummyData) && listMessageDummyData?.map((comment: {
                        id: number,
                        image: string,
                        username: string,
                        name: string,
                        message: string,
                        time: string,
                    }, index: number) => (
                        <React.Fragment  key={uuidv4()}>
                            <Box
                                display={'flex'}
                                alignItems={'start'}
                                className='item-message'>
                                <div className="icon-user">
                                    <img src={comment.image}
                                        alt="" />
                                </div>
                                <a href=''
                                    onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> e.preventDefault()}
                                    className="info-user">
                                    <Box component={'div'}
                                        width="100%">
                                        <Box display='flex'
                                            justifyContent='space-between'>
                                            <Box display={'flex'}
                                                component={'div'}>
                                                <b>{comment.name}</b>
                                                {comment.username}
                                            </Box>
                                            <Text caption1
                                                color={COLORS.shades75}>{comment.time}</Text>
                                        </Box>
                                        <Box>
                                            {comment.message}
                                        </Box>
                                    </Box>
                                </a>
                            </Box>
                            {index + 1 < listMessageDummyData.length &&
                            (
                                <Divider className='divider'
                                    sx={{backgroundColor:COLORS.grey1}}/>
                            )
                            }
                        </React.Fragment>
                    ))}
                    <Box className={classes.buttonAll}>
                        <ButtonTransparent>
                          View all message 
                        </ButtonTransparent>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(MessageComponent);