import React from 'react';
import {withStyles,WithStyles} from '@mui/styles';
import styles from './styles';
import { Box, Grid, IconButton } from '@mui/material';
import TitleElement from '@helpers/TitleElement';
import { COLORS } from '@constants/colors';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import faker from '@faker-js/faker';

interface TopContryProps {
    title: string,
    classes?: any,
}

function TopContryComponent(props: TopContryProps & WithStyles<typeof styles>) {
    const {title, classes, ...otherProps} = props;
    ChartJS.register(
        CategoryScale,
        LinearScale,
        BarElement,
        Title,
        Tooltip,
        Legend
    );
    const labels = ['Vietnam', 'USA', 'Indonesia', 'Hongkong', 'Russia', 'Ukraine'];
    const data ={
        labels,
        datasets: [
            {
                label: 'Top country',
                data: labels.map(() => faker.datatype.number({ min: 100, max: 2000 })),
                backgroundColor:COLORS.greenNgoc,
                hoverBackgroundColor: COLORS.greenNgocHover,
                barPercentage: 0.8,
                borderRadius: 4,
            }
        ]
    };
    const options = {
        responsive: true,
        maintainAspectRatio: false,
        indexAxis: 'y' as const,
        elements: {
            bar: {
            },
        },
        plugins: {
            legend: {
                position: 'right' as const,
                display: false
            },
            title: {
                display: false,
            },
            crosshair: false,
        },
        scales: {
            x: {
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,
                },
                ticks: {
                    stepSize: 500,
                    suggestedMin: 5,
                }
            },
            y: {
                grid: {
                    drawBorder: false,

                },
                ticks: {
                    suggestedMin: 5,
                }
            },
        }
    };

    return (
        <Box className={classes.TopContryComponent}>
            <TitleElement bg={COLORS.violetLight}>
                {title}
            </TitleElement>
            <Box
                className='chartBox'>
                <Bar options={options}
                    data={data} />
            </Box>
        </Box>
    );
}

export default withStyles(styles)(TopContryComponent);