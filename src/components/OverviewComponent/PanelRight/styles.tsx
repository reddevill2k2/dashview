import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root:{
    
    },
    RefundComponent:{
        backgroundColor: COLORS.grey1,
        padding: '24px',
        borderRadius: '8px',
        '& .content': {
            marginTop: 32,
            '& .icon-basket': {
                backgroundColor: COLORS.redLight,
                transition: 'all .3s ease',
                width: 48,
                height: 48,
                borderRadius: '48px',
                marginRight: 8,
                justifyContent: 'center',
                alignItems: 'center',
                display: 'flex',
                cursor: 'default',
                '& .icon-svg': {
                    color: COLORS.red,
                }
            }
        },
    },
    buttonTransparent: {
        marginTop: 24,
        textAlign:'center',
    },
    TopDeviceComponent:{
        marginTop: 8,
        backgroundColor: COLORS.grey1,
        padding: '24px',
        borderRadius: '8px',

    },
    DoughnutChart:{

        maxWidth: '228px',
        maxHeight: '228px',
        display: 'flex',
        margin: '24px auto'
    },
    TopContryComponent:{
        marginTop: 8,
        backgroundColor: COLORS.grey1,
        padding: '24px',
        borderRadius: '8px',
        '& .chartBox':{
            marginTop:32,
            height:'300px',
        }
    },
    MessageComponent:{
        marginTop: 8,
        backgroundColor: COLORS.grey1,
        padding: '24px',
        borderRadius: '8px',
        '& .render-list':{
            width: '100%',
        }
    },
    listMessages: {
        marginTop: 32,
        padding: 0,
        '& .item-message': {
            '& .icon-user': {
                '& img': {
                    width: 48,
                    height: 48,
                    display: 'inline-block', 
                    marginRight: 12,
                }
            },
            '& .info-user': {
                width: '100%',
                display: 'block',
                alignItems: 'start',
                textDecoration: 'none',
                color: 'unset',
                transition: 'all .3s ease',
                '&:hover': {
                    // color: COLORS.grey4,
                },
            }
        },
        '& .divider': {
            margin: '24px 0',
            
        }
    },
    buttonAll: {
        padding: '10px 15px',
        marginTop: 16,
    },
    NewCustomerComponent:{
        marginTop: 8,
        backgroundColor: COLORS.grey1,
        padding: '24px',
        borderRadius: '8px',
    }
});
export default styles;