import React from 'react';
import {withStyles,WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, IconButton } from '@mui/material';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { iconTablet, iconLaptop, iconMobile } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import { Doughnut } from 'react-chartjs-2';
import { Chart as ChartJS,
    ArcElement,
    CategoryScale,
    LinearScale,
    Filler,
    Legend,
    Title,
    Tooltip,
    SubTitle } from 'chart.js';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';

interface TopDeviceProps {
    title: string,
    classes?: any,
}

function TopDeviceComponent(props: TopDeviceProps & WithStyles<typeof styles>) {
    const {title, classes, ...otherProps} = props;
    ChartJS.register(
        ArcElement,
        CategoryScale,
        LinearScale,
        Filler,
        Legend,
        Title,
        Tooltip,
        SubTitle
    );
    const chartData = [20, 80];
    const showData = chartData[0] + '%';
    const options = {
        cutoutPercentage: 20,
        suppressTooltips: true,
        responsive: true,
        scales: {
        },
        plugins: {
            legend: {
                position: 'top' as const,
                display: false,
            },
            tooltip: {
                intersect: false,
                boxPadding:4,
                titleSpacing:4,
            },
            crosshair: false
        }
    };
    const labels =['Mobile', 'Tablet', 'Desktop'];
    const data = {
        labels,
        datasets: [
            {
                labels: labels,
                data: labels.map(() => _.round(_.random(100, 8000, true), 0)),
                backgroundColor: [COLORS.violet,COLORS.green,COLORS.blue],
                hoverBackgroundColor: [COLORS.violetLight,COLORS.greenNgoc,COLORS.blueLight],
                pointStyle: 'circle',
                cutout:'80%',
            }
        ]
    };
    const listDevicesRender = (dataset:any) =>{
        const total =  dataset.data[0]+dataset.data[1]+dataset.data[2];
        const icons = [iconMobile, iconTablet, iconLaptop];
        return(
            dataset.data.map((item:number,index:number) => (
                <Box className={classes.DevicesItem}
                    key={uuidv4()}
                    component={'div'}
                    sx={{display:'flex',flexDirection:'column'}}
                >
                    <IconSVG icon={icons[index]}
                        fill={dataset.backgroundColor[index]}
                    />
                    <Text captionM2
                        color={COLORS.grey4}
                        sx={{padding:'8px 0 4px'}}>
                        {dataset.labels[index]}
                    </Text>
                    <Text titleSB1
                        color={COLORS.grey6}>
                        {_.round(item / total * 100, 0)}%
                    </Text>
                    
                </Box>
            ))
        );
    };
    return (
        <Box className={classes.TopDeviceComponent}>
            <TitleElement bg={COLORS.blueLight}>
                {title}
            </TitleElement>
            <Box
                className={classes.DoughnutChart}
            >
                <Doughnut 
                    data={data}
                    options={options}
                />
            </Box>
            <Box className={classes.listDevices}
                sx={{ display: 'flex',alignItems:'center',justifyContent: 'space-between'}}>
                { data.datasets[0] && listDevicesRender(data.datasets[0]) } 
            </Box>
        </Box>
    );
}

export default withStyles(styles)(TopDeviceComponent);