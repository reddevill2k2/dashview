import React from 'react';
import {withStyles,WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, IconButton } from '@mui/material';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { iconBasketLight } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';


interface RefundProps {
    title: string,
    classes?: any,
}

function RefundComponent(props: RefundProps & WithStyles<typeof styles>) {
    const {title, classes, ...otherProps} = props;
    return (
        <Box className={classes.RefundComponent}>
            <TitleElement bg={COLORS.orangeLight}>
                {title}
            </TitleElement>
            <Box display={'flex'}
                className='content'>
                <IconButton className='icon-basket'>
                    <IconSVG icon={iconBasketLight}/>
                </IconButton>
                <Text bodyM1
                    color={COLORS.grey4}>
              You have <b>52 open refund requests</b> to action. This includes <b>8 new requests.</b> 👀
                </Text>
            </Box>
            <ButtonTransparent className={classes.buttonTransparent}>
            Review refund requests
            </ButtonTransparent>
        </Box>
    );
}

export default withStyles(styles)(RefundComponent);