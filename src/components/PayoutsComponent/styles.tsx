import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from '@constants/colors';
import { maxHeight } from '@mui/system';

const styles = (theme: any) => createStyles({
    root: {
    },
    overReview: {
        backgroundColor:COLORS.grey1,
        width: '100%',
        marginTop: 8,
        height: maxHeight,
        padding: 24,
        borderRadius:8,
        '& .overReviewBlockColor': {
            width: 16,
            height: 32,
            borderRadius:4,
            marginRight:16,

            '&.insights':{
                backgroundColor:COLORS.orangeLight,
            },
            '&.recentPost':{
                backgroundColor:COLORS.blueLight,
            },
            '&.refund':{
                backgroundColor:COLORS.violetLight,
            }
        },
    },
    header: {
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap:'16px'
    },
    subTitle: {
        display: 'flex',
        flex: 1,
    },
    postTable:{
        '& tr td':{
            verticalAlign:'top'
        }
    },
    wrapper:{
        marginTop:'32px',
        display:'flex',
        justifyContent:'space-between',
        gap:'64px',
        '& button':{
            alignSelf:'flex-start',
        }
    },
    currentBalance:{
        display:'flex',
        alignItems:'flex-start',
        gap:'24px',
        borderRight:'1px solid #EFEFEF',
        padding:'0 32px',
        paddingRight:'96px',
        '&.first':{
            paddingLeft:'0',
        },
        '& .icon':{
            background:props => props.bgColor,
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            borderRadius:'50%',
            width: '48px',
            height: '48px',
        }
    },
    table:{
        marginTop:'12px',
        '& tbody tr:nth-child(2n+1)':{
            backgroundColor: COLORS.grey9,
        },
    },
    status:{
        padding:'2px 8px !important',
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        width:'max-content',
        //height:'28px',
        background:COLORS.greenNgoc,
        borderRadius:'6px',
    },
    contentModal: {
        '& .modal-party': {
            width: 128,
            height: 128,
            borderRadius: '50%',
            backgroundColor: COLORS.greenLight,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            
        },
        '& .box-content-text': {
            textAlign: 'center',
            marginTop: 20,
          
        },
        '& .box-button-custom': {
            marginTop: 20,
        }
    }
});

export default styles;