import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles'
import Page from '@helpers/Page';

interface NotFoundProps {
}

function NotFoundComponent(props: NotFoundProps) {
  return (
    <Page title={'Page 404'}>Không tìm thấy đường dẫn</Page>
  )
}

export default withStyles(styles)(NotFoundComponent)