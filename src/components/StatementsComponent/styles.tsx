import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import { maxHeight } from '@mui/system';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {

    },
    overReview: {
        backgroundColor:COLORS.grey1,
        width: '100%',
        marginTop: 8,
        height: maxHeight,
        padding: 24,
        borderRadius:8,
        '& .overReviewBlockColor': {
            width: 16,
            height: 32,
            borderRadius:4,
            marginRight:16,

            '&.insights':{
                backgroundColor:COLORS.orangeLight,
            },
            '&.recentPost':{
                backgroundColor:COLORS.blueLight,
            },
            '&.refund':{
                backgroundColor:COLORS.violetLight,
            }
        },
    },
    header: {
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap:'16px'
    },
    subTitle: {
        display: 'flex',
        flex: 1,
    },
    postTable:{
        '& tr td':{
            verticalAlign:'top'
        }
    },
    wrapper:{
        display:'flex',
        justifyContent:'space-between',
        gap:'64px',
        '& button':{
            alignSelf:'flex-start',
        }
    },
    currentBalance:{
        display:'flex',
        alignItems:'flex-start',
        flexDirection:'column',
        gap:'24px',
        borderRight:'1px solid #EFEFEF',
        paddingRight:'96px',
        '&.first':{
            paddingLeft:'0',
        },
        '&.last':{
            borderRight:'none'
        },
        '& .icon':{
            background:props => props.bgColor,
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            borderRadius:'50%',
            width: '48px',
            height: '48px',
        }
    },
    table:{
        marginTop:'12px',
        '& tbody tr:nth-child(2n+1)':{
            backgroundColor: COLORS.grey9,
        },
    },
    status:{
        padding:'2px 8px !important',
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        width:'max-content',
        //height:'28px',
        background:COLORS.greenNgoc,
        borderRadius:'6px',
    },
});

export default styles;