import { COLORS } from '@constants/colors';
import { ITransaction } from '@containers/Admin/StatementsContainer/types';
import Button from '@helpers/Button';
import SelectCustom from '@helpers/SelectCustom';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import React, { useMemo } from 'react';
import { CellProps, Column } from 'react-table';
import styles from './styles';
interface ITransactionProps{
    title:string,
    dataTableTransactions: ITransaction,
    handleChangeSelect: (option: number) => void
}

const Transaction = (props:ITransactionProps & WithStyles<typeof styles>) => {
    const { title, dataTableTransactions,handleChangeSelect, classes} = props;
    const columns : Column[] = useMemo(()=> [
        {
            Header:'Date',
            Cell: (values:CellProps<NonNullable<null>>) =>{
                const original:ITransaction = values.cell.row.original;
                return (
                    <Text base2
                        color={COLORS.grey7}>{original.date}</Text>
                );
            }
        },
        {
            Header:'Type',
            Cell: (values:CellProps<NonNullable<null>>) => {
                const original:ITransaction = values.cell.row.original;
                return (
                    <Text base2
                        color={COLORS.grey7}
                        // eslint-disable-next-line react/prop-types
                        className={classes.status}
                        style={{background:`${original.type ? COLORS.greenNgoc : COLORS.orangeLight}`}}
                    >{`${original.type ? 'Sale' : 'Author fee'}`}</Text>
                );
            }
        },
        {
            Header:'Detail',
            Cell: (values:CellProps<NonNullable<null>>) =>{
                const original:ITransaction = values.cell.row.original;
                return (
                    <Box>
                        <Text base2
                            color={COLORS.grey5}>{original.detail}</Text>
                        <Text captionM2
                            color={COLORS.grey4}>{original.detail_thumbnail}</Text>
                    </Box>
                );
            }
        },
        {
            Header:'Price',
            Cell: (values:CellProps<NonNullable<null>>) =>{
                const original:ITransaction = values.cell.row.original;
                return (
                    <Text base2
                        color={COLORS.grey5}>${original.price}</Text>
                );
            }
        },
        {
            Header:'Amount',
            Cell: (values:CellProps<NonNullable<null>>) =>{
                const original:ITransaction = values.cell.row.original;
                return (
                    <Text base2
                        color={`${original.type ? COLORS.grey7 : COLORS.red}`}>
                        {`${original.type ? '+': '-'}`}${original.amount}</Text>
                );
            }
        }
    ],[]);


    const dataSelect = useMemo(()=>[
        {
            value: 'last30days',
            label: 'Last 30 days',
        },
        {
            value: '2',
            label: 'This month',
        },
        {
            value: '3',
            label: 'All time',
        },

    ],[]);

    const onHandleSelect = (e: React.FormEvent<HTMLInputElement>) => {
        handleChangeSelect(+e.target.value);
    };
    return (
        <Box className={classes.overReview}>
            <Box className={classes.header}
                component={'div'}>
                <Box className={classes.subTitle}>
                    <Box className='overReviewBlockColor recentPost'></Box>
                    <Text titleSB1>{title}</Text>
                </Box>
                <SelectCustom defaultValues='last30days'
                    handleSelect={onHandleSelect}
                    data={dataSelect}/>
            </Box>

            <Box className={classes.postTable}>
                <Box className={classes.table}>
                    <TableCustom data={dataTableTransactions}
                        options={{
                            hoverRow: true,
                            changePage: true,
                        }}
                        columns={columns}/>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(Transaction);