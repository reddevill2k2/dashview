import { COLORS } from '@constants/colors';
import { iconHeartFilled, iconHeartLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import { Box, IconButton } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import React, { useState } from 'react';
import styles from './styles';



const LoveReaction = (props: any & WithStyles<typeof styles>) => {
    const [ reactionState , setReactionState] = useState<boolean>(false);

    const onHandleReactionChange = () => {
        setReactionState(!reactionState);
    };
    return (
        <Box onClick = {onHandleReactionChange}>
            <IconButton className='icon'
                sx={{backgroundColor:COLORS.white}}>
                <IconSVG small
                    icon={`${reactionState ? iconHeartFilled : iconHeartLight}`}
                    fill={`${reactionState && COLORS.blueHover}`}/>
            </IconButton>
        </Box>
    );
};

export default React.memo(withStyles(styles)(LoveReaction));