import Button from '@helpers/Button';
import { Fab } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import React, { useMemo, useState } from 'react';
import styles from './styles';


const BLUSH = 'blush';
const SURPRISED = 'surprised';
const SWEATGRINNING = 'sweatgrinning';
const COOL = 'cool';
const SLEEPY = 'sleepy';
const CRYINGWITHLAUGHTER = 'cryingwithlaughter';
const HEARTYEYES = 'hearteyes';
const SMILEEYES = 'smileseye';

const blush = '😊';
const surprised = '😮';
const sweatgrinning = '😅';
const cool = '😎';
const sleepy = '😪';
const cryingwithlaughter = '😂';
const hearteyes = '😍';
const smileseye = '😄';
const smile = '😀';

interface IFacebookReactionsProps{
    children: React.ReactNode
}

interface IReaction{
    emoji:any,
    status:string | undefined
}

interface IFacebookReactionProps{
    reaction: string,
    onHandleReactionChange: (reaction: IReaction) => void,
    reactionState: IReaction
}

const FacebookReaction = ({
    reaction,
    onHandleReactionChange,
    reactionState
}: IFacebookReactionProps) => {
    let icon:string;
    switch (reaction) {
        case BLUSH:
            icon = blush;
            break;
        case SURPRISED:
            icon = surprised;
            break;
        case SWEATGRINNING:
            icon = sweatgrinning;
            break;
        case COOL:
            icon = cool;
            break;
        case SLEEPY:
            icon = sleepy;
            break;
        case CRYINGWITHLAUGHTER:
            icon = cryingwithlaughter;
            break;
        case HEARTYEYES:
            icon = hearteyes;
            break;
        case SMILEEYES:
            icon = smileseye;
            break;
        default:
            icon = '';
            break;
    }

    return (
        <button

            className="facebook-reaction"
            onClick={() => {
                if (reactionState.status !== reaction) {
                    onHandleReactionChange({ emoji: icon, status: reaction });
                }
            }}
        >
            <span className="tooltip">{reaction}</span>
            {icon}
        </button>
    );
};

const reactions = [
    BLUSH,
    SURPRISED,
    SWEATGRINNING,
    COOL,
    SLEEPY,
    CRYINGWITHLAUGHTER,
    HEARTYEYES,
    SMILEEYES
];


const FacebookReactions = (props: IFacebookReactionsProps & WithStyles<typeof styles>) => {
    const { classes } = props;
    const [reactionState, setReactionState] = useState<IReaction>({
        emoji: props.children,
        status: undefined
    });
    const onHandleReactionChange = (reaction: IReaction) => {
        setReactionState(reaction);
    };

    return (
        <div className={classes.commentReactions}>
            <span className="btn-reaction">
                <span>{reactionState.emoji}</span>
            </span>
            <ul className="facebook-reactions">
                {reactions.map((reaction, index) => (
                    <li key={index}>
                        <FacebookReaction
                            reactionState={reactionState}
                            onHandleReactionChange={onHandleReactionChange}
                            reaction={reaction}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default withStyles(styles)(FacebookReactions);
