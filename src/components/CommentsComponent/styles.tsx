import {createStyles} from '@mui/styles';
import { COLORS } from '@constants/colors';
import { maxHeight } from '@mui/system';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {

    },
    overReview: {
        backgroundColor:COLORS.grey1,
        width: '100%',
        marginTop: 8,
        height: maxHeight,
        padding: 24,
        borderRadius:8,
        '&.active': {
            marginBottom: '96px',
        },
        '& .overReviewBlockColor': {
            width: 16,
            height: 32,
            borderRadius:4,
            marginRight:16,

            '&.insights':{
                backgroundColor:COLORS.orangeLight,
            },
            '&.recentPost':{
                backgroundColor:COLORS.blueLight,
            },
            '&.refund':{
                backgroundColor:COLORS.violetLight,
            }
        },
    },
    header: {
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap:'16px'
    },
    subTitle: {
        padding:'12px',
        display: 'flex',
        flex: 1,
    },
    postTable:{
        '& tr td':{
            verticalAlign:'top'
        },
        '& tbody > tr:hover .box-icon':{
            opacity: 1,
            visibility: 'visible',
        }
    },
    product:{
        display:'flex',
        alignItems:'center',
        gap:'20px',
        width:'268px',
        height:'80px',
        '& .product-column-content':{
            display:'flex',
            flexDirection:'column',
            alignItems:'flex-start',
            gap:'4px',
            width:'168px',
            height:'68px'
        }
    },
    status:{
        width: '136px',
        height: '28px',
        position:'relative',
        '& .status-number':{
            display:'flex',
            flexDirection:'column',
            justifyContent:'center',
            alignItems:'center',
            padding:'2px 8px',
            gap:'10px',
            position:'absolute',
            width:'101px',
            height:'28px',
            left:'0px',
            top:'calc(50% - 28px/2)',
            background: '#B5E4CA',
            borderRadius: '6px',
            '&.active':{
                width:'91px',
                background: '#CABDFF',
            }
        },
        '& p':{
            width:'max-content'
        }
    },
    customer:{
        display:'flex',
        alignItems:'center',
        gap:'12px',
        width:'180px',
        height:'32px',
        '& .customer-image ':{
            '& img':{
                width:'32px',
                height:'32px'
            }
        }
    },
    comments:{
        flexGrow:'1',
        display:'flex',
        justifyContent:'space-between',
        '& .content':{
            display:'flex',
            gap:'20px',
            '& img':{
                width:'48px',
                height:'48px',
            },
            '& .box-icon':{
                // opacity: 0,
                // visibility: 'hidden',
                transition: 'all .3s ease',
                display: 'flex',
                gap: 'calc(34px - 16px)',
                marginTop: '0',
                // '& *~*':{
                //     marginLeft: 16,
                // },
                '& .icon':{
                    transition: 'all .3s ease',
                    '&:hover':{
                        backgroundColor: COLORS.white,
                        fill: COLORS.blue,
                    },
                    '&:hover .icon-svg': {
                        color: COLORS.blue,
                    },
                }
            },
        }
    },
    products:{
        flexShrink:'0',
        width:'268px',
        display:'flex',
        gap:'20px',
    },
    MessageComponent:{
        position: 'fixed',
        bottom: '-6%',
        opacity: '0',
        visibility: 'hidden',
        maxWidth: 1200,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: '24px 24px 20px',
        margin: '32px 0 -40px',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& .content':{
            display: 'flex',
            alignItems: 'center',
        },
        '&.active': {
            bottom: '4%',
            opacity: '1',
            visibility: 'visible'
        }
    },
    commentReactions: {
        '& .facebook-reaction':{
            display: 'flex',
            flexDirection: 'column',
            position: 'relative',
            transform: 'translateY(0)',
            transition: 'all 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55)',
            background: 'transparent',
            border: 'none',
            cursor: 'pointer',
            outline: 'none',
            '&:hover':{
                transform: 'translateY(-10px) scale(1.5)'
            },
            '&:hover .tooltip': {
                opacity: '1',
            },
        },
        '& svg': {
            width: '40px',
            height: '40px',
        },
        '& .btn-reaction ': {
            background: 'transparent',
            border: 'none',
            cursor: 'pointer',
            outline: 'none',
            padding: '0px '
        },
        '& .tooltip':{
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
            borderRadius: '5px',
            color: 'white',
            fontSize: '0.65rem',
            left: '50%',
            opacity: '0',
            padding: '5px',
            position: 'absolute',
            top: '-25px',
            transform: 'translateX(-50%)',
            transition: 'all 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55)',
        },
        '& .facebook-reactions': {
            display: 'inline-flex',
            listStyleType: 'none',
            padding: '10px',
            border: '1px solid lightgrey',
            borderRadius: '25px',
            boxShadow: '1px 0px 5px 1px #ccc',
            width: 'max-content',
            opacity: '0',
            visibility: 'hidden',
            marginLeft: '12px',
            '&:hover': {
                opacity: '1',
                visibility: 'visible'
            }
        },
        '& .btn-reaction':{
            //padding:'8px'
        },
        '& .btn-reaction:hover ~ .facebook-reactions': {
            opacity: '1',
            visibility: 'visible'
        }

    }
});

export default styles;