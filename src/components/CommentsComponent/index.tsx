import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { IProductComment } from '@containers/Admin/CommentsContainer/types';
import { Box } from '@mui/material';
import ProductComment from './ProductComment';
import DeleteComponent from './DeleteComponent';

interface CommentProps {
    title: string,
    classes: Record<keyof ReturnType<typeof styles>, string>, // optional
    titlePage?: string,
    dataTablePost:IProductComment[],
    keyword: string,
    selections: IProductComment[],
    setKeyword: (keyword : string) => void,
    handlerSearchComment: (e : React.ChangeEvent<HTMLInputElement>) => void,
    handlerDeleteComment: (comments? : IProductComment[]) => void,
    handlerRowSelection: (rows: any) => void,
    onChangeHandleSearchComment: (e : React.ChangeEvent<HTMLInputElement>) => void
}

function CommentsComponent(props: CommentProps) {
    const {title, titlePage, keyword,selections, handlerSearchComment,handlerDeleteComment,handlerRowSelection,onChangeHandleSearchComment, classes, ...otherProps} = props;
    return (
        <Page title={title}
            titlePage={titlePage}
        >
            <Box>
                <ProductComment
                    title = 'Product comments'
                    keyword = {keyword}
                    selections = {selections}
                    handlerSearchComment = {handlerSearchComment}
                    handlerDeleteComment = {handlerDeleteComment}
                    handlerRowSelection = {handlerRowSelection}
                    onChangeHandleSearchComment = {onChangeHandleSearchComment}
                    {...otherProps}
                />
            </Box>
            {
                selections.length > 0 &&
                    <Box>
                        <DeleteComponent selections = {selections}
                            handlerDeleteComment = {handlerDeleteComment}/>
                    </Box>
            }
        </Page>



    );
}

export default withStyles(styles)(CommentsComponent);