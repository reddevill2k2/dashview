import React, { forwardRef, useState } from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import Products from './Products';
import Message from './Message';
import { useForm } from 'react-hook-form';
import { Box } from '@mui/material';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import Button from '@helpers/Button';
import ModalCustom from '@helpers/ModalCustom';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import IconSVG from '@helpers/IconSVG';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { iconCalendar, iconClock } from '@constants/imageAssets';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import DateCustom from '@helpers/DateCustom';
import TimeCustom from '@helpers/TimeCustom';
import RescheduleModal from '@helpers/RescheduleModal';

interface DraffsProps {
    classes:any,
    title: string,
    titlePage?: string,
    tabActiveProduct: number,
    dataTableProducts: any,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleChangeTabActiveProduct: (index: number) =>void,
    handleChangePaginationProducts: ()=> void,
    handleSelectRows: (rows:any) => void,
    handleSelectedItem: (item:any) => void,
    handleDeleteProducts: (selectedRow:any) => void,
    handlePublishProducts:(selectedRow:any) => void,
    handleSetDate:(selectedRow:any) => void,
    handleOpenModalRechedule: () => void,
    handleCloseModalRechedule: () => void,
    handleOpenSelectDate: () => void,
    handleCloseSelectDate: () => void,
    handleOpenSelectTime: () => void,
    handleCloseSelectTime: () => void,
    handlReschedule: () => void,
    SelectDate: boolean,
    openModalRechedule: boolean,
    openModalSelectDate: boolean,
    openModalSelectTime: boolean,
    searchProduct: string,
    selectedRow:any,
    selectedItem: any,
    dateSelected: any,
    pagination: number,
    isLoadingMore:boolean,
}
function DraffsComponent(props: DraffsProps) {
    const {
        title, 
        titlePage, 
        classes,        
        dateSelected ,
        handleCloseModalRechedule,
        handleOpenSelectDate,
        handleCloseSelectDate,
        handleSetDate, 
        handlReschedule,
        handleOpenSelectTime,
        handleCloseSelectTime,
        openModalSelectTime,
        openModalSelectDate,
        openModalRechedule,
        ...otherProps
    } = props;


    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <Products title="Products"
                {...otherProps} />
            <Message {...otherProps} /> 
            <RescheduleModal         
                handleCloseModalRechedule={handleCloseModalRechedule}
                handleOpenSelectDate={handleOpenSelectDate}
                handleCloseSelectDate={handleCloseSelectDate}
                handleSetDate={handleSetDate} 
                handleOpenSelectTime={handleOpenSelectTime}
                handleCloseSelectTime={handleCloseSelectTime}
                openModalSelectTime={openModalSelectTime}
                openModalSelectDate={openModalSelectDate}
                dateSelected={dateSelected}
                handlReschedule={handlReschedule}
                openModalRechedule={openModalRechedule} /> 
        </Page>
    );
}

export default withStyles(styles)(DraffsComponent);