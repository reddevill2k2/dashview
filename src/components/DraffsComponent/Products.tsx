import React, { useMemo } from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid, IconButton } from '@mui/material';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import _ from 'lodash';
import NumberFormat from 'react-number-format';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { v4 as uuidv4 } from 'uuid';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import Button from '@helpers/Button';
import TableCustom from '@helpers/TableCustom';
import { Column } from 'react-table';
import LabelColor from '@helpers/LabelColor';
import { iconListFilled, iconGridLight, iconCalendar, iconEdit, iconTrash, iconClock } from '@constants/imageAssets';
import { truncateString } from './../../utils/index';
import moment from 'moment';

interface IProductList {
    classes:any,
    title: string,
    tabActiveProduct: number,
    dataTableProducts: any,
    handleChangeTabActiveProduct: (index: number) =>void,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleChangePaginationProducts: ()=> void,
    handleDeleteProducts: (selectedRow:any) => void,
    handleSelectRows: (rows:any) => void,
    handleSelectedItem: (item:any) => void,
    selectedRow: any,
    handleOpenModalRechedule: () => void,
    handleCloseModalRechedule: () => void,n
    searchProduct: string,
    pagination: number,
    isLoadingMore:boolean,
    openModalRechedule: boolean,
}

function ProductList(props: IProductList & WithStyles<typeof styles>) {
    const { 
        classes, 
        title,
        tabActiveProduct, 
        dataTableProducts,
        handleOpenModalRechedule, 
        handleCloseModalRechedule,
        handleChangeTabActiveProduct, 
        handleSearchProduct, 
        handleChangePaginationProducts,
        handleDeleteProducts,
        handleSelectRows,
        handleSelectedItem,
        searchProduct,
        selectedRow, 
        openModalRechedule,
        pagination,
        isLoadingMore,
        ...otherProps
    } = props;
    const tabs = [
        {

            icon: iconListFilled,
            indexTab: 0,
        },
        {
            icon: iconGridLight,
            indexTab: 1,
        },

    ];
    const disabled = pagination*6 > dataTableProducts?.length;

    const columnsProducts: Column[] = useMemo(()=> [
        {
            Header: 'Product',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        alignItems={'center'}
                        sx={{cursor:'pointer'}}>
                        <Box className="products-image">
                            <img src={original.products.previewsm}
                                alt={original.products.title}
                                width={80}
                                height={80} />
                        </Box>
                        <Box className="products-content info"
                            sx={{maxWidth:180}}>
                            <Text color={COLORS.grey7}
                                className="products-title"
                                baseB1>
                                {truncateString(original.products.title,32)}
                            </Text>
                            <Text color={COLORS.grey4}
                                caption1>
                                {truncateString(original.products.link,25)}
                            </Text>
                        </Box>
                    </Box>
                );
            },
        },
        {
            Header: 'Price',
            accessor: 'price',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={`$${original.price}`}
                    bg={original.price > 0 ? COLORS.greenNgoc : COLORS.grey3} />);
            }
        },
        {
            Header: 'Last edited',
            minWidth: 300,
            width: 'max-content',
            accessor: 'lastEdited',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box component={'div'}
                        className="last-edited"
                        sx={{display: 'flex', alignItems:'center',justifyContent: 'space-between'}} >

                        <Text color={COLORS.grey4}
                            caption1>
                            {moment(original.lastEdited).format('MMM D, YYYY')} at {moment(original.lastEdited).format('h:mm A')}
                        </Text>
                        <Box component={'div'}
                            className="box-icon"
                            sx={{display:'flex',alignItems:'center'}}>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                                onClick ={() =>{
                                    handleSelectedItem(original);
                                    handleOpenModalRechedule();
                                }}
                            >
                                <IconSVG small
                                    icon={iconCalendar}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                            >
                                <IconSVG small
                                    icon={iconEdit}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                                onClick ={() =>{
                                    handleDeleteProducts([original]);
                                }}
                            >
                                <IconSVG small
                                    icon={iconTrash}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                        </Box>
                    </Box>
                );
            }
        },
       
       
    ], []);
    const renderProductsGrid = (datas:any) =>{

        return(
            datas.slice(0, 6*pagination).map((data:any )=>(

                <Grid item
                    xs={12}
                    md={4}
                    key={uuidv4()}>
                    <Box className="product-item">
                        <Box className='product-image'>
                            <img src={data?.products?.previewxl}
                                alt="" />
                        </Box>
                        <Box className="product-content"
                            display='flex'
                            justifyContent={'space-between'}
                        >
                            <Box className="product-content-left"
                                sx={{alignItems:'center'}}>
                                <Text baseSB1
                                    color={COLORS.grey7}
                                    className="product-title">
                                    {data?.products?.title}
                                </Text>
                                <Box sx={{ marginLeft:'-2px',display:'flex',alignItems:'center',paddingTop:'8px'}}>
                                    <IconSVG icon={iconClock}
                                        fill={COLORS.grey4}/>
                                    <Text base2
                                        sx={{marginLeft:'8px'}}
                                        color={COLORS.grey4}>{moment(data.lastEdited).format('MMM D, YYYY')} at {moment(data.lastEdited).format('h:mm A')}</Text>
                                </Box>
                            </Box>
                            <Box className="product-content-right">
                                <Box className="product-price">
                                    <LabelColor bg={COLORS.greenNgoc} 
                                        color={COLORS.grey5}
                                        title={data.price}/>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Grid>
            ))
        );
    };
    const renderTabProducts = () => {
        return (
            <TableCustom className='table-products'
                data={dataTableProducts}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                columns={columnsProducts}
                handleSelectRow={handleSelectRows}
            />
        );
    };
    const renderTabGrid = () => {
        return (
            <Box>
                <Grid container
                    className="tab-gridviewproduct"
                    spacing={2}>
                    {renderProductsGrid(dataTableProducts)}


                </Grid>
                {!disabled && (
                    <Box className={classes.loadMore} >
                        <ButtonTransparent isLoading={isLoadingMore}
                            disabled={disabled}
                            onClick={handleChangePaginationProducts}>Load More</ButtonTransparent>
                    </Box>
                )}
            </Box>
        );
    };
    return (
        <Box
            component={'div'}
            className={classes.ProductPage + (_.size(selectedRow) > 0 ? ' margin-more' :'')}
        >
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <Box className="content-left"
                    display={'flex'}
                    alignItems={'center'}>

                    <TitleElement bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                    <Input placeholder='Search product'
                        style={{marginLeft:24}}
                        search={searchProduct}
                        handleSearch={handleSearchProduct}
                    />
                </Box>
                <Box className="content-right products-tabs"
                    display={'flex'}>

                    {tabs.map((tab: {
                        icon: string,
                        indexTab: number
                    })=> (
                        <Button key={uuidv4()}
                            onClick={()=> {
                                handleChangeTabActiveProduct(tab.indexTab);
                            }}
                            className={clsx('button-tab', {
                                ['active']: tabActiveProduct === tab.indexTab,
                            })}
                            color={'transparent'}>
                            <IconSVG icon={tab.icon}
                                fill={COLORS.grey4}/>
                        </Button>
                    ))}
                </Box>
            </Box>
            {tabActiveProduct === 0 && (
                renderTabProducts()
            )}

            {tabActiveProduct === 1 && (
                renderTabGrid()
            )}

        </Box>
    );
}

export default withStyles(styles)(ProductList);