import React from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid, IconButton } from '@mui/material';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import { imageProductView } from '@constants/imageAssets';
import SelectCustom from '@helpers/SelectCustom';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import {faker} from '@faker-js/faker';

interface IProductViews {
    title: string,
    labelsProductViews:any,
    fillterProductViewsSelect:string,
    handleSelectProductViews: (e: React.ChangeEvent<HTMLInputElement>) => void,
}

const ProductViewElement = (props: IProductViews & WithStyles<typeof styles>) => {
    const { 
        classes, 
        title, 
        labelsProductViews, 
        fillterProductViewsSelect, 
        handleSelectProductViews, 
        ...otherProps
    } = props;

    ChartJS.register(
        CategoryScale,
        LinearScale,
        BarElement,
        Title,
        Tooltip,
        Legend
    );

    const data = React.useMemo(()=> ({
        labels: labelsProductViews,
        datasets: [
            {
                label: 'Product view',
                data: labelsProductViews.map(() => faker.datatype.number({ min: 5, max: 30 })),
                backgroundColor: [COLORS.blue],
                barPercentage: 0.7,
                hoverBackgroundColor: COLORS.blueHover,
                borderRadius: 4,
            },
        ],
    }), [labelsProductViews]);

    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false
            },
            crosshair: false,
        },
        scales: {
            y: {
                display:false,
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,
                    
                },
                ticks: {
                    stepSize: 5,
                    suggestedMin: 5,
                }
            },
        }
    };
    
    const dataSelectTime = [
        {
            label: 'Last 7 days',
            value: 'Last 7 days',
        },
        {
            label: 'This month',
            value: 'This month',
        },
        {
            label: 'All time',
            value: 'All time',
        },
    ];
   
    return (
        <Box
            component={'div'}
            className={classes.productViews}
        >
            <Grid container
                className={classes.boxHeader}>
                <Grid item>
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                </Grid>
                <Grid item
                    component={'div'}

                >
                    <SelectCustom select={fillterProductViewsSelect}
                        data={dataSelectTime}
                        handleSelect={handleSelectProductViews}
                        defaultValues={'All time'}/>
                </Grid>
            </Grid>
            <Box
                className={'render-chart'}
            >
                <Bar options={options}
                    data={data} />
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ProductViewElement);