import React from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, Tooltip } from '@mui/material';
import Button from '@helpers/Button';
import Overview from './Overview';
import ProductActivity from './ProductActivity';
import Products from './Products';
import ProductViews from './ProductViews';
import ModalCustom from '@helpers/ModalCustom';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import SelectCustom from '@helpers/SelectCustom';

interface DashboardProps {
    title: string,
    titlePage?: string | undefined,
    tabActiveProduct: number,
    dataTableMarket: any,
    dataChartFaker:any,
    handleChangeTabActiveProduct: (index: number)=> void,
    handleChangeStatusProducts: (selectedRow:any,status:string)=> void,
    handleDeleteProducts: (selectedRow:any) => void,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleSelectRows: (rows: any)=> void,
    handleSelectOverview: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleSelectProductActivity: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleSelectProductViews: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleSelectProductsStatus: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleOpenModalStatus:() => void,
    handleCloseModalStatus:() => void,
    openModalStatus:boolean,
    fillterOverviewSelect: string,
    fillterProductActivitySelect: string,
    fillterProductViewsSelect:string,
    labelsProductViews:any,
    searchProduct: string,
    selectedRow:any,
    productsStatus:string,

}

function DashboardComponent(props: DashboardProps & WithStyles<typeof styles>) {
    const {
        title,
        classes, 
        titlePage,
        openModalStatus,
        handleCloseModalStatus,
        handleSelectProductsStatus,
        selectedRow,
        handleChangeStatusProducts,
        productsStatus, 
        ...otherProps
    } = props;
    const dataSelectstatus = [
        {
            label: 'Active',
            value: 'Active',
        },
        {
            label: 'Deactive',
            value: 'Deactive',
        },
    ];
    const ContentModal =  () => {
        return (
        // eslint-disable-next-line react/prop-types
            <Box className={classes.contentModal}>
                <Box textAlign={'center'}
                    display='flex'
                    justifyContent={'center'}
                >
                </Box>
                <Box className="box-content-text">
                    <Text bodyM1
                        color={COLORS.grey4}>
                      Choose status in the future you want your product to be published.
                    </Text>
                    <Box className="box-calendar">
                        <SelectCustom defaultValues='Active'
                            handleSelect={handleSelectProductsStatus}
                            select={productsStatus}
                            data={dataSelectstatus}/>
                    </Box>
                    <Box className='box-button-custom'
                        textAlign={'center'}>
                        <Button color={'primary'}
                            onClick={() =>{
                                handleChangeStatusProducts(selectedRow,productsStatus);
                                handleCloseModalStatus();
                            }}>Set Status</Button>
                    </Box>
                </Box>
            </Box>
        );
    };
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <Box className={classes.productDashboardPage}>
                <Overview title='Overview'
                    {...otherProps}/>
                <Grid container
                    display={'flex'}
                    alignItems={'center'}
                    spacing={2}>
                    <Grid item
                        md={8}
                        xs={12}>
                        <ProductActivity title='Product activity'
                            {...otherProps}/>
                    </Grid>
                    <Grid item
                        md={4}
                        xs={12}>
                        <ProductViews title={'Product views'}
                            {...otherProps}/>
                    </Grid>
                </Grid>
                <Products title={'Products'}
                    selectedRow={selectedRow}
                    {...otherProps}/>
                <ModalCustom
                    open={openModalStatus}
                    onClose={() =>{
                        handleCloseModalStatus();
                    }}
                    title={'Status change product'}
                    bgTitle={COLORS.orangeLight}
                    width={390}
                    height={240}
                >
                    {ContentModal()}
                </ModalCustom>
            </Box>
        </Page>
    );
}

export default withStyles(styles)(DashboardComponent);