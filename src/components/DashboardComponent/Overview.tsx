import React, { useEffect, useState } from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, IconButton, Tooltip } from '@mui/material';
import TitleElement from '@helpers/TitleElement';
import { COLORS } from '@constants/colors';
import SelectCustom from '@helpers/SelectCustom';
import IconSVG from '@helpers/IconSVG';
import iconActivityFilled from '@assets/icons/activity/filled.svg';
import { iconChevronDown, iconChevronUp, iconDown, iconInfo, iconPaymentLight, iconShoppingBag, iconUp, imageSmallLine1, imageSmallLine2, imageSmallLine3 } from '@constants/imageAssets';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import Text from '@helpers/Text';
import TooltipCustom from '@helpers/TooltipCustom';
import LabelColor from '@helpers/LabelColor';
import faker from '@faker-js/faker';

interface IOverviewComponent {
    title: string,
    dataChartFaker:any,
    fillterOverviewSelect:string,
    handleSelectOverview:(e: React.ChangeEvent<HTMLInputElement>) => void,
}

interface IChartItem {
    icon: any,
    iconChart: any,
    title: string,
    quantity: string,
    mode: string,
    numeral: string,
    titleMode: string,
    bg: string,
}

function OverviewComponent(props: IOverviewComponent & WithStyles<typeof styles>) {
    const {title,
        classes, 
        dataChartFaker, 
        fillterOverviewSelect, 
        handleSelectOverview, 
        ...otherProps} = props;
    const dataSelect = [
        {
            value: 'week',
            label: 'This week',
        },
        {
            value: 'month',
            label: 'This month',
        },
        {
            value: 'year',
            label: 'This year',
        },

    ];
    const renderChart = (data: IChartItem) => {
        const {icon,
            iconChart,
            title,
            quantity,
            mode,
            numeral,
            titleMode, 
            bg,
        } = data;
        return (
            <Grid item
                md={4}
                xs={12}
                key={uuidv4()}
                className='item-chart'>
                <Box sx={{
                    backgroundColor: bg,
                    padding: 4,
                    borderRadius: '12px',
                }}>
                    <IconButton className='icon'>
                        <IconSVG icon={icon}
                            fill={COLORS.white}/>
                    </IconButton>
                    <Box
                        display={'flex'}
                        justifyContent='space-between'
                        className="chart-info">
                        <Box>
                            <Box display={'flex'}>
                                <Text caption1
                                    sx={{marginRight: 0.5,}}
                                    color={COLORS.grey5}>
                                    {title}
                                </Text>
                                <TooltipCustom title='Small description'>
                                    <IconSVG icon={iconInfo}
                                        width={16}
                                        height={16}
                                        fill={COLORS.grey5}/>
                                </TooltipCustom>
                            </Box>
                            <Text h2
                                color={COLORS.grey7}
                                sx={{marginTop: '-14px'}}>
                                {quantity}
                            </Text>
                            <LabelColor 
                                sx={{marginLeft: '-14px'}}
                                mode={mode}
                                titleMode={titleMode}
                                numeral={numeral}
                            />
                        </Box>
                        <img className='image-chart'
                            src={iconChart}
                            width='80'
                            alt="" />
                    </Box>
                </Box>
            </Grid>
        );
    };

    return (
        <Box className={classes.overviewComponent}>
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <TitleElement color={COLORS.grey7}
                    bg={COLORS.violetLight}>
                    {title}
                </TitleElement>
                <SelectCustom defaultValues='week'
                    handleSelect={handleSelectOverview}
                    select={fillterOverviewSelect}
                    data={dataSelect}/>
            </Box>
            <Grid container
                spacing={2}
                className="list-chart">
                {!_.isEmpty(dataChartFaker) && dataChartFaker.map((chart: IChartItem)=> (
                    renderChart(chart)
                ))}
            </Grid>
        </Box>
    );
}

export default withStyles(styles)(OverviewComponent);