import React from 'react';
import {WithStyles, withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid, IconButton } from '@mui/material';
import Button from '@helpers/Button';
import { v4 as uuidv4 } from 'uuid';
import SelectCustom from '@helpers/SelectCustom';
import IconSVG from '@helpers/IconSVG';
import { iconFilterLight, iconStarFilled } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import _ from 'lodash';
import Text from '@helpers/Text';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import clsx from 'clsx';

interface ShopProps {
    titlePage?: string,
    dataListShop: any,
    handleChangeTabActive: (e: number)=> void,
    tabActive: number,
    pagination: number,
    handleChangePaginationProducts: ()=> void,
    isLoadingMore:boolean,
    dataListFollower: any,
    dataListFollowing: any,
}

interface IDataProducts {
    title: string,
    price: string,
    rating: string,
    totalRating: string,
    preview: any,
    url: '',
}
interface IDataFollowers {
    name: string,
    totalProduct:number,
    isMessage:boolean,
    totalFollower: number,
    preview: string,
    listProduct: string[],
}


function ShopComponent(props: ShopProps  & WithStyles<typeof styles>) {
    const {titlePage,
        classes,
        dataListShop, 
        handleChangeTabActive, 
        tabActive, 
        handleChangePaginationProducts,
        pagination,
        isLoadingMore,
        dataListFollower,
        dataListFollowing,
        ...otherProps} = props;

    const listTitle = [
        {
            title: 'Products',
            indexActive: 0,
        },
        {
            title: 'Followers',
            indexActive: 1,
        },
        {
            title: 'Following',
            indexActive: 2,
        },
    ];

    const dataFilters = [
        {
            value: 'mostRecent',
            label: 'Most recent'
        },
        {
            value: 'mostNew',
            label: 'Most new'
        },
        {
            value: 'mostPopular',
            label: 'Most popular'
        },
    ];

    const renderListProducts = () => {

        return (
            !_.isEmpty(dataListFollower) && dataListShop.slice(0, 9*pagination).map((product: IDataProducts)=> (
                <Grid key={uuidv4()}
                    item
                    xs={12}
                    md={4}>
                    <Box className="product-item">
                        <Box className='product-image'>
                            <img src={product.preview}
                                alt="" />
                        </Box>
                        <Box className="product-content"
                            display='flex'
                            justifyContent={'space-between'}
                        >
                            <Box className="product-content-left">
                                <Text baseSB1
                                    color={COLORS.grey7}
                                    className="product-title">
                                    {product.title}
                                </Text>
                                <Box display={'flex'}
                                    alignItems={'center'}>
                                    <IconSVG icon={iconStarFilled}
                                        fill={COLORS.yellowLight}/>
                                    <Text base2
                                        color={COLORS.grey7}>{product.rating}</Text>
                                    <Text base2
                                        color={COLORS.grey4}>({product.totalRating})</Text>
                                </Box>
                            </Box>
                            <Box className="product-content-right">
                                <Box className="product-price">
                                    <Text baseB1
                                        color={COLORS.grey7}>{product.price}</Text>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Grid>
            ))
        );
    };
    const renderListFollowers = () => {

        return (
            !_.isEmpty(dataListFollower) && dataListFollower.slice(0, 9*pagination).map((product: IDataFollowers)=> (

                <React.Fragment key={uuidv4()}>
                    <Box className="follower-item"
                        display={'flex'}
                        justifyContent='space-between'>
                        <Box className='follower-left'>
                            <Box display='flex'>
                                <Box className="follower-user">
                                    <img src={product.preview}
                                        width={80}
                                        height={80}
                                        alt="" />
                                </Box>
                                <Box className={'follower-content'}>
                                    <Text titleSB1
                                        color={COLORS.grey7}>{product.name}</Text>
                                    <Box className='follower-data'
                                        display='flex'>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalProduct} products</Text>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalFollower} followers</Text>
                                    </Box>
                                    <Box display='flex'>
                                        <ButtonTransparent>Unfollow</ButtonTransparent>
                                        {product.isMessage && <Button color={'primary'}>Message</Button>}
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                        <Box className='follower-right'
                            display={'flex'}>
                            {!_.isEmpty(product.listProduct) && product.listProduct.map((pic)=> (
                                <Box className='follower-img'
                                    key={uuidv4()}>
                                    <img src={pic}
                                        alt=""
                                        className="cursor-pointer" />
                                </Box>
                            ))}
                        </Box>
                    </Box>
                    <Divider/>
                </React.Fragment>
            ))
        );
    };
    const renderListFollowing = () => {

       
        return (
            !_.isEmpty(dataListFollowing) && dataListFollowing.slice(0, 9*pagination).map((product: IDataFollowers)=> (

                <React.Fragment key={uuidv4()}>
                    <Box className="follower-item"
                        display={'flex'}
                        justifyContent='space-between'>
                        <Box className='follower-left'>
                            <Box display='flex'>
                                <Box className="follower-user">
                                    <img src={product.preview}
                                        width={80}
                                        height={80}
                                        alt="" />
                                </Box>
                                <Box className={'follower-content'}>
                                    <Text titleSB1
                                        color={COLORS.grey7}>{product.name}</Text>
                                    <Box className='follower-data'
                                        display='flex'>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalProduct} products</Text>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalFollower} followers</Text>
                                    </Box>
                                    <Box display='flex'>
                                        <ButtonTransparent>Unfollow</ButtonTransparent>
                                        {product.isMessage && <Button color={'primary'}>Message</Button>}
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                        <Box className='follower-right'
                            display={'flex'}>
                            {!_.isEmpty(product.listProduct) && product.listProduct.map((pic)=> (
                                <Box className='follower-img'
                                    key={uuidv4()}>
                                    <img src={pic}
                                        alt=""
                                        className="cursor-pointer" />
                                </Box>
                            ))}
                        </Box>
                    </Box>
                    <Divider/>
                </React.Fragment>
            ))
        );
    };

    const disabled = pagination*9 > dataListShop?.length;
    return (
        <Box className={classes.listShoppingComponent}>
            <Box display='flex'
                alignItems={'center'}
                justifyContent={'space-between'}> 
                <Box className="list-tab">
                    {listTitle.map((item: any)=> (
                        <Button key={uuidv4()}
                            color='transparent'
                            onClick={()=>handleChangeTabActive(item.indexActive)}
                            className={clsx('tab-item', 
                                {['active']: tabActive === item.indexActive})}>
                            {item.title}
                        </Button>
                    ))}
                </Box>
                <Box className="filters">
                    <SelectCustom data={dataFilters}
                        defaultValues='mostRecent'/>
                    <Box className="box-all-filter">
                        <IconButton className='icon-filter'>
                            <IconSVG icon={iconFilterLight}
                                fill={COLORS.grey4}/>
                        </IconButton>
                    </Box>
                </Box>
            </Box>
            <Grid container
                spacing={2}
                className={'product-list'}>
                {tabActive === 0 && renderListProducts()}
                
            </Grid>
            <Box className='follower-list'>
                {tabActive === 1 && renderListFollowers()}
                {tabActive === 2 && renderListFollowing()}
            </Box>
            {!disabled && (
                <Box className={classes.loadMore}>
                    <ButtonTransparent isLoading={isLoadingMore}
                        disabled={disabled}
                        onClick={handleChangePaginationProducts}>Load More</ButtonTransparent>
                </Box>
            )}
            
        </Box>
    );
}

export default withStyles(styles)(ShopComponent);