import React from 'react';
import {WithStyles, withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import HeaderPage from './HeaderPage';
import ListShopping from './ListShopping';
import { Box, Divider } from '@mui/material';
import { imageCoverShop } from '@constants/imageAssets';

interface ShopProps {
    title: string,
    titlePage?: string,
    dataListShop:any,
    handleChangeTabActive: (e: number)=> void,
    tabActive: number,
    pagination: number,
    handleChangePaginationProducts: ()=> void,
    isLoadingMore:boolean,
    dataListFollower: any,
    dataListFollowing: any,
}

function ShopComponent(props: ShopProps  & WithStyles<typeof styles>) {
    const {title, classes, ...otherProps} = props;
    return (
        <Page title={title}
            bgCoverPage={imageCoverShop}
            {...otherProps}>
            <Box className={classes.ShoppingPage}>
                <HeaderPage/>
                <Divider className={classes.dividerStyle}/>
                <ListShopping
                    {...otherProps} />
            </Box>
        </Page>
    );
}

export default withStyles(styles)(ShopComponent);