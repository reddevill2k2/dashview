import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    ShoppingPage: {
        zIndex: 20,
        position: 'relative',
        borderRadius: '8px',
        backgroundColor: COLORS.white,
        padding: 24,
        marginTop: -60,
    },
    listShoppingComponent: {
        '& .filters': {
            display: 'flex',
            alignItems: 'center',
            '& button': {
                borderRadius: '8px',
            },
            '& .box-all-filter': {
                width: 40,
                height: 40,
                border: `2px solid ${COLORS.grey3}`,
                borderRadius: '8px',
                marginLeft: 16,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                '&:hover .icon-svg': {
                    color: COLORS.grey7,
                }
            }
        },
        '& .product': {
            '&-list': {
                marginTop: 32,
            },
            '&-item': {
                marginBottom: 32,
            },
            '&-content': {
                marginTop: 16,
                '&-left': {

                },
                '&-right': {
                    '& .product-price': {
                        borderRadius: '6px',
                        height: 32,
                        padding: '4px 8px',
                        backgroundColor: COLORS.greenLight,
                        textAlign: 'center',
                        '& .text': {
                            marginRight: 0,
                        }
                    }
                },
                '& .icon-svg': {
                    marginRight: 8,
                },
                '& .text': {
                    marginRight: 8,
                }
            },
            '&-image': {
                width: '100%',
                height: 230,
                overflow: 'hidden',
                borderRadius: '16px',
                position: 'relative',
                '&::before': {
                    content: '""',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                    backgroundColor: 'transparent',
                    transition: 'all .3s ease',
                    zIndex: -1,
                },
                '& img': {
                    width: 'auto',
                    height: 'calc(100% + 10px)',
                    objectFit: 'contain',
                },
                '&:hover::before': {
                    zIndex: 1,
                    backgroundColor: '#46464661',
                },
            },
            '&-title': {
                marginBottom: 8,
                color: COLORS.grey7
            },

        },
        '& .list-tab': {
            '& .tab-item': {
                '&.active': {
                    backgroundColor: COLORS.grey3
                }
            }
        },

        //followers
        '& .follower-list': {
            marginBottom: 32,
        },
        '& .follower-list .follower': {
            '&-item': {
                width: '100%',
                marginTop: 25,
                marginBottom: 25,
            },
            '&-user': {
                marginRight: 16,
                width: 80,
                height: 80,
                borderRadius: '50%',
                overflow: 'hidden',
                '& img': {
                    width: '100%',
                    height: '100%',
                    objectFit: 'contain',
                }
            },
            '&-content': {
                '& .button-transparent': {
                    marginRight: 16,
                },
            },
            '&-data': {
                marginTop: 4,
                marginBottom: 16,
                '& .text:nth-child(1)': {
                    marginRight: 12,
                    paddingRight: 12,
                    borderRight: `2px solid ${COLORS.grey2}`
                },
            },
            '&-right': {
                display: 'flex',
                '& .follower-img': {
                    height: 116,
                    width: 148,
                    marginRight: 12,
                    borderRadius: 12,
                    overflow: 'hidden',
                    '& img': {
                        width: '100%',
                        height: '100%',
                        objectFit: 'cover',
                        display: 'inline-block',
                        
                    }
                }
            }
        },
    },
    dividerStyle: {
        margin: '48px 0 !important',
    },
    headerPageComponent: {
        '& .header-left': {
            '&-image': {
                marginRight: 16,
                position: 'relative',
                '& img': {

                },
                '& .button-add': {
                    position: 'absolute',
                    bottom: 6,
                    right: 0,
                    border: `2px solid ${COLORS.white}`,
                    backgroundColor: COLORS.blue,
                    width: 24,
                    height: 24,
                    borderRadius: '24px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    '& svg': {
                        width: 16,
                        height: 16,
                    }
                }
            },
            '&-content': {  
                '& .title': {
                    marginBottom: 8,
                }
            }
        },
        '& .header-right': {
            '& .list-icon': {
                '& .icon-social': {
                    marginRight: 32,
                    display: 'inline-block',
                    '&:hover .icon-svg': {
                        color: COLORS.grey7,
                    }
                }
            }
        },
       
    },
    loadMore: {
        display: 'table',
        margin: ' 0 auto 32px auto',
    }
});

export default styles;