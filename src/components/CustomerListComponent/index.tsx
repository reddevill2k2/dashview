import React from 'react';
import { withStyles } from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import CustomerList from './CustomerList';
import Message from './Message';
import ModalCustom from '@helpers/ModalCustom';
import { COLORS } from '@constants/colors';
import { Box, Divider, Slider } from '@mui/material';
import Input from '@helpers/Input';
import Text from '@helpers/Text';
import SelectCustom from '@helpers/SelectCustom';
import { v4 as uuidv4 } from 'uuid';
import CheckboxCustom from '@helpers/CheckboxCustom';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Button from '@helpers/Button';
import { useForm } from 'react-hook-form';

interface CustomerListProps {
    title: string;
    classes:any;
    titlePage?: string;
    handleChangeTabActiveCustomer: (index: number)=> void;
    tabActiveCustomer: number;
    dataTableNewCustomers: any;
    dataTableActiveCustomers: any;
    handleSearchCustomer: (e: React.ChangeEvent<HTMLInputElement>) => void;
    searchCustomer: string;
    openModalFilter: boolean;
    handleOpenModalFilter: () => void;
    handleCloseModalFilter: () => void;
    refFilter: HTMLButtonElement,
    positionModal: DOMRect,
    handleChangeProgressBarPrice: (event: Event, newValue: number | number[]) => void,
    handleResetProgressBarPrice: () => void,
    progressBarPrice: number[],
}

function CustomerListComponent(props: CustomerListProps) {
    const { title, titlePage,classes, openModalFilter,
        handleOpenModalFilter, handleCloseModalFilter, 
        refFilter, positionModal, 
        handleChangeProgressBarPrice,
        handleResetProgressBarPrice,
        progressBarPrice,
        ...otherProps } = props;

    const dataSelectSort = [
        {
            label: 'Featured',
            value: 'Featured',
        },
        {
            label: 'Last',
            value: 'Last',
        },
        {
            label: 'New',
            value: 'New',
        },
    ];

    const dataListCheckbox = [
        {
            label: 'All Products',
            value: 'All Products',
        },
        {
            label: 'UI kit',
            value: 'UI kit',
        },
        {
            label: 'Illustration',
            value: 'Illustration',
        },
        {
            label: 'WireFrame kit',
            value: 'WireFrame kit',
        },
        {
            label: 'Icons',
            value: 'Icons',
        },
    ];

    const renderContentModalFilter = () => {
        const {register} = useForm();
        return (
            <Box className={classes.contentModalFilter}>
                
                <Box className="box-input-search">
                    <Input 
                        placeholder='Search for products'
                        {...register('search')}
                    />
                </Box>
                
                <Box className="box-sortBy">
                    <Text base2
                        color={COLORS.grey4}>
                    Sort by
                    </Text>
                    <SelectCustom 
                        style={{
                            marginTop: 12,
                        }}
                        data={dataSelectSort}
                        defaultValues={'Featured'}
                    />
                </Box>
                
                <Box className="list-checkbox">
                    <Text base2
                        color={COLORS.grey4}>Showing</Text>
                    <Box
                        className='list-item'>
                        {
                            dataListCheckbox.map((checkbox: {
                                label: string,
                                value: string,
                            }) => (
                                <Box key={uuidv4()}
                                    className='box-item'>
                                    <Text baseB1
                                        color={COLORS.grey7}>{checkbox.label}</Text>
                                    <CheckboxCustom className={'cursor-pointer'}/>
                                </Box>
                            ))
                        }
                    </Box>
                    
                    <Box className="box-price">
                        <Text base2
                            color={COLORS.grey4}>
                        Price
                        </Text>
                        <Box className='price-select'>
                            <Slider
                                classes={{
                                    root: 'progress-bar',
                                }}
                                getAriaLabel={() => 'Minimum distance shift'}
                                value={progressBarPrice}
                                onChange={handleChangeProgressBarPrice}
                                getAriaValueText={(val) => `$${val}`}
                                valueLabelDisplay="on"
                                disableSwap
                            />
                        </Box>
                        <Box className="group-button"
                            display={'flex'}
                            justifyContent={'flex-end'}>
                            <Box display={'flex'}>
                                <ButtonTransparent style={{marginRight: 12}}
                                    onClick={handleResetProgressBarPrice}>Reset</ButtonTransparent>
                                <Button color='primary'>Apply</Button>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>
        );
    };
    
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <CustomerList title={'Customer'}
                handleOpenModalFilter={handleOpenModalFilter}
                handleCloseModalFilter={handleCloseModalFilter}
                openModalFilter={openModalFilter}
                refFilter={refFilter}
                ContentModal={renderContentModalFilter}
                {...otherProps} />
            <Message title={'Message'}
                {...otherProps} />
        </Page>
    );
}
export default withStyles(styles)(CustomerListComponent);
