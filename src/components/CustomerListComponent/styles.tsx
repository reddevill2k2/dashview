import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
        
    },

    CustomerPage:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: '24px 24px 2px',
        marginBottom: 8,
        width: '100%',
        '& .table-customer': {
            marginTop: 32,
            '& tr td:nth-child(1)': {
                verticalAlign: 'top',
            },
            '& .Customer-image': {
                marginRight: 20,
            },
            '& .Customer-content': {
                minWidth: 200,
            },
            '& .box-mode': {
                backgroundColor: 'transparent',
            },
            '& .tooltip-traffic': {
                backgroundColor: COLORS.grey6,
                borderRadius: '6px',
                height: 48,
                width: 62,
                color: `${COLORS.white} !important`,
            }
        },
        '& .customers-tabs': {
            '& .button-tab': {
                height: 40,
                borderRadius: '8px',
                padding: '8px 16px',
                backgroundColor: 'transparent',
                color: COLORS.grey4,
                transition: 'all .3s ease',
                '& .text': {
                    color: `${COLORS.grey4} !important`,
                },
                '&.active': {
                    color: COLORS.grey7,
                    backgroundColor: COLORS.grey3,
                    '& .text': {
                        color: `${COLORS.grey7} !important`,
                    }
                }
            },
            '& .box-all-filter': {
                width: 40,
                height: 40,
                border: `2px solid ${COLORS.grey3}`,
                borderRadius: '8px',
                marginLeft: 16,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                '&:hover .icon-svg': {
                    color: COLORS.white,
                },
                '&:hover':{
                    backgroundColor: COLORS.blue,
                    border: `2px solid ${COLORS.blue}`,

                }
            }
        }
    },
    MessageComponent:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: '24px 24px 20px',
        margin: '32px 0 -40px',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& .content':{
            display: 'flex',
            alignItems: 'center',
        }
    },
    contentModalFilter: {
        '& .box-input-search': {
            padding: '10px 0',
        },
        '& .box-sortBy': {
            padding: '10px 0',
        },
        '& .list-checkbox': {
            padding: '10px 0',
            '& .list-item': {
                marginTop: 6,
                marginBottom: 6,
                '& .box-item': {
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: '6px 0',
                    position: 'relative',
                    '& .checkbox-custom': {
                        position: 'absolute',
                        width: '100%',
                        textAlign: 'right',
                    }
                }
            }
        },
        '& .box-price': {
            padding: '10px 0',
            '& .progress-bar': {
                color: COLORS.blue,
                marginTop: 40,
            },
            '& .MuiSlider-track': {
                border: 'unset !important',
            },
            '& .MuiSlider-rail': {
                backgroundColor: `${COLORS.grey3} !important`,
            },
            '& .MuiSlider-valueLabel': {
                background: COLORS.grey6,
                color: COLORS.white,
                borderRadius: '4px',
                padding: '2px 8px',
                height: 28,
                minWidth: 40,
                boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1), inset 0px 0px 1px #000000',
                '& .MuiSlider-valueLabelLabel': {
                    color: COLORS.white,
                    fontWeight: 600,
                    fontSize: 12,
                    lineHeight: 24,
                }
            },
            '& .MuiSlider-thumb': {
                width: '16px !important',
                height: '16px !important',
                '&:after': {
                    width: '6px !important',
                    height: '6px !important',
                    background: COLORS.white,
                }
            }
        }
    }
});

export default styles;