import React from 'react';
import {withStyles,WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, IconButton } from '@mui/material';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { iconCheckAll } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import Button from '@helpers/Button';


interface MessageProps {
    title: string,
    classes?: any,
}

function MessageComponent(props: MessageProps & WithStyles<typeof styles>) {
    const {title, classes, ...otherProps} = props;
    return (
        <Box className={classes.MessageComponent}>
            
            <Box component={'div'}
                className='content'>
                <IconButton className='icon-basket'>
                    <IconSVG icon={iconCheckAll}/>
                </IconButton>
                <Text caption1
                    color={COLORS.grey4}>
                    <b>2 customers </b>selected
                </Text>
            </Box>
            <Button 
                color={'primary'}
            >Message</Button>
        </Box>
    );
}

export default withStyles(styles)(MessageComponent);