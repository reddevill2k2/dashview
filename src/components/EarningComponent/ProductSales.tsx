import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Grid, Box } from '@mui/material';
import TitleElement from '@helpers/TitleElement';
import { COLORS } from '@constants/colors';
import SelectCustom from '@helpers/SelectCustom';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { faker } from '@faker-js/faker';


interface IProductSalesProps {
    title: string,
    phone?: number, // optional
    classes: any,
}

function ProductSalesComponent(props: IProductSalesProps) {
    const {title, classes, ...otherProps} = props;
    ChartJS.register(
        CategoryScale,
        LinearScale,
        BarElement,
        Title,
        Tooltip,
        Legend
    );

    const labels =[ '22', '23', '24', '25', '26', '27', '28'];
    const data = React.useMemo(() =>({
        labels,
        datasets:[
            {
                barPercentage: 0.7,
                borderRadius: 4,
                label: 'Product sales',
                data: labels.map(() => faker.datatype.number({ min:500,max:3200})),
                backgroundColor: COLORS.greenNgoc,
                hoverBackgroundColor: COLORS.greenNgocHover
            },
        ],
    }),[]);
    const options = {

        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false
            },
            crosshair: false,
        },
        scales: {
            x: {
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,
                },
            },
            y: {
                grid: {
                    drawBorder: false,

                },
                ticks: {
                    stepSize: 800,
                    suggestedMin: 5,
                }
            },
        }
    };
    const dataSelectTime = [
        {
            label: 'Last 7 days',
            value: 'Last 7 days',
        },
        {
            label: 'This month',
            value: 'This month',
        },
        {
            label: 'All time',
            value: 'All time',
        },
    ];

    return (
        <Box component={'div'}
            className={classes.productSales}>
            <Grid container
                className={classes.boxHeader}>
                <Grid item>
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.violetLight} >{title}</TitleElement>
                </Grid>
                <Grid item
                    component={'div'} >
                    <SelectCustom data={dataSelectTime}
                        defaultValues="Last 7 days"/>
                </Grid>
            </Grid>
            <Box
                className={'render-chart'}
            >
                <Bar options={options}
                    data={data} />
            </Box>
        </Box>
    );
}

export default withStyles(styles)(ProductSalesComponent);