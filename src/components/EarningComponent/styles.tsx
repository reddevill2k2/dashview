import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
  
    },
    EarningPage: {
        width: '100%',
    },
    overviewComponent: {
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: 24,
        marginBottom: 8,
        width: '100%',
        '& .box-title-element': {
            padding: '5px 0',
        },
        '& .list-earning': {
            marginTop: 32,
        },
        '& .item-earning': {
            padding : '16px 32px 16px 64px !important',
            '&:last-child .item-box': {
                borderRight:'0 !important'
            },
            '& .item-box':{
                borderRight:`1px solid ${COLORS.grey3}`,

            },
            '& .icon': {
                width: 48,
                height: 48,
                borderRadius: '48px',
            },
            '& .earning-info': {
                marginTop: 16,
                '& .icon-mode': {
                    width: 16,
                    height: 16,
                },
                '& .image-earning': {
                    objectFit: 'contain',
                    height: 50,
                },
                '& .icon-svg svg': {
                    width: 16,
                    height: 16,
                }
            }
        }
    },
    productSales:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: 24,
        marginBottom: 8,
        width: '100%',
        '& .render-chart': {
            height: 320,
            marginTop: 32,
            '& img': {
                width: '100%',
                objectFit: 'cover',
            }
        }
    },
    boxHeader: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    topContries:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: 24,
        marginBottom: 8,
        width: '100%',
        '& .table':{
            padding: '36px 0 8px',
            '& .item:not(:last-child)': {
                borderBottom:`1px solid ${COLORS.grey3}`,
                paddingBottom:'12px',
                marginBottom:'12px',
            }
        },
    },
    earningReports:{
        backgroundColor: COLORS.white,
        paddingBottom: 24,
        borderRadius: 8,
        marginBottom: 8,
        width: '100%',
        '& .table-results':{
            '& tr':{

                '& td, & th':{ 
                    border: 'hidden', 
                },
            },
            '& tbody tr:nth-child(2n+1)':{
                backgroundColor: COLORS.grey9,
            },
        },
    },
});

export default styles;