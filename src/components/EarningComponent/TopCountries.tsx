import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Grid, Box } from '@mui/material';
import TitleElement from '@helpers/TitleElement';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import { v4 as uuidv4 } from 'uuid';
import * as _ from 'lodash';

interface ITopContries {
    title: string,
    classes:any,
    dataTableContries:any,
}
interface ITopContriesItem {
    code:string,
    name:string,
    earnings:string,
}
const renderDataTopContries = (data:ITopContriesItem) =>{
    const {
        code,name,earnings
    } = data;
    return (
        <Box component={'div'}
            key={uuidv4()}
            className="item"
            sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent:'space-between',
            }}
        >
            <Box className="contry-code"
                sx={{
                    borderRadius:'52px',
                    padding: '4px',
                    width: '32px',
                    marginRight:'12px',
                    backgroundColor:`${COLORS.grey3}`,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent:'center',
                }}>
                <Text base2
                    color={COLORS.grey5}>{code}</Text>
            </Box>
            <Box className="contry-name"
                sx={{
                    textAlign:'left',
                    flex:1,
                    marginRight:'4px'
                }}>
                <Text baseSB1 
                    color={COLORS.grey5}>{name}</Text>
            </Box>
            <Box className="contry-earnings"
                sx={{
                    display:'flex',
                    justifyContent: 'flex-end',
                }}>
                <Text baseSB1
                    color={COLORS.grey7}>{earnings}</Text>
            </Box>

        </Box>
    );
};
function TopContriesComponent(props: ITopContries) {
    const {title, classes, dataTableContries, ...otherProps} = props;
    return (
        <Box component={'div'}
            className={classes.topContries}>
            <TitleElement color={COLORS.grey7}
                bg={COLORS.blueLight}>{title}</TitleElement>
            <Box component={'div'}
                className='table'>
                {!_.isEmpty(dataTableContries) && dataTableContries.map((data: ITopContriesItem)=> (
                    renderDataTopContries(data)
                ))}
            </Box>
        </Box>
    );
}

export default withStyles(styles)(TopContriesComponent);