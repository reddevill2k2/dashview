import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Grid, IconButton } from '@mui/material';
import { iconActivityFilled, iconShoppingBag, iconInfoFilled, iconPieChar } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import { Box } from '@mui/system';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import TooltipCustom from '@helpers/TooltipCustom';
import LabelColor from '@helpers/LabelColor';
import { v4 as uuidv4 } from 'uuid';
import * as _ from 'lodash';

interface IOverviewComponent {
    title: string,
    phone?: number, // optional
    titlePage?: string,
    classes?:any,
}

interface IOverviewItem {
    icon: any,
    title: string,
    quantity: string,
    mode: string,
    numeral: string,
    titleMode: string,
    bg: string,
    col:number,
}
function OverviewComponent(props: IOverviewComponent) {
    const {title, titlePage, classes, ...otherProps} = props;
    
    const listOverviewDummyData = [
        {
            icon: iconActivityFilled,
            title: 'Earning',
            quantity: '$128k',
            mode: 'up',
            numeral: '39.9%',
            titleMode: 'this week',
            bg: COLORS.greenLight,
            col:4.5,
        },
        {
            icon: iconPieChar,
            title: 'Balance',
            quantity: '$512.64',
            mode: 'down',
            numeral: '34.9%',
            titleMode: 'this week',
            bg: COLORS.violetLight,
            col:4.5,

        },
        {
            icon: iconShoppingBag,
            title: 'Total value of sales',
            quantity: '$64k',
            mode: 'up',
            numeral: '41.0%',
            titleMode: 'this week',
            bg: COLORS.blueLight,
            col:3,

        },
    ];
    const renderListOverview =(data:IOverviewItem) =>{
        const {icon,
            title,
            quantity,
            mode,
            numeral,
            titleMode, 
            bg,
            col,
        } = data;
        return(
            <Grid item
                className='item-earning'
                md={col}
                xs={12}
                key={uuidv4()}
            >
                <Box className='item-box'
                    sx={{
                        borderRadius: '12px',
                    }}>
                    <IconButton className='icon'
                        sx={{backgroundColor:bg}}>
                        <IconSVG icon={icon}
                            fill={COLORS.black}
                            
                        />
                    </IconButton>
                    <Box
                        display={'flex'}
                        justifyContent='space-between'
                        className="earning-info">
                        <Box>
                            <Box display={'flex'}>
                                <Text caption1
                                    sx={{marginRight: 0.5,}}
                                    color={COLORS.grey5}>
                                    {title}
                                </Text>
                                <TooltipCustom title='Small description'>
                                    <IconSVG icon={iconInfoFilled}
                                        width={16}
                                        height={16}
                                        fill={COLORS.grey5}/>
                                </TooltipCustom>
                            </Box>
                            <Box sx={{
                                marginTop:'-8px'
                            }}>

                                <Text h2
                                    color={COLORS.grey7}
                                >
                                    {quantity}
                                </Text>
                            </Box>
                            <Box sx={{
                                marginTop:'-8px'
                            }}>

                                <LabelColor 
                                    sx={{marginLeft: '-14px'}}
                                    mode={mode}
                                    titleMode={titleMode}
                                    numeral={numeral}
                                />
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Grid>
        );
    };
    return (

        <Box className={classes.overviewComponent}>
            <Grid container
                spacing={2}
                className="list-overview">
                {!_.isEmpty(listOverviewDummyData) && listOverviewDummyData.map((data: IOverviewItem)=> (
                    renderListOverview(data)
                ))}
            </Grid>
        </Box>

    );
}

export default withStyles(styles)(OverviewComponent);