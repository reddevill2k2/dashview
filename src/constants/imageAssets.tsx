//image
import iconAdd from '@assets/icons/add/light.svg';
import logo from '@assets/images/logo.png';
import Avatar from '@assets/images/Avatar.png';
import avatarNotification1 from '@assets/images/notification1.png';
import avatarNotification2 from '@assets/images/notification2.png';
import avatarNotification3 from '@assets/images/notification3.png';
import avatarNotification4 from '@assets/images/notification4.png';
import avatarNotification5 from '@assets/images/notification5.png';
import messageNotification1 from '@assets/images/message1.png';
import messageNotification2 from '@assets/images/message2.png';
import messageNotification3 from '@assets/images/message3.png';
import messageNotification4 from '@assets/images/message4.png';
import messageNotification5 from '@assets/images/message5.png';
import imageOverviewUser1 from '@assets/images/img_overview_user_1.png';
import imageOverviewUser2 from '@assets/images/img_overview_user_2.png';
import imageOverviewUser3 from '@assets/images/img_overview_user_3.png';
import imageProductView from '@assets/images/product_view.png';
import chartLineOverview from '@assets/images/chart_line_overview.png';
import imageRecent1 from '@assets/images/recent1.png';
import imageRecent2 from '@assets/images/recent2.png';
import imageSmallLine1 from '@assets/images/small_line_1.png';
import imageSmallLine2 from '@assets/images/small_line_2.png';
import imageSmallLine3 from '@assets/images/small_line_3.png';
import imageProduct1 from '@assets/images/img_product_1.png';
import imageProduct2 from '@assets/images/img_product_2.png';
import imageProduct3 from '@assets/images/img_product_3.png';
import imageProduct4 from '@assets/images/img_product_4.png';
import imageProduct5 from '@assets/images/img_product_5.png';
import imageProduct6 from '@assets/images/img_product_6.png';
import imageProduct7 from '@assets/images/img_product_7.png';
import imageProduct8 from '@assets/images/img_product_8.png';
import imageProduct9 from '@assets/images/img_product_9.png';
import imageProduct10 from '@assets/images/img_product_10.png';
import imageShop1 from '@assets/images/img_shop_1.png';
import imageShop2 from '@assets/images/img_shop_2.png';
import imageShop3 from '@assets/images/img_shop_3.png';
import imageShop4 from '@assets/images/img_shop_4.png';
import imageShop5 from '@assets/images/img_shop_5.png';
import imageShop6 from '@assets/images/img_shop_6.png';
import imageShop7 from '@assets/images/img_shop_7.png';
import imageShop8 from '@assets/images/img_shop_8.png';
import imageShop9 from '@assets/images/img_shop_9.png';
import imageCoverShop from '@assets/images/img_cover_shop.png';
import imagePost1 from '@assets/images/img_104x80_01.png';
import imagePost2 from '@assets/images/img_104x80_02.png';
import imagePost3 from '@assets/images/img_104x80_03.png';
import imagePost4 from '@assets/images/img_104x80_04.png';
import imagePost5 from '@assets/images/img_104x80_05.png';
import imagePost6 from '@assets/images/img_104x80_06.png';
import imagePreview from '@assets/images/preview.png';

//icon
import iconActions from '@assets/icons/actions/filled.svg';
import iconHomeLight from '@assets/icons/home/light.svg';
import iconChevronDown from '@assets/icons/chevron_down/light.svg';
import iconChevronUp from '@assets/icons/chevron_up/light.svg';
import iconChevronBackward from '@assets/icons/chevron_backward/light.svg';
import iconChevronForward from '@assets/icons/chevron_forward/light.svg';
import iconSearch from '@assets/icons/search/light.svg';
import iconAddWhite from '@assets/icons/UIicon/add/filled.svg';
import iconMessage from '@assets/icons/message/light.svg';
import iconArrowForward from '@assets/icons/arrow_forward/light.svg';
import iconBackForward from '@assets/icons/arrow_backward/light.svg';
import IconNotification from '@assets/icons/notification/light.svg';
import iconMoreHorizontal from '@assets/icons/more_horizontal/filled.svg';
import iconShoppingBag from '@assets/icons/shopping_bag/light.svg';
import iconInfo from '@assets/icons/info/light.svg';
import iconActivity from '@assets/icons/activity/light.svg';
import iconActivityFilled from '@assets/icons/activity/filled.svg';
import iconMessageLight from '@assets/icons/message/light.svg';
import iconHeartLight from '@assets/icons/heart/light.svg';
import iconHeartFilled from '@assets/icons/heart/filled.svg';
import iconLinkLight from '@assets/icons/link/light.svg';
import iconScheduleLight from '@assets/icons/schedule/light.svg';
import iconDesignLight from '@assets/icons/design/light.svg';
import iconPhoneLight from '@assets/icons/phone/light.svg';
import iconVideoRecorderLight from '@assets/icons/video_recorder/light.svg';
import iconMultiSelectLight from '@assets/icons/multiselect/light.svg';
import iconFacebook from '@assets/icons/facebook/filled.svg';
import iconTwitter from '@assets/icons/twitter/filled.svg';
import iconTwitterLight from '@assets/icons/twitter/light.svg';
import iconInstagram from '@assets/icons/instagram/filled.svg';
import iconBasketLight from '@assets/icons/basket/light.svg';
import iconBarChartFilled from '@assets/icons/bar_chart/filled.svg';
import iconBarChartLight from '@assets/icons/bar_chart/light.svg';
import iconTicketLight from '@assets/icons/ticket/light.svg';
import iconGridLight from '@assets/icons/grid/light.svg';
import iconLeaderBoardFilled from '@assets/icons/leaderboard/filled.svg';
import iconCloseLight from '@assets/icons/close/light.svg';
import iconPhotoLight from '@assets/icons/photos/light.svg';
import iconPaymentLight from '@assets/icons/payment/light.svg';
import iconUp from '@assets/icons/up/up.svg';
import iconUpLoadFilled from '@assets/icons/upload/filled.svg';
import iconDown from '@assets/icons/down/down.svg';
import iconDollar from '@assets/icons/dollar/filled.svg';
import iconArrowUpDown from '@assets/icons/arrow_up_down/filled.svg';
import iconInfoFilled from '@assets/icons/info/filled.svg';
import iconMessages from '@assets/icons/messages/light.svg';
import iconMouse from '@assets/icons/mouse/light.svg';
import iconProfile from '@assets/icons/profile_circled/light.svg';
import iconFilterLight from '@assets/icons/filter/light.svg';
import iconStarFilled from '@assets/icons/star/filled.svg';
import iconStarLight from '@assets/icons/star/light.svg';
import iconInstagramLight from '@assets/icons/instagram/light.svg';
import iconPinterestLight from '@assets/icons/pinterest/light.svg';
import iconImageFilled from '@assets/icons/image/filled.svg';
import iconVideoRecorderFilled from '@assets/icons/video_recorder/filled.svg';
import iconPieChar from '@assets/icons/pie_chart/light.svg';
import iconDonutChartLight from '@assets/icons/donut_chart/light.svg';
import iconMobile from '@assets/icons/mobile/light.svg';
import iconTablet from '@assets/icons/tablet/light.svg';
import iconLaptop from '@assets/icons/laptop/light.svg';
import iconPromtion from '@assets/icons/promotion/light.svg';
import iconClock from '@assets/icons/clock/light.svg';
import iconListFilled from '@assets/icons/list/filled.svg';
import iconCheckAll from '@assets/icons/check_all/light.svg';
import iconCalendar from '@assets/icons/calendar/light.svg';
import iconEdit from '@assets/icons/edit/light.svg';
import iconTrash from '@assets/icons/trash/light.svg';
import iconRepeatFilled from '@assets/icons/repeat/repeat.svg';
import iconSmileLight from '@assets/icons/smile/light.svg';
import iconLikeFilled from '@assets/icons/facebookreaction/like.svg';
import iconAngry from '@assets/icons/facebookreaction/angry.svg';
import iconCare from '@assets/icons/facebookreaction/care.svg';
import iconLikeLight from '@assets/icons/facebookreaction/likeLight.svg';
import iconLove from '@assets/icons/facebookreaction/love.svg';
import iconSad from '@assets/icons/facebookreaction/sad.svg';
import iconSmile from '@assets/icons/facebookreaction/smile.svg';
import iconWow from '@assets/icons/facebookreaction/wow.svg';
import iconWarning from '@assets/icons/warning/filled.svg';
import iconCheckCircled from '@assets/icons/check_circled/filled.svg';

export {
    iconActions,
    iconLikeFilled,
    iconAngry,
    iconCare,
    iconLikeLight,
    iconLove,
    iconSad,
    iconSmile,
    iconWow,
    iconStarLight,
    iconWarning,
    iconCheckCircled,
    iconTrash,
    iconEdit,
    iconCalendar,
    iconListFilled,
    iconCheckAll,
    iconClock,
    iconPromtion,
    iconLaptop,
    iconTablet,
    iconMobile,
    iconPieChar,
    iconBarChartLight,
    iconArrowUpDown,
    iconInfoFilled,
    iconMessages,
    iconMouse,
    iconProfile,
    iconVideoRecorderFilled,
    iconHomeLight,
    logo,
    iconChevronDown,
    iconChevronUp,
    iconAdd,
    iconChevronBackward,
    iconChevronForward,
    iconSearch,
    iconAddWhite,
    iconMessage,
    iconRepeatFilled,
    iconSmileLight,
    IconNotification,
    Avatar,
    avatarNotification1,
    avatarNotification2,
    avatarNotification3,
    avatarNotification4,
    avatarNotification5,
    iconMoreHorizontal,
    messageNotification1,
    messageNotification2,
    messageNotification3,
    messageNotification4,
    messageNotification5,
    iconShoppingBag,
    iconInfo,
    iconActivity,
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
    iconArrowForward,
    iconBackForward,
    iconMessageLight,
    iconHeartLight,
    iconHeartFilled,
    iconLinkLight,
    imageProductView,
    chartLineOverview,
    iconScheduleLight,
    iconDesignLight,
    iconPhoneLight,
    iconVideoRecorderLight,
    iconMultiSelectLight,
    iconActivityFilled,
    iconInstagram,
    iconFacebook,
    iconTwitter,
    iconTwitterLight,
    iconBasketLight,
    iconBarChartFilled,
    iconTicketLight,
    iconGridLight,
    iconLeaderBoardFilled,
    imageRecent1,
    imageRecent2,
    iconCloseLight,
    iconPhotoLight,
    iconImageFilled,
    imageSmallLine1,
    imageSmallLine2,
    imageSmallLine3,
    iconPaymentLight,
    iconUp,
    iconUpLoadFilled,
    iconDollar,
    iconDown,
    imageProduct1,
    imageProduct2,
    imageProduct3,
    imageProduct4,
    imageProduct5,
    imageProduct6,
    imageProduct7,
    imageProduct8,
    imageProduct9,
    imageProduct10,
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9,
    iconFilterLight,
    iconDonutChartLight,
    iconStarFilled,
    iconInstagramLight,
    iconPinterestLight,
    imageCoverShop,
    imagePost1,
    imagePost2,
    imagePost3,
    imagePost4,
    imagePost5,
    imagePost6,
    imagePreview,
};