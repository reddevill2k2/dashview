export const COLORS = {
    white: '#FFFFFF',
    black: '#000000',
    grey1: '#FCFCFC',
    grey2: '#F4F4F4',
    grey3: '#EFEFEF',
    grey4: '#6F767E',
    grey5: '#33383F',
    grey6: '#272B30',
    grey7: '#1A1D1F',
    grey8: '#111315',
    grey9:'rgba(244, 244, 244, 0.5)',
    // shadow
    shades40: '#6F767E66',
    shades50: '#6F767E',
    shades50Dark: '#11131580',
    shades75: '#9A9FA5',
    shades95: 'rgba(154, 159, 165,0.2)',

    //primary
    blue: '#2A85FF',
    blueHover: '#005cd6',
    green: '#83BF6E',
    red: '#FF6A55',
    violet: '#8E59FF',
    greenNgoc:'#B5E4CA',
    greenNgocHover:'rgba(181, 228, 202,0.85)',
    //secondary
    redLight: '#FFE7E4',
    orangeLight: '#FFBC99',
    violetLight: '#CABDFF',
    blueLight: '#B1E5FC',
    greenLight: '#EAFAE5', // #B5E4CA
    yellowLight: '#FFD88D',
    //hoverColor
    hover: {
        hoverPicutre:'#dfdbdb',
        hoverChartZoomBox: '#48F',
        hoverZoomBoxBackground:'#4285f433',
    }
};