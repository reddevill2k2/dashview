// export const COLORS = {
//     white: '#FFFFFF',
//     black: '#000000',
//     grey1: '#FCFCFC',
//     grey2: '#F4F4F4',
//     grey3: '#EFEFEF',
//     grey4: '#6F767E',
//     grey5: '#33383F',
//     grey6: '#272B30',
//     grey7: '#1A1D1F',
//     grey8: '#111315',

//     // shadow
//     shades40: '#6F767E66',
//     shades50: '#6F767E',
//     shades50Dark: '#11131580',
//     shades75: '#9A9FA5',

//     //primary
//     blue: '#2A85FF',
//     green: '#83BF6E',
//     red: '#FF6A55',
//     violet: '#8E59FF',

//     //secondary
//     orangeLight: '#FFBC99',
//     violetLight: '#CABDFF',
//     blueLight: '#B1E5FC',
//     greenLight: '#B5E4CA',
//     yellowLight: '#FFD88D',
// };