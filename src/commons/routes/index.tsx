
import DashboardLayout from '@commons/layouts';
import NotFoundComponent from '@components/NotFoundComponent';
import CommentsContainer from '@containers/Admin/CommentsContainer';
import CreateContainer from '@containers/Admin/CreateContainer';
import CustomerListContaier from '@containers/Admin/CustomerListContaier';
import DashboardContainer from '@containers/Admin/DashboardContainer';
import DraffsContainer from '@containers/Admin/DraffsContainer';
import EarningContainer from '@containers/Admin/EarningContainer';
import HomeContainer from '@containers/Admin/HomeContainer';
import OverviewContainer from '@containers/Admin/OverviewContainer';
import PayoutsContainer from '@containers/Admin/PayoutsContainer';
import PromoteContainer from '@containers/Admin/PromoteContainer';
import RefundsContainer from '@containers/Admin/RefundsContainer';
import ReleasedContainer from '@containers/Admin/ReleasedContainer';
import ScheduledContainer from '@containers/Admin/ScheduledContainer';
import ShopContainer from '@containers/Admin/ShopContainer';
import StatementsContainer from '@containers/Admin/StatementsContainer';
import React from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
export default function Router() {
    return useRoutes([
        {
            path: '/',
            element: <Navigate to="/dashboard/home"
                replace />,
        },
        {
            path: '/',
            element: <DashboardLayout/>,
            children:[
                { path: 'product-add', element: <CreateContainer/>}
            ]
        },
        {
            path: '/dashboard',
            element: <DashboardLayout/>,
            children: [
                { path: 'home', element: <HomeContainer /> },
                { path: 'app', element: <DashboardContainer /> },
                { path: 'drafts', element: <DraffsContainer /> },
                { path: 'released', element: <ReleasedContainer /> },
                { path: 'comments', element: <CommentsContainer /> },
                { path: 'scheduled', element: <ScheduledContainer /> },
                { path: 'overview', element: <OverviewContainer /> },
                { path: 'customer-list', element: <CustomerListContaier /> },
                {path: 'shop',element: <ShopContainer />},
                { element: <Navigate to="/income/earning"
                    replace /> },
                { path: 'earning', element: <EarningContainer /> },
                { path: 'refunds', element: <RefundsContainer /> },
                { path: 'payouts', element: <PayoutsContainer /> },
                { path: 'statements', element: <StatementsContainer /> },
                { path: 'promote', element: <PromoteContainer /> },
            ]
        },
        {
            path: '*',
            element: <Navigate to="/404"
                replace />,
        },
        {
            path: '404',
            element: <NotFoundComponent />,
        }
    ]);
}