import _ from 'lodash';
import * as React from 'react';
import {Outlet} from 'react-router-dom';
import DashboardAppBar from './DashboardAppBar';
import DashboardDrawer from './DashboardDrawer';

const DashboardLayout = () => {
    const [open, setOpen] = React.useState(true);
    const [search, setSearch] = React.useState<string>('');
    const [scrollPage, setScrollPage] = React.useState<boolean>(false);

    React.useEffect(()=> {
        const onScroll = () => {
            const windowY = window.scrollY;
            if(windowY > 0) {
                setScrollPage(true);
            } else {
                setScrollPage(false);
            }
        };
        // clean up code
        window.removeEventListener('scroll', onScroll);
        window.addEventListener('scroll', onScroll, { passive: true });
        return () => window.removeEventListener('scroll', onScroll);
    }, []);

    const handleSearch = (e: any)=> {
        if(!_.isEmpty(e))
            setSearch(e.target.value);
        else  setSearch('');
    };

    const onCloseSlidebar = () => {
        setOpen(!open);
    };
    return (
        <div>
            <DashboardDrawer
                isOpenSlidebar={open}
                onCloseSlidebar={onCloseSlidebar}
            />
            <DashboardAppBar search={search}
                scrollPage={scrollPage}
                handleSearch={handleSearch}/>
                
            <Outlet/>
        </div>
    );
};

export default DashboardLayout;