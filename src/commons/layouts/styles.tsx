import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    root: {
        
    },
    styleLogo: {
        width: 48,
        height: 48
    },
    slidebar: {
        padding: 24
    },
    iconButton: {
        width: 24,
        height: 24
    },textButton: {
        color: COLORS.grey4
    },
    listMenu: {
        marginTop: '48px !important',
        '& .Mui-selected': {
            backgroundColor: '#EFEFEF !important',
            borderRadius: 12,
            height: 48,
            boxShadow: 'inset 0px -2px 1px rgb(0 0 0 / 5%), inset 0px 1px 1px #ffffff'

        },
        '& .css-cvhtoe-MuiButtonBase-root-MuiListItemButton-root:hover': {
            backgroundColor: 'unset',
        }
    },
    listChildren: {
        marginLeft: '36px !important',
        '& .MuiListItemButton-root::before': {
            background: `url(${borderMenu})  no-repeat 40% 57%/100% auto`,
            content: '""',
            position: 'absolute',
            width: 12,
            height: 12,
            top: 12,
            left: 1,
        },
        '&::after': {
            content: '""',
            position: 'absolute',
            width: 2,
            backgroundColor: COLORS.grey3,
            top: 0,
            bottom: 28,
            left: 0,
            zIndex: 200,
        }
    },
    notification: {
        color: COLORS.grey7,
        width: 24,
        height: 24,
        borderRadius: 6,
        justifyContent: 'center',
        display: 'flex',
        alignItems: 'center',
    },
    boxIconAdd: {
        width: 24,
        height: 24,
        borderRadius: 24,
        border: `2px solid ${COLORS.grey3}`,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 8,
        '&:hover': {
            borderColor: COLORS.grey7,
        }
    },
    searchBox: {
        backgroundColor: COLORS.grey2,
        borderRadius: 12,
        color: COLORS.shades40,
        position: 'relative',
        width: 360,
        height: 48,
        display: 'flex',
        alignItems: 'center'
    },
    iconSearch: {
        position: 'absolute',
        left: 8,
        top: 12,
        bottom: 12,
    },
    appBarStyle: {
        width: 'calc(100% - 340px) !important',
        boxShadow: 'none !important',
        padding: 20,
        transition: 'all .3s ease',
        '&.active': {
            zIndex: '4000 !important',
            borderBottom: `1px solid ${COLORS.grey3}`
        }
    },
    inputSearch: {
        '& input': {
            fontWeight: 'bold',
            padding: '10px 25px 10px 35px',
            border: '2px solid transparent',
            borderRadius: 12,
            '&:focus': {
                outline: 'none',
                border: `2px solid  ${COLORS.blue}`,
            }
        }
        
    },
    commandF: {
        padding: '4px 12px',
        borderRadius: 8,
        backgroundColor: COLORS.white,
        position: 'absolute',
        top: 8,
        bottom: 8,
        cursor: 'pointer',
        right: 12,
        width: 60,
        color: COLORS.black,
        fontWeight: 'bold',

    },
    toolbarStyle: {
        display: 'flex',
        justifyContent: 'space-between',
        '& .group-search': {
            width: 360,
            position: 'relative',
        }
    }, 
    boxRight: {
        display: 'flex',
        alignItems: 'center',
        '& .text': {
            color: `${COLORS.white} !important`
        }
    },
    iconRight: {
        marginLeft: 36,
        cursor: 'pointer',
        position: 'relative',
        '&:hover': {
            '& .icon-svg': {
                color: COLORS.grey7,
            }
        }
    },
    iconUseRight: {
        marginLeft: 36,
        height: '48px !important',
        width: '48px !important',
        cursor: 'pointer',
        position: 'relative',
        '&:hover': {
            '& .icon-svg': {
                color: COLORS.grey7,
            }
        }
    },
    dotNotification: {
        position: 'absolute',
        border: '2px solid #fff',
        top: 0,
        right: 0,
        width: 14,
        height: 14,
        borderRadius: 14,
        backgroundColor: COLORS.red,
    },
    boxHeader: {
        color: COLORS.grey7,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 12,
    },
    dropdownList: {
        padding: 12,
        borderRadius: 16,
    },
    boxIconMore: {
        width: 32,
        height: 32,
        borderRadius: 32,
        backgroundColor: COLORS.grey3,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer'
    },
    titleDropDown: {
        color: COLORS.grey7
    },
    itemName: {display: 'inline-block', color: COLORS.grey7},
    itemUsername: {
        display: 'inline-block', color: COLORS.shades75, paddingLeft: 4
    },
    itemNoti: {
        display: 'flex !important',
        alignItems: 'flex-start !important',
        justifyContent: 'space-between !important',
        width: '100%',
    },
    dFlex: {
        display: 'flex',
    },
    itemItem: {
        color: COLORS.shades75
    },
    dotUnread: {
        backgroundColor: COLORS.blue,
        width:12,
        height: 12,
        borderRadius: 12,
        marginLeft: 8
    },
    dotRead: {
        backgroundColor: COLORS.shades75,
        width:12,
        height: 12,
        borderRadius: 12,
        marginLeft: 8
    },
    boxUnread: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 5,
    },
    listItemButton: {
        borderRadius: '8px !important'
    },
    itemAvatar: {
        position: 'relative',
    },
    statusOnline: {
        backgroundColor: COLORS.green,
        width:12,
        height: 12,
        borderRadius: 12,
        position: 'absolute',
        content: '""',
        top: 0,
        left: 0,
    },
    statusOffline: {
        // backgroundColor: COLORS.shades75,
        width:12,
        height: 12,
        borderRadius: 12,
        position: 'absolute',
        content: '""',
        top: 0,
        left: 0,
    },
    buttonAll: {
        margin: '24px 12px !important',
        width: 'calc(100% - 24px) !important',
    },
    dropdownUseInfo: {
        width: 280,
        '& .group-items': {
            padding: '12px 0',
        },
        '& .item-info': {
            padding: 12,
            display: 'flex',
            borderRadius: 12,
            width: '100%',
            cursor: 'pointer',
            transition: 'all .3s ease',
            '&:hover': {
                backgroundColor: COLORS.grey2,
                '& .icon-svg':{
                    color: `${COLORS.grey7} !important`,
                },
                '& .text': {
                    color: `${COLORS.grey7} !important`,
                }
            },
            '& .icon-svg': {
                marginRight: 12,
                width: 24,
                height: 24,
                color: COLORS.grey4,
                transition: 'all .3s ease',
            },
            '&.violet': {
                '& .icon-svg': {
                    color: COLORS.violet,
                },
                '& .text': {
                    color: `${COLORS.violet} !important`,
                }
            }
        }
    },
    boxSearch: {
        backgroundColor: 'transparent',
        padding: 10,
       
        // position: 'absolute',
        // top: -34,
        position: 'relative',
        '& .search-content': {
            width: 384,
            position: 'absolute',
            visibility: 'hidden',
            opacity: 0,
            transition: 'all .2s ease',
            left: 0,
            top: 0,
            right: 0,
            zIndex: -1,
            paddingTop: 60,
            borderRadius: '16px',
        },
        '&.active': {
            
            '& .search-content': {
                visibility: 'visible',
                opacity: 1,
                backgroundColor: COLORS.white,
                boxShadow: '0px 32px 48px -8px rgb(0 0 0 / 10%), 0px 0px 14px -4px rgb(0 0 0 / 5%)',
            }
        }
    },
    listContentSearch: {
        '& .recent-title': {
            marginTop: 24,
            padding: '0 12px',
        },
        '& .recent-list': {
            padding: '24px 0',
            '& .recent-item': {
                padding: 12,
                '& img': {
                    marginRight: 16,
                }
            },
            '& .box-close': {
                color: COLORS.grey4,
                width: 30,
                height: 30,
                '& .icon-svg': {
                    width: 24,
                    height: 24,
                }
            },
            '& .icon-picture': {
                width: 48,
                height: 48,
                borderRadius: 48,
                border: '2px solid',
                borderColor: COLORS.violetLight,
                marginRight: 16,
            }
        }
    }
});

export default styles;