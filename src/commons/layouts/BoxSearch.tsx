import { COLORS } from '@constants/colors';
import { Avatar, iconBarChartFilled, iconCloseLight, iconGridLight, iconLeaderBoardFilled, iconPhoneLight, iconPhotoLight, iconTicketLight, imageRecent1, imageRecent2 } from '@constants/imageAssets';
import Dropdown from '@helpers/Dropdown';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import Text from '@helpers/Text';
import { Box, Divider, Icon, IconButton, List, ListItemAvatar, ListItemButton, ListItemText } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';
import iconChevronForward from '@assets/icons/chevron_forward/light.svg';

interface IUserInfoHeader {
    search?: string,
    handleSearch: any
}

const UserInfoHeader = (props: IUserInfoHeader & WithStyles<typeof styles>) => {
    const {classes, search, handleSearch} = props;

    return (
        <Box className={clsx(classes.boxSearch,  {
            ['active']: search,
        })}>
            <Input placeholder='Search or type a command'
                handleSearch={handleSearch}
                search={search}
                commandF/>
            <Box className={clsx(classes.listContentSearch, 'search-content')}>
                <Text color={COLORS.grey4}
                    baseSB1
                    className={'recent-title'} >
             Recent Search
                </Text>
                <Box className='recent-list'>
                    <Box display={'flex'}
                        justifyContent={'space-between'}
                        className='recent-item'>
                        <Box component={'a'}
                            href={'#'}
                            onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> {
                                e.preventDefault();
                            }}
                            display={'flex'}
                            alignItems={'center'}>
                            <img src={imageRecent1}
                                width={48}
                                height={48}/>
                            <Box>
                                <Text caption1>Small caption</Text>
                                <Text baseSB1
                                    color={COLORS.grey7}>Put your title here</Text>
                            </Box>
                        </Box>
                        <IconButton className={'box-close'}>
                            <IconSVG icon={iconCloseLight}/>
                        </IconButton>
                    </Box>
                    <Box display={'flex'} 
                        justifyContent={'space-between'}
                        className='recent-item'>
                        <Box component={'a'}
                            href={'#'}
                            onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> {
                                e.preventDefault();
                            }}
                            display={'flex'}
                            alignItems={'center'}>
                            <img src={imageRecent2}
                                width={48}
                                height={48}/>
                            <Box>
                                <Text caption1>Small caption</Text>
                                <Text baseSB1
                                    color={COLORS.grey7}>Put your title here</Text>
                            </Box>
                        </Box>
                        <IconButton className={'box-close'}>
                            <IconSVG icon={iconCloseLight}/>
                        </IconButton>
                    </Box>
                </Box>
                <Divider/>
                <Text color={COLORS.grey4}
                    baseSB1
                    className={'recent-title'} >
             Suggestions
                </Text>
                <Box className='recent-list'>
                    <Box display={'flex'}
                        justifyContent={'space-between'}
                        className='recent-item'>
                        <Box component={'a'}
                            href={'#'}
                            onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> {
                                e.preventDefault();
                            }}
                            display={'flex'}
                            alignItems={'center'}>
                            <IconButton className='icon-picture'>
                                <IconSVG icon={iconPhotoLight}/>
                            </IconButton>
                            <Box>
                                <Text baseSB1
                                    color={COLORS.grey7}>Put your title here</Text>
                                <Text caption1
                                >Small caption</Text>
                            </Box>
                        </Box>
                        <IconButton className={'box-close'}>
                            <IconSVG icon={iconChevronForward}/>
                        </IconButton>
                    </Box>
                    <Box display={'flex'}
                        justifyContent={'space-between'}
                        className='recent-item'>
                        <Box component={'a'}
                            href={'#'}
                            onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> {
                                e.preventDefault();
                            }}
                            display={'flex'}
                            alignItems={'center'}>
                            <IconButton className='icon-picture'>
                                <IconSVG icon={iconPhotoLight}/>
                            </IconButton>
                            <Box>
                                <Text baseSB1
                                    color={COLORS.grey7}>Put your title here</Text>
                                <Text caption1
                                >Small caption</Text>
                            </Box>
                        </Box>
                        <IconButton className={'box-close'}>
                            <IconSVG icon={iconChevronForward}/>
                        </IconButton>
                    </Box>
                </Box>
            </Box>
        </Box>
        
    );
};

export default withStyles(styles)(UserInfoHeader);