import { PATH_DASHBOARD } from '@commons/routes/path';
import ic_home from '@assets/icons/home/light.svg';
import icon_diamond from '@assets/icons/diamond/light.svg';
import icon_profile from '@assets/icons/profile_circled/light.svg';
import icon_store from '@assets/icons/store/light.svg';
import icon_pie from '@assets/icons/pie_chart/light.svg';
import icon_promotion from '@assets/icons/promotion/light.svg';
import { COLORS } from '@constants/colors';

const ICONS = {
    ic_home: ic_home,
    icon_diamond: icon_diamond,
    icon_profile: icon_profile,
    icon_store: icon_store,
    icon_pie: icon_pie,
    icon_promotion: icon_promotion,
};

const sidebarConfig = [
    {
        subHeader: 'Home',
        icon: ICONS.ic_home,
        path: PATH_DASHBOARD.path,
        items: []
    },
    {
        subHeader: 'Products',
        icon: ICONS.icon_diamond,
        path: PATH_DASHBOARD.products.path,
        addProduct: true,
        items: [
            {
                title: 'Dashboard',
                path: PATH_DASHBOARD.products.Dashboard,
            },
            {
                title: 'Drafts',
                path: PATH_DASHBOARD.products.Drafts,
                notification: 2,
                colorNoti: COLORS.orangeLight,
            },
            {
                title: 'Released',
                path: PATH_DASHBOARD.products.Released,
            },
            {
                title: 'Comments',
                path: PATH_DASHBOARD.products.Comment,
            },
            {
                title: 'Scheduled',
                path: PATH_DASHBOARD.products.Scheduled,
                notification: 8,
                colorNoti: COLORS.greenLight,
            },
        ]
    },
    {
        subHeader: 'Customers',
        icon: ICONS.icon_profile,
        path: PATH_DASHBOARD.customers.path,
        items: [
            {
                title: 'Overview',
                path: PATH_DASHBOARD.customers.Overview,
            },
            {
                title: 'Customer List',
                path: PATH_DASHBOARD.customers.CustomerList,
            }
        ]
    },
    {
        subHeader: 'Shop',
        icon: ICONS.icon_store,
        path: PATH_DASHBOARD.shop.path,
        items: []
    },
    {
        subHeader: 'Income',
        icon: ICONS.icon_pie,
        path: PATH_DASHBOARD.Income.path,
        items: [
            {
                title: 'Earning',
                path: PATH_DASHBOARD.Income.Earning,
            },
            {
                title: 'Refunds',
                path: PATH_DASHBOARD.Income.Refunds,
            },
            {
                title: 'Payouts',
                path: PATH_DASHBOARD.Income.Payouts,
            },
            {
                title: 'Statements',
                path: PATH_DASHBOARD.Income.Statements,
            },
        ]
    }, {
        subHeader: 'Promote',
        icon: ICONS.icon_promotion,
        path: PATH_DASHBOARD.Promote.path,
        items: []
    }
];

export default sidebarConfig;