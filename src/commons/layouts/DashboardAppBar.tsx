import * as React from 'react';
import {withStyles, WithStyles} from '@mui/styles';
import styles from './styles';
import { AppBar, Box, Drawer, Icon, SvgIcon, Toolbar, Typography } from '@mui/material';
import Button from '@helpers/Button';
import iconMessage from '@assets/icons/message/light.svg';
import IconNotification from '@assets/icons/notification/light.svg';
import { Avatar } from '@constants/imageAssets';
import NotificationHeader from './NotificationHeader';
import Input from '@helpers/Input';
import MessageHeader from './MessageHeader';
import UseInfoHeader from './UseInfoHeader';
import BoxSearch from './BoxSearch';
import clsx from 'clsx';
import { useNavigate } from 'react-router-dom';

interface DashboardAppBarProps {
    open?: boolean,
    search?: string,
    handleSearch?: (e: any)=> void,
    handleCloseDrawer?: any,
    scrollPage?: boolean,
}

const DashboardAppBar = (props: DashboardAppBarProps & WithStyles<typeof styles>) => {
    const {classes, search, handleSearch, scrollPage } = props;
    const navigate = useNavigate();
    return (
        <React.Fragment>
            <AppBar
                color={'primary'}
                className={clsx(classes.appBarStyle, { ['active']: search || scrollPage})}
            >
                <Toolbar
                    className={clsx(classes.toolbarStyle)}
                >
                    <Box className='group-search'>
                        <BoxSearch search={search}
                            handleSearch={handleSearch}/>
                    </Box>
                    <Box
                        className={classes.boxRight}
                    >
                        <Button
                            color={'primary'}
                            isIconBefore
                            onClick={()=>{ navigate('/product-add');}}
                        >Create</Button>
                        <MessageHeader/>
                        <NotificationHeader/>
                        <UseInfoHeader/>
                    </Box>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
};
export default withStyles(styles)(DashboardAppBar);