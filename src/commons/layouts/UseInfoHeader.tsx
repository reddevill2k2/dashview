import { COLORS } from '@constants/colors';
import { Avatar, iconBarChartFilled, iconGridLight, iconLeaderBoardFilled, iconTicketLight } from '@constants/imageAssets';
import Dropdown from '@helpers/Dropdown';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, Divider, Icon, List, ListItemAvatar, ListItemButton, ListItemText } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';

interface IUserInfoHeader {
    isOpenSlidebar?: boolean,
    onCloseSlidebar?: any,
}

const UserInfoHeader = (props: IUserInfoHeader & WithStyles<typeof styles>) => {
    const {classes, isOpenSlidebar, onCloseSlidebar} = props;
    const [anchorElNotification, setAnchorElNotification] = React.useState<HTMLDivElement | null>(null);

    const handleOpenDropdownNotification = (event:React.MouseEvent<HTMLDivElement>) => {
        setAnchorElNotification(event.currentTarget);
    };
    const handleCloseDropdownUseInfo = () => {
        setAnchorElNotification(null);
    };

    const renderItem = (data: {
        name: string,
        image: string,
        username: string,
        type: string,
        content: string,
        time: string,
    }, idx: number) => {
        const {image, name, username, type, content, time}= data;
        
        return (
            <Box component={'div'}
                key={uuidv4()}>
                <ListItemButton
                    className={classes.listItemButton}>
                    <Box className={classes.itemNoti}>
                        <Box component={'div'}
                            className={classes.dFlex}>
                            <ListItemAvatar>
                                <img src={image}
                                    width={48}
                                    height={48}/>  
                            </ListItemAvatar>
                            <Box component={'div'}>
                                <ListItemText primary={
                                    <div>
                                        <Text base1
                                            className={classes.itemName}>{name}</Text> 
                                        <Text base1
                                            color={COLORS.grey4}
                                            className={classes.itemUsername}>{username}</Text> 
                                    </div>
                                }/>
                                <ListItemText primary={
                                    <div>
                                        <Text bodyM1
                                            color={COLORS.grey4}
                                            className={classes.itemName}>{type}</Text> 
                                        <Text base1
                                            className={classes.itemUsername}>{content}</Text> 
                                    </div>
                                }/>
                            </Box>
                        </Box>
                        <Box component={'div'}
                            className={classes.boxUnread}>
                            <Text caption1
                                color={COLORS.grey4}
                                className={classes.itemItem}>{time}</Text>
                            <Box className={classes.dotUnread}/>
                        </Box>
                    </Box>
                    
                </ListItemButton>
            </Box>
        );
    };

    const open = Boolean(anchorElNotification);
  
    return (
        <React.Fragment>
            <Icon
                component={'div'}
                className={classes.iconUseRight}
                onClick={handleOpenDropdownNotification}
            >
   
                <img src={Avatar}
                    width={48}
                    height={48}/>
            </Icon>
            <Dropdown
                anchorEl={anchorElNotification}
                open={open}
                onClose={handleCloseDropdownUseInfo}
            >
                <Box
                    className={clsx(classes.dropdownList, classes.dropdownUseInfo)}
                >
                    <List>
                        <Box className='item-info'>
                            <Text color={COLORS.grey4}
                                baseSB1>
                            Profile
                            </Text>
                        </Box>
                        <Box className='item-info'>
                            <Text color={COLORS.grey4}
                                baseSB1>
                            Edit Profile
                            </Text>
                        </Box>
                        <Divider/>
                        <Box className='group-items'>
                            <Box className='item-info'>
                                <IconSVG icon={iconBarChartFilled}/>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Analytics
                                </Text>
                            </Box>
                            <Box className='item-info'>
                                <IconSVG icon={iconTicketLight}/>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Affiliate center
                                </Text>
                            </Box>
                            <Box className='item-info'>
                                <IconSVG icon={iconGridLight}/>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Explore authors
                                </Text>
                            </Box>
                        </Box>
                        <Divider/>
                        <Box className="group-items">
                            <Box className='item-info violet'>
                                <IconSVG icon={iconLeaderBoardFilled}/>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Upgrade to Pro
                                </Text>
                            </Box>
                        </Box>
                        <Divider/>
                        <Box className="group-items">
                            <Box className='item-info'>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Account Settings
                                </Text>
                            </Box>
                            <Box className='item-info'>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Logout
                                </Text>
                            </Box>
                        </Box>
                    </List>
                </Box>
            </Dropdown>
        </React.Fragment>
    );
};

export default withStyles(styles)(UserInfoHeader);