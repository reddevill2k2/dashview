import { WithStyles, withStyles } from '@mui/styles';
import { Editor } from '@tinymce/tinymce-react';
import React, { useRef } from 'react';
import styles from './styles';
import { Box } from '@mui/material';

interface INumberProps{
    children?: React.ReactChildren,
    background?: string,
    style?: object
}
const Number = (props : INumberProps & WithStyles<typeof styles>) => {

    const {classes,children,style,...otherProps} = props;
    return (
        <Box className={classes.number}
            style={{...style}}>
            {children}
        </Box>
    );
};

export default  withStyles(styles)(Number);