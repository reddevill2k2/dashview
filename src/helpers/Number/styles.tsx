import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    number:{
        display:'inline-block',
        background: (props:any) => props.background,
        borderRadius: '6px'
    }

});

export default styles;