import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    boxSelect: {
        borderRadius: '12px !important',
        border: `2px solid ${COLORS.grey3}`,
        display: 'flex !important',
        justifyContent: 'center !important',
        alignItems: 'center !important',
        height: 40,
        padding: 8,
        outlineOffset: -2,
        borderImage: 'none',
        outline: 'transparent',
        color: `${COLORS.grey4} !important`,
        fontWeight: `${600} !important`,
        fontSize: '14px !important',
        '&:before': {
            borderColor: COLORS.grey3,
        },
        '&:after': {
            borderColor: COLORS.grey3,
        },
        '& .Mui-focused': {
            border: '0 !important',
        },
        '& .MuiOutlinedInput-notchedOutline': {
            border: '0 !important',
        },
        '& .MuiMenu-paper': {
            borderRadius: '12px',
            marginTop: -5
        }
    },
});

export default styles;