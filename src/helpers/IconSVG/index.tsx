import iconAddWhite from '@assets/icons/UIicon/add/filled.svg';
import { Button, Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import Text from '../Text';
import styles from './styles';
import SVG, { Props as SVGProps } from 'react-inlinesvg';
import { COLORS } from '@constants/colors';
import { SxProps } from '@mui/system';


interface IconSVGProps {
    icon: any,
    fill?: string,
    title?: string,
    className?: string,
    width?: number | string | undefined,
    height?: number | string | undefined,
    small?: boolean,
    sx?: SxProps,
}


const IconSVG = (props: IconSVGProps & WithStyles<typeof styles>) => {
    const {classes, icon, fill, title, className, small, sx} = props;
    let {width = 24, height = 24} = props;
    if(small) {
        width = 20;
        height = 20;
    }
    return (
        <Box color={fill}
            className={clsx('icon-svg', classes.boxIconSvg)}
            style={{
                width: width,
                height: height,
            }}
            sx={sx}
            component='span'>
            <SVG src={icon}
                className={className}
                preProcessor={(code) => code.replace(/fill=".*?"/g, 'fill="currentColor"')}
                title={title} 
                // width={width}
                // height={height}
            />
               
        </Box>
    );
};

export default withStyles(styles)(IconSVG);