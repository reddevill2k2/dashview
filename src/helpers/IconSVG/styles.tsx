import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    boxIconSvg: {
        transition: 'all .3s ease',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default styles;