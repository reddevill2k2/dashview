import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    boxTitle: {
        display: 'flex',
        alignItems: 'center',
    },
    rock: {
        borderRadius: 4,
        width: 16,
        height: 32,
        marginRight: 16,
    }
});

export default styles;