import { WithStyles, withStyles } from '@mui/styles';
import { Editor } from '@tinymce/tinymce-react';
import React, { useRef } from 'react';
import styles from './styles';
import { Box } from '@mui/material';
const TextEditor = React.forwardRef((props : any & WithStyles<typeof styles>,
    ref:React.RefObject<HTMLInputElement>) =>{
    const editorRef = useRef<any>(null);
    const {classes,isHide,onChange,value} = props;
    React.useEffect(() => {
        isHide ? editorRef.current.focus() : null;
        return () => {
            // console.log('This will be logged on unmount');
        };
    }, [isHide]);
    const handleEditorChange = (editor:any) => onChange(editor);
    return (
        <Box className={classes.editor}>
            <Editor
                apiKey="qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"
                onInit={(evt, editor) => (editorRef.current = editor)}
                onEditorChange={handleEditorChange}
                value = {value}
                init={{
                    height: 160,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table paste code help '
                    ],
                    toolbar:
                      'bold italic underline addunorderedlist aligncenter ' +
                      ' bullist link | undo redo ',
                    branding: false,
                    statusbar: false,
                }}
            />

        </Box>
    );
});

export default  withStyles(styles)(TextEditor);