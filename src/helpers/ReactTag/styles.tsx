import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
const styles = (theme: any) => createStyles({
    root:{},

    reactTag: {
        width:'100%',
        '& .css-1s2u09g-control':{
            cursor:'pointer !important',
            background: COLORS.grey2,
            borderRadius:'12px',
            border:0,
            padding:'4px !important',

        },

        '& .css-12jo7m5':{

            fontStyle: 'normal',
            fontWeight: '700',
            fontSize: '13px',
            lineHeight: '24px',
            /* identical to box height, or 185% */

            letterSpacing: '-0.01em',

            /* Neutral/01 */

            color: COLORS.grey1,
        },

        '& .css-1pahdxg-control':{
            padding:'4px !important',
            border:0,
        },

        '& .css-1rhbuit-multiValue': {
            padding: '8px 16px',
            borderRadius: '8px',
            gap: '8px',
            background: '#2a85ff',
            color: COLORS.white,
        },

        '& .css-1rhbuit-multiValue .css-12jo7m5': {
            color: COLORS.white,
        }
    }


});

export default styles;
