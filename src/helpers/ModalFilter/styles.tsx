import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    modalFilterCustom: {
        position: 'relative',
        '& .box-all-filter': {
            width: 40,
            height: 40,
            border: `2px solid ${COLORS.grey3}`,
            borderRadius: '8px',
            marginLeft: 16,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            '&:hover .icon-svg': {
                color: COLORS.white,
            },
            '&:hover':{
                backgroundColor: COLORS.blue,
                border: `2px solid ${COLORS.blue}`,

            }
        },
        '& .modal-filter': {
            position: 'absolute',
            top: 0,
            right: 0,
            background: COLORS.white,
        }
    },
    modalBox: {
        borderRadius: '16px',
        boxShadow: '0px 0px 14px -4px rgba(0, 0, 0, 0.05), 0px 32px 48px -8px rgba(0, 0, 0, 0.1)',
        backdropFilter: 'blur(32px)',
        padding: 24,
        opacity: 0,
        visibility: 'hidden',
        background: COLORS.white,
        transition: 'all .2s ease',
        right: -50,
        top: -50,
        position: 'absolute' as const,
        '&.active': {
            opacity: 1,
            visibility: 'visible',
        },
        '& .box-title': {
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 20,
            '& .title': {
            
            },
            '& .box-close': {

            },
            '& .icon-btn-closed': {
                width: 36,
                height: 36,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: COLORS.grey3,
                borderRadius: '50%',
                transition: 'all .3s ease',
                '& .icon-svg': {
                    width: 20,
                    height: 20,
                    transition: 'all .3s ease',
                },
                '&:hover': {
                    '& .icon-svg': {
                        transform: 'rotate(90deg)',
                    }
                }
            }
        },
    }
});

export default styles;