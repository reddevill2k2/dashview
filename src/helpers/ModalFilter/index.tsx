import { COLORS } from '@constants/colors';
import { iconCloseLight, iconFilterLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import TitleElement from '@helpers/TitleElement';
import { Box, Modal, SxProps, IconButton, Backdrop, Fade } from '@mui/material';
import Popover from '@mui/material/Popover';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import { useForm } from 'react-hook-form';
import styles from './styles';

interface IModalFilter {
    children: React.ReactNode | undefined,
    openModal: boolean,
    handleCloseModal: () => void,
    handleOpenModal: () => void,
    title?: string,
    width?: number,
    height?: number,
    style?: any,
    sx?: SxProps,
    className?: string,
    bgTitle?: string,
    onSubmit?: any,
}


const ModalFilter = (props: IModalFilter & WithStyles<typeof styles>) => {
    const {
        classes, 
        className,
        children, 
        openModal=false,
        title,
        width,
        height,
        handleCloseModal,
        handleOpenModal,
        style, 
        sx,
        bgTitle,
        onSubmit,
        ...otherProps} = props;

    console.log('openModal');

    const onSubmit1 = (data: any) => {
        console.log({data});
    };
        

    const {handleSubmit, register} = useForm();

    return (
        <Box className={classes.modalFilterCustom}>
            <Box className="box-all-filter">
                <IconButton className='icon-filter'
                    onClick={handleOpenModal}>
                    <IconSVG icon={iconFilterLight}
                        fill={COLORS.grey4}/>
                </IconButton>
            </Box>
            <Box className={clsx(classes.modalBox, {
                ['active']: openModal,
            })}
            style={{
                ...width && {width},
                ...height && {minHeight: height},
            }}
            >
                <Box className={'box-title'}
                    display='flex'
                    justifyContent={'space-between'}>
                    <Box>
                        {title &&  <TitleElement bg={bgTitle || COLORS.orangeLight}>{title}</TitleElement>}
                    </Box>
                    <IconButton className='icon-btn-closed'
                        onClick={handleCloseModal}>
                        <IconSVG icon={iconCloseLight}
                            fill={COLORS.grey5}/>
                    </IconButton>
                </Box>
                <form autoComplete='off'
                    onSubmit={handleSubmit(onSubmit1)}>
                    {children}
                </form>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ModalFilter);