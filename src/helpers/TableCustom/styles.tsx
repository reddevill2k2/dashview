import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    root: {

    },
    boxTable: {
        width: '100%',
        overflowX: 'auto'
    },
    tableCustom: {
        overflowX: 'auto',
        // display: 'block !important',
        '& .text': {
            // minWidth: 'max-content',
        }
    },
    checkboxRoot: {
        
        borderRadius: '6px',
    },
    checkboxChecked: {
        border: `2px solid ${COLORS.grey4}`,
        borderRadius: '6px',
        color: `${COLORS.blue} !important`,
    },
    tableRowHeader: {
        borderBottom: `2px solid ${COLORS.grey2}`,
    },
    tableRowBody: {
        borderBottom: `2px solid ${COLORS.grey2}`,
        transition: 'all .3s ease',
        '&.active:hover': {
            backgroundColor: COLORS.grey2,
        }
    },
    boxChangePage: {
        marginTop: 32,
        marginBottom: 32,
        '& .icon-button': {
            width: 40,
            height: 40,
            marginRight: 8,
            borderRadius: '40px',
            border: `1px solid ${COLORS.grey3}`,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        }
    },
    tableCellBody: {
        // paddingLeft: '0 !important',
    }
});

export default styles;