import Text from '@helpers/Text';
import { withStyles } from '@mui/styles';
import { Box } from '@mui/system';
import React, {forwardRef} from 'react';
import { Helmet } from 'react-helmet-async';
import styles from './styles';

interface IPage {
    title: string,
    titlePage: string | undefined,
    children: React.ReactNode,
    classes?: any,
}

// eslint-disable-next-line react/display-name
const TitlePageComponent = forwardRef((props: IPage, ref: unknown) => {
    const {title, titlePage, classes, children, ...otherProps} = props;
    
    return (
        <Box ref={ref}
            {...otherProps}>
            <Helmet>
                <title>{title}</title>
            </Helmet>
            <Box
                className={classes.root}
            >
                <Text h3
                    className={classes.titlePageCustom}
                >
                    {titlePage}
                </Text>
                <Box component={'div'}
                    className={classes.childrenCustom}
                >
                    {children}
                </Box>
            </Box>
        </Box>
    );
});

export default withStyles(styles)(TitlePageComponent);