import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root:{},
    ModalCustomTime:{
        padding: '24px',
        position: 'absolute',
        left: '103%',
        top: '0%',
        borderRadius:16,
        opacity:0,
        visibility: 'hidden',
        backgroundColor: COLORS.white,
        transition: 'all .3s ease 0s',
        '& .box-title': {
            display: 'flex',
            justifyContent: 'space-between',
            paddingBottom: '20px',
            borderBottom: `1px solid ${COLORS.grey3}`,
            marginBottom: 24,
            '& .title': {
          
            },
            '& .box-close': {

            },
            '& .icon-btn-closed': {
                width: 36,
                height: 36,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: COLORS.grey3,
                borderRadius: '50%',
                transition: 'all .3s ease',
                '& .icon-svg': {
                    width: 20,
                    height: 20,
                    transition: 'all .3s ease',
                },
                '&:hover': {
                    '& .icon-svg': {
                        transform: 'rotate(90deg)',
                    }
                }
            }
        },
        '&.showModal':{
            opacity:1,
            visibility: 'visible',
        },
        '& .react-datepicker':{
            border:'none',
            '&__header':{
                display: 'none',
            },
            '&--time-only':{
                width:232,
                height:360,
            },
            '&__time-container':{
                width:'100%',

            },
            '&__time-box':{
                width:'100% !important',

            },
            '&__time-list':{
                maxHeight:360,
                height: '100% !important',
                '&-item':{
                    textAlign:'left',
                    color:COLORS.grey4,
                    padding: '0 12px !important',
                    fontSize:'14px',
                    lineHeight: '40px',
                    cursor: 'pointer',
                    transition: 'all .2s',
                    borderRadius: 4,
                    fontWeight:600,
                    height:'40px !important',
                    '&--selected':{
                        backgroundColor: `${COLORS.grey3} !important`,
                        color:`${COLORS.grey7} !important`,
                      
                    },
                    '&:hover':{
                        backgroundColor: COLORS.grey3,
                        color:COLORS.grey7,
                      
                    }
                },
            },
            
        },
        '& .box-button-custom':{
            display: 'flex',
            alignItems: 'center',
            justifyContent:'flex-end',
          
        }

    },
});

export default styles;