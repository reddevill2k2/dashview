import { Box, IconButton } from '@mui/material';
import Popover from '@mui/material/Popover';
import { withStyles, WithStyles } from '@mui/styles';
import * as React from 'react';
import styles from './styles';
import DatePicker  from 'react-datepicker';
import IconSVG from '@helpers/IconSVG';
import { iconChevronBackward, iconChevronForward, iconCloseLight } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import moment from 'moment';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Button from '@helpers/Button';
import ModalCustom from '@helpers/ModalCustom';
import TitleElement from '@helpers/TitleElement';

interface TimeCustomProps {
    data:any,
    handleSetDate: (e:any) => void,
    handleCloseModal: () => void,
    openModal:boolean,
}

const TimeCustom = (props: TimeCustomProps & WithStyles<typeof styles>) => {
    const {classes,data,handleSetDate,handleCloseModal,openModal, ...other} = props;

    return (
        <Box className={classes.ModalCustomTime + (openModal ? ' showModal':'')}>
            <Box className={'box-title'}
                display='flex'
                justifyContent={'space-between'}>
                <Box>
                    <Text titleSB1
                        color={COLORS.grey7}>{moment(data).format('h:mm A')}</Text >
                </Box>
                <IconButton className='icon-btn-closed'
                    onClick={handleCloseModal}>
                    <IconSVG icon={iconCloseLight}
                        fill={COLORS.grey5}/>
                </IconButton>
            </Box>
            <Box className={'box'}>
                <Box className={'calendar-box'}>
                    <DatePicker
                        selected={data}
                        onChange={(date) => handleSetDate(date)}
                        showTimeSelect
                        showTimeSelectOnly
                        showPopperArrow={false}
                        timeIntervals={30}
                        timeCaption="Time"
                        dateFormat="h:mm aa"
                        inline
                    />  
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(TimeCustom);