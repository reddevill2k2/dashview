import iconAddWhite from '@assets/icons/UIicon/add/filled.svg';
import { COLORS } from '@constants/colors';
import { Button, SxProps } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import Text from '../Text';
import styles from './styles';

interface ButtonCustomProps {
    children: React.ReactNode,
    iconBefore?: any,
    iconAfter?: any,
    isIconBefore?: boolean,
    isIconAfter?: boolean,
    color?: string,
    className?: any,
    sx?: SxProps,
    onClick?: any,
    type?: string,
}


const ButtonCustom = (props: ButtonCustomProps & WithStyles<typeof styles>) => {
    const {classes,
        children,
        iconBefore,
        iconAfter,
        isIconBefore,
        isIconAfter,
        color,
        className,
        ...otherProps} = props;
    return (
        <React.Fragment>
            <Button
                className={clsx(
                    classes.buttonCustom,
                    'button-custom',
                    {
                        [classes.colorPrimary]: color === 'primary',
                        [classes.colorDefault]: color === 'default',
                        [classes.colorTransparent]: color === 'transparent',
                    },
                    className && className,
                )}
                style={{
                    textTransform: 'capitalize',

                }}
                {...otherProps}
            >
                {
                    isIconBefore && (
                        <img src={iconBefore || iconAddWhite}
                            width={20}
                            height={20}
                            className={classes.iconBefore}

                        />
                    )
                }
                <Text button1>
                    {children}
                </Text>
                {
                    isIconAfter && (
                        <img src={iconAfter || iconAddWhite}
                            width={20}
                            height={20}
                            className={classes.iconBefore}

                        />
                    )
                }
            </Button>
        </React.Fragment>
    );
};

export default withStyles(styles)(ButtonCustom);