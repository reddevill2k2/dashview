import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import Text from '../Text';
import styles from './styles';

interface IButtonTransparent {
    children: React.ReactNode,
    onClick?: ()=> void;
    className?: string;
    isElement?: boolean;
    isLoading?: boolean,
    disabled?: boolean,
    style?: any,
}


const ButtonTransparent = (props: IButtonTransparent & WithStyles<typeof styles>) => {
    const {classes, children, className,isElement, isLoading, disabled, style, ...otherProps} = props;
    return (
        <React.Fragment>
            <Box 
                className={clsx(
                    className && className,
                    classes.ButtonTransparent, 
                    'button-transparent',
                    disabled && 'disabled')}
                style={style}
                {...otherProps}
            >
                {isLoading && (
                    <Box className={classes.loading}>
                        <span></span>
                        <span></span>
                        <span></span>
                    </Box>
                )}
                {isElement ? children: (<Text center
                    className='button-text'>
                    {children}
                </Text>)}
                
            </Box>
        </React.Fragment>
    );
};

export default withStyles(styles)(ButtonTransparent);