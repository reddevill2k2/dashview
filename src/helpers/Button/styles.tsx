import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    buttonCustom: {
        borderRadius: '12px !important',
        padding: '12px 20px !important',
        transition: 'all .3s ease',
        '& .text': {
            color: `${COLORS.white} !important`,
        }
    },
    colorPrimary: {
        color: `${COLORS.white} !important`,
        backgroundColor: `${COLORS.blue} !important`,
        '&:hover': {
            backgroundColor: `${COLORS.blueHover} !important`,
        }
    },
    colorDefault: {
        color: `${COLORS.black} !important`,
        backgroundColor: `${COLORS.white} !important`,
    },
    colorTransparent: {
        color: `${COLORS.black}`,
        backgroundColor: 'transparent',
        '& .text': {
            color: `${COLORS.grey4} !important`,
        }
    },
    iconBefore: {
        marginRight: 8,
    },
    ButtonTransparent: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        cursor: 'pointer',
        padding: '12px 20px',
        borderRadius: 12,
        border: `2px solid ${COLORS.grey3}`,
        transition: 'all 0.4s ease',
        '& .button-text': {
            fontWeight: 700,
            fontSize: 15,
            color: COLORS.grey7,
        },
        '&:not(.disabled):hover': {
            border: `2px solid ${COLORS.grey4}`,
        },
        '&.disabled': {
            cursor: 'not-allowed',
            border: `2px solid ${COLORS.grey2}`,
            '& .button-text': {
                fontWeight: 700,
                fontSize: 15,
                color: COLORS.grey4,

            },
        }
    },
    loading: {
        color: 'rgba(255, 255, 255, 0)',
        marginRight: 8,
        '& span': {
            height: 5,
            width: 5,
            marginRight: 2,
            borderRadius: '50%',
            background: COLORS.grey4,
            display: 'inline-block',
            animation: 'loadingButton 0.5s infinite linear alternate',
            '&:nth-child(2)': {
                animationDelay: '0.3s',
            },
            '&:nth-child(3)': {
                animationDelay: '0.6s'
            },
        }
    }
});

export default styles;