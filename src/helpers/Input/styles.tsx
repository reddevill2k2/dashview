import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    notification: {
        color: COLORS.grey7,
        width: 24,
        height: 24,
        borderRadius: 6,
        justifyContent: 'center',
        display: 'flex',
        alignItems: 'center',
    },
    boxIconAdd: {
        width: 24,
        height: 24,
        borderRadius: 24,
        border: `2px solid ${COLORS.grey3}`,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 8,
        '&:hover': {
            borderColor: COLORS.grey7,
        }
    },
    searchBox: {
        backgroundColor: COLORS.grey2,
        borderRadius: 12,
        color: COLORS.shades40,
        position: 'relative',
        width: 360,
        height: 48,
        display: 'flex',
        alignItems: 'center',
    },
    iconSearch: {
        position: 'absolute',
        left: 8,
        top: 12,
        bottom: 12,
    },
    appBarStyle: {
        width: 'calc(100% - 340px) !important',
        boxShadow: 'none !important',
        padding: 20
    },
    inputSearch: {
        '& input': {
            fontWeight: 'bold',
            padding: '10px 25px 10px 35px',
            border: '2px solid transparent',
            borderRadius: 12,
            '&:focus': {
                outline: 'none',
                border: `2px solid  ${COLORS.blue}`,
            }
        }

    },
    commandF: {
        padding: '4px 12px',
        borderRadius: 8,
        backgroundColor: COLORS.white,
        position: 'absolute',
        top: 8,
        color: COLORS.black,
        bottom: 8,
        cursor: 'pointer',
        right: 12,
        //width: 56,
        // color: COLORS.black,
        fontWeight: 'bold',
        transition: 'all .3s ease',
        width:'max-content',
        '&:hover': {
            color: COLORS.blueHover,
        }
    },
    boxClosed: {
        width: 30,
        height: 30,
        position: 'absolute !important',
        top: 8,
        bottom: 8,
        right: 8,
        backgroundColor: `${COLORS.grey3} !important`,
        '&:hover .icon-svg': {
            color: COLORS.red,
        }
    },
    price: {
        position: 'relative',
        width: '100%',
        '& > div:first-of-type': {
            overflow: 'hidden',
            position: 'absolute !important',
            width: '100%',
            border: '2px solid #EFEFEF',

        },
        '& input': {
            background: '#FFFFFF !important',
            paddingLeft: '58px !important',
        },
        '& .box-icon':{
            position:'absolute',
            width: '48px',
            height: '48px',
            background: `${COLORS.grey2} !important`,
            borderTopLeftRadius: '12px',
            borderBottomLeftRadius: '12px',
            display:'flex',
            alignItems:'center',
            justifyContent:'center'
        }
    }
});

export default styles;