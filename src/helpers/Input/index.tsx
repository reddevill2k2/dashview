import iconSearch from '@assets/icons/search/light.svg';
import { iconCloseLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import { Box, IconButton, Input, SxProps } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import _ from 'lodash';
import * as React from 'react';
import {  FieldValues, UseFormRegister, UseFormReturn } from 'react-hook-form';
import styles from './styles';

interface IInputCustom {
    placeholder?: string;
    isFocused?: boolean;
    disableIconSearch?: false ;
    onBlur?: ()=> void;
    onChange?: ()=> void;
    onfocus?: ()=> void;
    commandF?: boolean;
    className?: string;
    style?: any;
    search?: string,
    handleSearch?: (e: any)=> void,
    sx?: SxProps,
    register?: UseFormReturn['register']
}

const InputCustom = React.forwardRef((props: IInputCustom & WithStyles<typeof styles>,
    ref: React.RefObject<HTMLInputElement>) => {
    const {classes, isFocused, placeholder, commandF, search, style
        , handleSearch,disableIconSearch,className, ...otherProps} = props;
    return (
        <Box
            className={clsx(
                className && className,
                classes.searchBox,
            )}
            style={style}
        >
            {
                !disableIconSearch && <Box
                    component={'div'}
                    className={classes.iconSearch}
                >
                    <img src={iconSearch}
                        width={24}
                        height={24}/>
                </Box>
            }

            <Input
                autoFocus={isFocused}
                fullWidth
                placeholder={placeholder}
                disableUnderline
                // value={search}
                onChange={handleSearch}
                className={classes.inputSearch}
                ref = {ref}
                {...otherProps}
            />
            {
                commandF && !search ?
                    (<Box
                        component={'div'}
                        className={classes.commandF}
                    >
                  ⌘ F
                    </Box>): search &&
                    (<IconButton
                        className={classes.boxClosed}
                        onClick={()=> {
                            if(_.isFunction(handleSearch)) {
                                handleSearch(null);
                            }
                        }}
                    >
                        <IconSVG icon={iconCloseLight} />
                    </IconButton>)
            }

        </Box>
    );
});

export default withStyles(styles)(InputCustom);