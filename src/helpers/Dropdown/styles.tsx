import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    popverStyle: {
        borderRadius: 16,
        padding: 28,
        '& .MuiPopover-paper': {
            overflow: 'unset',
            borderRadius: 16,
            boxShadow: '0px 32px 48px -8px rgb(0 0 0 / 10%), 0px 0px 14px -4px rgb(0 0 0 / 5%)'
        }
    },
    childrenPopover: {
        position: 'relative',
        '&::before': {
            position: 'absolute',
            content: '""',
            backgroundColor:COLORS.white,
            width: 18,
            height: 18,
            top: -10,
            right: 50,
            transform: 'rotate(45deg)'
        }
    }
});

export default styles;