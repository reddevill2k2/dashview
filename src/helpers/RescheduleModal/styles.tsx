import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    root:{},
    contentModal: {
        borderTop: `1px solid ${COLORS.grey3}`,
        '& .modal-party': {
            width: 128,
            height: 128,
            borderRadius: '50%',
            backgroundColor: COLORS.greenLight,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        
        },
        '& .button-modal':{
            marginTop:8,
            justifyContent: 'flex-start',
            transition: 'border .3s ease 0s',
            '&:hover': {
                borderColor:COLORS.blue,
            },
            '& .icon':{
                marginRight:'12px'
            },
            '&.active-button':{
                borderColor:COLORS.blue,
                '& .icon-svg': {
                    color: COLORS.grey7,
                }
            }
        },
        '& .box-content-text': {
            textAlign: 'center',
            marginTop: 20,
      
        },
        '& .box-calendar':{
            marginTop: 12,
        },
        '& .box-button-custom': {
            marginTop: 20,
        }
    }
});

export default styles;