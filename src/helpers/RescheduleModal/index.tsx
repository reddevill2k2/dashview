import { Box, IconButton } from '@mui/material';
import Popover from '@mui/material/Popover';
import { withStyles, WithStyles } from '@mui/styles';
import * as React from 'react';
import styles from './styles';
import DatePicker  from 'react-datepicker';
import IconSVG from '@helpers/IconSVG';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import moment from 'moment';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Button from '@helpers/Button';
import ModalCustom from '@helpers/ModalCustom';
import DateCustom from '@helpers/DateCustom';
import TimeCustom from '@helpers/TimeCustom';
import { iconCalendar, iconClock } from '@constants/imageAssets';

interface DateCustomProps {
    handleSetDate:(selectedRow:any) => void,
    handleOpenModalRechedule: () => void,
    handleCloseModalRechedule: () => void,
    handleOpenSelectDate: () => void,
    handleCloseSelectDate: () => void,
    handleOpenSelectTime: () => void,
    handleCloseSelectTime: () => void,
    handlReschedule: () => void,
    openModalRechedule: boolean,
    openModalSelectDate: boolean,
    openModalSelectTime: boolean,
    dateSelected: any,
}

const RescheduleModal = (props: DateCustomProps & WithStyles<typeof styles>) => {
    const {classes,
        dateSelected ,
        handleCloseModalRechedule,
        handleOpenSelectDate,
        handleCloseSelectDate,
        handleSetDate, 
        handleOpenSelectTime,
        handleCloseSelectTime,
        handlReschedule,
        openModalSelectTime,
        openModalSelectDate,
        openModalRechedule} = props;
    const ContentModal =  () => {
        return (
        // eslint-disable-next-line react/prop-types
            <Box className={classes.contentModal}>
                <Box textAlign={'center'}
                    display='flex'
                    justifyContent={'center'}
                >
                </Box>
                <Box className="box-content-text">
                    <Text bodyM1
                        color={COLORS.grey4}>
                        Choose a day and time in the future you want your product to be published.
                    </Text>
                    <Box className="box-calendar">
                        <ButtonTransparent isElement
                            onClick={() =>{
                                handleOpenSelectDate();
                                handleCloseSelectTime();
                            }}
                            className={'button-modal' + (openModalSelectDate? ' active-button' :'')}
                        >
                            <IconSVG
                                className={'icon'} 
                                width={36}
                                height={24}
                                icon={iconCalendar}
                                fill={COLORS.grey4}/>
                            <Box>
                                <Text captionM2
                                    color={COLORS.grey4}>Date</Text>
                                <Text baseSB1
                                    color={COLORS.grey7}>{moment(dateSelected).format('DD MMMM, YYYY')}</Text>
                            </Box>
                        </ButtonTransparent>
                        <ButtonTransparent isElement
                            onClick={() =>{
                                handleOpenSelectTime();
                                handleCloseSelectDate();
                            }}
                            className={'button-modal' + (openModalSelectTime? ' active-button' :'')}
                        >
                            <IconSVG
                                className={'icon'} 
                                width={36}
                                height={24}
                                icon={iconClock}
                                fill={COLORS.grey4}/>
                            <Box>
                                <Text captionM2
                                    color={COLORS.grey4}>Time</Text>
                                <Text baseSB1
                                    color={COLORS.grey7}>{moment(dateSelected).format('hh:mm A')}</Text>
                            </Box>
                        </ButtonTransparent>
                        <DateCustom  data={dateSelected}
                            handleSetDate={handleSetDate}
                            handleCloseModal={handleCloseSelectDate}
                            openModal={openModalSelectDate} />
                        <TimeCustom data={dateSelected}
                            handleSetDate={handleSetDate}
                            handleCloseModal={handleCloseSelectTime}
                            openModal={openModalSelectTime} />          
                    </Box>
                    <Box className='box-button-custom'
                        textAlign={'right'}>
                        <Button color={'primary'}
                            onClick={() =>{
                                handleCloseSelectTime();
                                handleCloseSelectDate();
                                handleCloseModalRechedule();
                                handlReschedule();
                            }}>Reschedule</Button>
                    </Box>
                </Box>
            </Box>
        );
    };
    return (
        <ModalCustom
            open={openModalRechedule}
            onClose={() =>{
                handleCloseModalRechedule();
                handleCloseSelectDate();
                handleCloseSelectTime();
            }}
            title={'Reschedule product'}
            bgTitle={COLORS.orangeLight}
            width={390}
            height={400}
        >
            {ContentModal()}
        </ModalCustom>
    );
};

export default withStyles(styles)(RescheduleModal);