import { COLORS } from '@constants/colors';
import { iconDown, iconUp } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, SxProps, Tooltip, Zoom } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import styles from './styles';

interface LabelColorCustomProps {
    title?: string,
    mode?: string,
    color?: string, 
    bg?: string, 
    className?: any,
    sx?: SxProps,
    titleMode?: string,
    numeral?: string,
    widthAfter?: number,
    styleCustom?: {
        label?: any,
        mode?: any,
    }
}


const LabelColorCustom = (props: LabelColorCustomProps & WithStyles<typeof styles>)=> {
    const {classes, 
        title,
        titleMode,
        mode, 
        numeral,
        bg, 
        color = COLORS.grey7, 
        className, 
        sx, 
        widthAfter, 
        styleCustom} = props;
    
    return (
        <Box className={clsx(className, classes.LabelCustom)}
            display={'flex'}
            alignItems={'center'}
            sx={{
                ...sx
            }}>
            <Box sx={{
                color: color,
                marginRight: 1,
                backgroundColor: bg || COLORS.white,
            }}
            className='label'>
                {title && (<Text baseSB1
                    color={color}>
                    {title}
                </Text>)}
            </Box>
            {mode && (
                <Box className="box-mode"
                    style={{...styleCustom?.mode && styleCustom?.mode}}
                    display={'flex'}>
                    <IconSVG className={'icon-mode'}
                        width={16}
                        height={16}
                        fill={mode === 'up' ? COLORS.green: COLORS.red}
                        sx={{marginRight: 0.5}}
                        icon={mode === 'up'? iconUp: iconDown}/>
                    {numeral && <Text color={mode === 'up' ? COLORS.green: COLORS.red}
                        sx={{marginRight: 0.5}}
                        caption2>
                        {numeral}
                    </Text>}
                    {titleMode && ( <Text color={COLORS.grey4}
                        caption2>
                        {titleMode}
                    </Text> )}
                </Box>
            )}
            {widthAfter && <Box style={{width: widthAfter, marginLeft: 8, ...styleCustom?.label && styleCustom.label}}
                className='box-after'/>}
        </Box>
    );
};

export default withStyles(styles)(LabelColorCustom);