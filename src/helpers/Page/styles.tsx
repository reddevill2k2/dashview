import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
        marginTop: 100,
        marginLeft: 340,
        padding: 40,
        backgroundColor:COLORS.grey3,
        minHeight: 'calc(100vh - 100px)',
    },
    titlePageCustom: {
        marginBottom: 24,
        color: `${COLORS.grey6} !important`,
    },
    childrenCustom: {
        // overflow: 'hidden',
        width: '100%',
        minHeight:'calc(100vh - 235px)'

    },
    contentPage: {
        // display: 'table',
        margin: '0 auto',
        maxWidth: 1200,
        width: '100%',
        zIndex: 20,
        position: 'relative',
    },
    boxTitlePage: {
        display: 'table',
        margin: '0 auto',
        maxWidth: 1200,
        width: '100%',
        marginBottom: 24,
    },
    bgPageCover: {
        height: 400,
        width: 'calc(100% + 100px)',
        zIndex: 10,
        marginLeft: -50,
        overflow: 'hidden',
        borderRadius: '20px',
        marginTop: -40,
        '& img': {
            width: '100%',
            height: 'auto',
            objectFit: 'cover',
        }
    }
});

export default styles;