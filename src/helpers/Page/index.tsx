import Text from '@helpers/Text';
import { withStyles } from '@mui/styles';
import { Box } from '@mui/system';
import React, {forwardRef} from 'react';
import { Helmet } from 'react-helmet-async';
import styles from './styles';

interface PageProps {
    title: string,
    titlePage?: string | undefined,
    children: React.ReactNode,
    classes?: any,
    className?: string,
    bgCoverPage?: any,
}

// eslint-disable-next-line react/display-name
const Page = forwardRef((props: PageProps, ref: unknown) => {
    const {title, titlePage, classes, children, bgCoverPage, ...otherProps} = props;
    
    return (
        <Box ref={ref}
            style={{overflowX: 'hidden'}}
            {...otherProps}>
            <Helmet>
                <title>{title}</title>
            </Helmet>
            
            <Box
                className={classes.root}
            >
                {!bgCoverPage  && (<Box className={classes.boxTitlePage}>
                    <Text h3
                        className={classes.titlePageCustom}
                    >
                        {titlePage}
                    </Text>
                </Box>)}
                <Box component={'div'}
                    className={classes.childrenCustom}
                >
                   
                    <Box className={classes.contentPage}>
                        {bgCoverPage && <Box className={classes.bgPageCover}>
                            <img src={bgCoverPage}
                                alt="" />
                        </Box>}
                        {children}
                    </Box>
                </Box>
            </Box>
        </Box>
    );
});

export default withStyles(styles)(Page);