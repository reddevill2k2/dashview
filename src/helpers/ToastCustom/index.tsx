import Text from '@helpers/Text';
import { withStyles } from '@mui/styles';
import { Box } from '@mui/system';
import React, {forwardRef} from 'react';
import { Helmet } from 'react-helmet-async';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styles from './styles';
import { SxProps } from '@mui/material';
import { iconCheckCircled, iconCloseLight, iconWarning } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import { COLORS } from '@constants/colors';

interface IToastCustom {
    message: string,
    classes?: any,
    success?:boolean,
    error?: boolean,
    warning?: boolean,
    sx?: SxProps,
}

// eslint-disable-next-line react/display-name
const ToastCustomComponent = (props: IToastCustom) => {
    const {message, classes,success,error,warning,sx, ...otherProps} = props;

    return (
        <Box sx={{display: 'flex', alignItems:'center'}}>
            {success && <IconSVG small
                sx={{marginRight:'8px'}}
                icon={iconCheckCircled}
                fill={COLORS.green}/>}
            {error && <IconSVG small
                sx={{marginRight:'8px'}}
                icon={iconWarning}
                fill={COLORS.red}/>}
            {warning && <IconSVG small
                sx={{marginRight:'8px'}}
                icon={iconWarning}
                fill={COLORS.yellowLight}/>}
            <Text base2
                color={COLORS.grey2}>{message}</Text>
        </Box>
    );
};

export default withStyles(styles)(ToastCustomComponent);

export const customToastNotify = (mode:string, message : string, darkTheme = true) =>{
    switch (darkTheme) {
        case true :
            switch (mode) {
                case 'success':
                    toast( <ToastCustomComponent success
                        message={message} />, {
                        position: 'bottom-left',
                        autoClose: 2000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme:'dark',
                    });
                    break;
                case 'error':
                    toast( <ToastCustomComponent error
                        message={message} />, {
                        position: 'bottom-left',
                        autoClose: 2000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme:'dark',
                    });
                    break;
            }
            break;

        case false:
            switch (mode) {
                case 'success':
                    toast( <ToastCustomComponent success
                        message={message} />, {
                        position: 'bottom-left',
                        autoClose: 2000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme:'light',
                    });
                    break;
                case 'error':
                    toast( <ToastCustomComponent error
                        message={message} />, {
                        position: 'bottom-left',
                        autoClose: 2000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme:'light',
                    });
                    break;
            }
            break;
    }
};