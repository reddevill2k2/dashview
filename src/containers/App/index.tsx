import * as React from 'react';
import {ThemeProvider} from '@mui/material/styles';
import theme from '@commons/theme/theme';
import {Typography} from '@mui/material';
import {ToastContainer, Slide} from 'react-toastify';
import CssBaseline from '@mui/material/CssBaseline';
import {BrowserRouter as Router, Routes} from 'react-router-dom';
import RouterDefault from '@commons/routes/index';
import {Provider} from 'react-redux';
import configureStore from '@redux/configStore';
import { HelmetProvider } from 'react-helmet-async';
import './main.scss';

function App() {
    const store = configureStore();
    return (
        <HelmetProvider>
            <ThemeProvider theme={theme}>
                <Provider store={store}>
                    <ToastContainer
                        position="top-center"
                        limit={1}
                        autoClose={50000}
                        transition={Slide}
                        closeButton={true}
                    />
                    <CssBaseline />
                    <Router>
                        <RouterDefault/>
                    </Router>
                </Provider>
            </ThemeProvider>
        </HelmetProvider>

    );
}

export default App;