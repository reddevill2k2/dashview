import StatementsComponent from '@components/StatementsComponent';
import { COLORS } from '@constants/colors';
import { iconActivityFilled, iconDonutChartLight, iconPieChar } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { createFakerList } from '@utils/';
import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { IStatementBanner, ITransactions } from './types';


const statementBannerList: IStatementBanner[] = [
    {
        icon:iconActivityFilled,
        title:'Funds',
        price:'128,789',
        bg:COLORS.greenNgoc
    },
    {
        icon:iconPieChar,
        title:'Earning',
        price:'128,789',
        bg:COLORS.violetLight
    },
    {
        icon:iconDonutChartLight,
        title:'Fees',
        price:'338,98',
        bg:COLORS.blueLight
    }
];


const Transactions = () =>{
    const currentTransaction:ITransactions = {
        date:`${faker.datatype.number({
            'min': 1,
            'max': 29
        })} ${faker.date.month({ abbr:true })} ${faker.datatype.number({
            'min': 2015,
            'max': 2022
        })}`,
        type:faker.datatype.boolean(),
        detail:'Fleet - Travel shopping UI Design kit',
        detail_thumbnail:'Invoice: UI8-8934AS',
        price:`${faker.datatype.number({
            'min': 1,
            'max': 200
        }).toLocaleString(undefined,{ minimumFractionDigits : 2})}`,
        amount:`${faker.datatype.number({
            'min': 1,
            'max': 200
        }).toLocaleString(undefined,{ minimumFractionDigits : 2})}`

    };

    return currentTransaction;
};

const StatementsContainer = () => {
    const initDataTable:ITransactions[] = useMemo(()=>createFakerList(Transactions,100),[]);

    const [dataTableTransactions, setDataTableTransactions] =
    useState<ITransactions[]>([...initDataTable]);

    const [currentOption, setCurrentOption] = useState<number>(1);

    const onHandleChangeSelect = (option: number) => {
        if(option === currentOption) return;
        switch (option){
            case 1:
                setDataTableTransactions(initDataTable);
                setCurrentOption(option);
                break;
            case 2:
                setDataTableTransactions(()=>createFakerList(Transactions,100));
                setCurrentOption(option);
                break;
            case 3:
                setDataTableTransactions(()=>createFakerList(Transactions,100));
                setCurrentOption(option);break;
            default:
                setDataTableTransactions(initDataTable);
                setCurrentOption(option); break;
        }
    };
    return (
        <StatementsComponent title='Statement Page'
            statementBannerList = {statementBannerList}
            dataTableTransactions = {dataTableTransactions}
            handleChangeSelect = {onHandleChangeSelect}
            titlePage={'Statement'} />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(StatementsContainer);