export interface IStatementBanner{
    icon:string,
    title:string,
    price:string,
    bg:string,
}

export interface ITransaction{
    date:string,
    type:boolean,
    detail:string,
    detail_thumbnail:string,
    price:string,
    amount:string
}