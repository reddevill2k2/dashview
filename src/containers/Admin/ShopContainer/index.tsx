import DraffsComponent from '@components/DraffsComponent';
import ShopComponent from '@components/ShopComponent';
import { 
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9
} from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';

const listImage = [
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9,
];

const listUser = [
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
];

const singleDataShop = ()=> {
    return {
        title: faker.lorem.sentence(4),
        price: `$${_.random(20, 100)}`,
        rating: `${_.random(3, 5, true).toFixed(1)}`,
        totalRating: `${_.random(20, 100)}`,
        preview: faker.helpers.arrayElement(listImage),
        url: '',
    };
};

const singleDataFollower = ()=> {
    return {
        name: faker.name.findName(),
        totalProduct: _.random(1, 30),
        isMessage: faker.datatype.boolean(),
        totalFollower: _.random(100, 1000),
        preview: faker.helpers.arrayElement(listUser),
        listProduct: faker.helpers.arrayElements(listImage,3),
    };
};

const ShopContainer = () => {
    const dataListShopFaker = useMemo(()=> createFakerList(singleDataShop, 100), []);
    const dataListFollowerFaker = useMemo(()=> createFakerList(singleDataFollower, 100), []);
    const dataListFollowingFaker = useMemo(()=> createFakerList(singleDataFollower, 100), []);
    const [dataListShop, setDataListShop]= useState(dataListShopFaker);
    const [dataListFollower, setDataListFollower] =useState(dataListFollowerFaker);
    const [dataListFollowing, setDataListFollowing] = useState(dataListFollowingFaker);
    const [tabActive, setTabActive] = useState<number>(0);
    const [pagination, setPagination] = useState<number>(1);
    const [isLoadingMore, setIsLoadingMore] = useState<boolean>(false);

    const handleChangeTabActive = (index: number)=> {
        setTabActive(index);
        setPagination(1);
    };

    const handleChangePaginationProducts = async () => {
        await setIsLoadingMore(true);
        await setTimeout(()=> {
            setIsLoadingMore(false);
            setPagination((value)=> value + 1);
        }, 800);
    };

    return (
        <ShopComponent title='Shop Page'
            dataListShop={dataListShop}
            dataListFollower={dataListFollower}
            dataListFollowing={dataListFollowing}
            tabActive={tabActive}
            pagination={pagination}
            handleChangePaginationProducts={handleChangePaginationProducts}
            isLoadingMore={isLoadingMore}
            handleChangeTabActive={handleChangeTabActive}
            titlePage={'Shop'} />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ShopContainer);