import HomeComponent from '@components/HomeComponent';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import imageOverviewUser1 from '@assets/images/img_overview_user_1.png';
import imageOverviewUser2 from '@assets/images/img_overview_user_2.png';
import imageOverviewUser3 from '@assets/images/img_overview_user_3.png';

const HomeContainer = () => {
    const [selectTabOverView, setSelectTabOverView] = useState(1);

    const overviewUserDummyData = [
        {
            id: 1,
            url: imageOverviewUser1,
            name: 'Gladyce',
        },
        {
            id: 2,
            url: imageOverviewUser2,
            name: 'Elbel',
        },
        {
            id: 3,
            url: imageOverviewUser3,
            name: 'Joyce',
        },
    ];

    const handleChangeSelectTabOverview = (value: number) => {
        setSelectTabOverView(value);
    };
    return (
        <HomeComponent title='Home page'
            titlePage={'Dashboard'}
            handleChangeSelectTabOverview={handleChangeSelectTabOverview}
            selectTabOverView={selectTabOverView}
            overviewUserDummyData={overviewUserDummyData}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);