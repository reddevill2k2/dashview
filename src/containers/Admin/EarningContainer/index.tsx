import EarningComponent from '@components/EarningComponent';
import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import faker from '@faker-js/faker';
import _ from 'lodash';
import { createFakerList } from '@utils/';
import { formatDate } from '@utils/';

const singleDataContries = () => {
    return {
        code: faker.address.countryCode(),
        name: faker.address.county(),
        earnings: `$${_.round(_.random(10, 1000, true), 1).toFixed(2)}`,
    };
};
const singleDataEarningResults = () => {
    return {
        date: formatDate(faker.date.between('2010-01-01', '2022-01-01')),
        status: faker.datatype.boolean()?'Paid':'Pending',
        salecount: `${_.round(_.random(10, 99, true), 0)}`,
        earnings: `$${_.round(_.random(10, 10000, true), 1).toFixed(2)}`,
    };
};

const EarningContainer = () => {
    const dataTableContriesFaker =  useMemo(()=> createFakerList(singleDataContries, 6), []);

    const [dataTableContries, setDataTableContries] = useState<any>(dataTableContriesFaker);
    const dataTableEarningResultsFaker =  useMemo(()=> createFakerList(singleDataEarningResults, 7), []);

    const [dataTableEarningResults, setDataTableEarningResults] = useState<any>(dataTableEarningResultsFaker);

    return (
        <EarningComponent title='Earning Page'
            dataTableContries={dataTableContries}
            dataTableEarningResults={dataTableEarningResults}
            titlePage='Earning' />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(EarningContainer);