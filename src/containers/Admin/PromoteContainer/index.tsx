import PromoteComponent from '@components/PromoteComponent';
import { COLORS } from '@constants/colors';
import { iconImageFilled, iconVideoRecorderFilled, imagePost1, imagePost10, imagePost2, imagePost3, imagePost4, imagePost5, imagePost6 } from '@constants/imageAssets';

import faker from '@faker-js/faker';
import { createFakerList, generateRandomListFromANumber } from '@utils/index';
import _ from 'lodash';
import React, { useCallback, useMemo, useState } from 'react';
import { connect } from 'react-redux';

const listColor = [COLORS.greenLight, COLORS.violetLight, COLORS.blueLight, COLORS.yellowLight];
const listImagePosts = [
    imagePost1,
    imagePost2,
    imagePost3,
    imagePost4,
    imagePost5,
    imagePost6,
    // imagePost7,
    // imagePost8,
    // imagePost9,
    // imagePost10,
];


const singleDataPost = ()=> {
    return {
        post: {
            title: faker.lorem.sentence(4),
            description: faker.lorem.sentence(3),
            thumbnail: faker.datatype.boolean() ? iconImageFilled : iconVideoRecorderFilled,
            preview: faker.helpers.arrayElement(listImagePosts),
            linkFacebook: faker.internet.url(),
            linkTwitter: faker.internet.url(),
            
        },
        distribution: {
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(1, 10, true), 1).toFixed(1)}x`,
        },
        linkClicks:{
            mode: faker.datatype.boolean() ? 'down': 'up',
            quantity:`${_.round(_.random(10, 100, true))}`,
            numeral: `${_.round(_.random(10, 100, true), 1)}%`, 
            width: _.random(10, 40), 
        }, 
          
        views: {
            quantity: `${_.round(_.random(10, 300, true))}`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 1)}%`,
            width: _.random(10, 40), 

        },
        engagement: {
            quantity: `${_.round(_.random(10, 300, true))}`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 1)}%`,
            width: _.random(10, 40), 
        },
        color: faker.helpers.arrayElement(listColor),
    };
};


const PromoteContainer = () => {
    const dataTablePostFaker =  useMemo(()=> createFakerList(singleDataPost, 100), []);

    const [dataTablePost, setDataTablePost] = useState<any>(dataTablePostFaker);

    return (
        <PromoteComponent title='Promote Page'
            dataTablePost={dataTablePost}
            titlePage={'Promotes'} />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(PromoteContainer);