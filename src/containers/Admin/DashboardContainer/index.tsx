import DashboardComponent from '@components/DashboardComponent';
import { COLORS } from '@constants/colors';
import { iconChevronDown, iconChevronUp, iconDown, iconInfo, iconPaymentLight, iconShoppingBag, iconUp, imageSmallLine1, imageSmallLine2, imageSmallLine3, iconActivityFilled, imageProduct1, imageProduct10, imageProduct2, imageProduct3, imageProduct4, imageProduct5, imageProduct6, imageProduct7, imageProduct8, imageProduct9 } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { customToastNotify } from '@helpers/ToastCustom';
import { createFakerList, generateRandomListFromANumber } from '@utils/index';
import _ from 'lodash';
import moment from 'moment';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';

const listColor = [COLORS.greenLight, COLORS.violetLight, COLORS.blueLight, COLORS.yellowLight];
const listImageProduct = [
    imageProduct1,
    imageProduct2,
    imageProduct3,
    imageProduct4,
    imageProduct5,
    imageProduct6,
    imageProduct7,
    imageProduct8,
    imageProduct9,
    imageProduct10,
];


const singleDataProduct = ()=> {
    return {
        products: {
            title: faker.lorem.sentence(4),
            description: faker.lorem.sentence(3),
            preview: faker.helpers.arrayElement(listImageProduct),
        },
        status: faker.datatype.boolean()?'Active': 'Deactive',
        price: `${_.round(_.random(10, 100, true), 2)}$`,
        sales: {
            quantity: `${_.round(_.random(10, 100, true), 3)}$`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2)}%`,  
        },
        views: {
            quantity: `${_.random(10, 100)}k`,
            width: _.random(10, 40),
        },
        likes: {
            quantity: `${_.random(2, 50)}k`,
            width: _.random(10, 40),
        },
        viewers: {
            followers:  {
                title: 'Followers',
                value: generateRandomListFromANumber(800, 2)[0],
                color: COLORS.green,
            },
            others:  {
                title: 'Others',
                value: generateRandomListFromANumber(800, 2)[1],
                color: COLORS.violet,
            },
        },
        traffic_sources: {
            market:  {
                title: 'Market',
                value: generateRandomListFromANumber(800, 5)[0],
                color: COLORS.orangeLight,
            },
            social_media:  {
                title: 'Source media',
                value: generateRandomListFromANumber(800, 5)[1],
                color: COLORS.violet,
            },
            direct:  {
                title: 'Direct',
                value: generateRandomListFromANumber(800, 5)[2],
                color: COLORS.green,
            },
            dashview:  {
                title: 'Dashview',
                value: generateRandomListFromANumber(800, 5)[3],
                color: COLORS.blue,
            },
            other:  {
                title: 'Other',
                value: generateRandomListFromANumber(800, 5)[4],
                color: COLORS.yellowLight,
            },
        },
        color: faker.helpers.arrayElement(listColor),
    };
};

const singleProductActivity = () =>([
    {
        start_week: `${moment(faker.date.past()).format('D MMM')}`,
        end_week: `${moment(faker.date.past()).format('D MMM')}`,
        products: {
            quantity: `${_.random(10, 100)}`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2).toFixed(2)}%`,  
        },
        views: {
            quantity: `${_.random(10, 100)}k`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2).toFixed(2)}%`,  
        },
        likes: {
            quantity: `${_.random(10, 100)}`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2).toFixed(2)}%`,  
        },
        comments: {
            quantity: `${_.random(10, 100)}`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2).toFixed(2)}%`,  
        },
        colors: [COLORS.greenLight, COLORS.violetLight, COLORS.blueLight, COLORS.yellowLight]
    },
    {
        start_week: `${moment(faker.date.past()).format('D MMM')}`,
        end_week: `${moment(faker.date.past()).format('D MMM')}`,
        products: `${_.random(10, 100)}`,
        views: `${_.random(10, 100)}k`,
        likes: `${_.random(10, 100)}`,
        comments: `${_.random(10, 100)}`,
        mode: faker.datatype.boolean() ? 'down': 'up',
        numeral: `${_.round(_.random(10, 100, true), 2).toFixed(2)}%`,
        colors: [COLORS.grey3, COLORS.grey3, COLORS.grey3, COLORS.grey3]
    }
]);

const DashboardContainer = () => {
    const [tabActiveProduct, setTabActiveProduct] = useState<number>(0);
    const [searchProduct, setSearchProduct] = useState<string>('');
    const dataProductActivityFaker = useMemo(()=> singleProductActivity(),[] );
    const dataTableMarketFaker =  useMemo(()=> createFakerList(singleDataProduct, 100), []);
    const [dataTableMarket, setDataTableMarket] = useState<any>(dataTableMarketFaker);
    const [dataTableProductActivity, setDataTableProductActivity] = useState<any>(dataProductActivityFaker);
    const [dataChartFaker, setDataChartFaker] = useState<any>(listChartDummyData);
    const [fillterOverviewSelect,setFillterOverviewSelect] = useState<string>('week');
    const [fillterProductActivitySelect,setFillterProductActivitySelect] = useState<string>('Last 2 weeks');
    const [fillterProductViewsSelect,setFillterProductViewsSelect] = useState<string>('Last 7 days');
    const [labelsProductViews,setLabelsProductViews] = useState<Array<string>>(['Mon', 'Tue', 'We', 'Th', 'Fr', 'Sa', 'Su']);
    const [selectedRow, setSelectedRow] = useState<any>();
    const [productsStatus,setProductsStatus] = useState<string>('Active');
    const [openModalStatus, setOpenModalStatus] = useState<boolean>(false);
    const listChartDummyData = [
        {
            icon: iconActivityFilled,
            iconChart: imageSmallLine1,
            title: 'Earning',
            quantity: `${_.random(10, 200)}k`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2)}%`,
            titleMode: `this ${fillterOverviewSelect.toLowerCase()}`,
            bg: COLORS.greenLight,
        },
        {
            icon: iconShoppingBag,
            iconChart: imageSmallLine2,
            title: 'Customer',
            quantity: `${_.random(100, 600)}`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2)}%`,
            titleMode: `this ${fillterOverviewSelect.toLowerCase()}`,
            bg: COLORS.blueLight,
        },
        {
            icon: iconPaymentLight,
            iconChart: imageSmallLine3,
            title: 'Payouts',
            quantity: `${_.random(10, 200)}k`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 2)}%`,
            titleMode: `this ${fillterOverviewSelect.toLowerCase()}`,
            bg: COLORS.violetLight,
        },
    ];

    const debouncedSearchProduct = _.debounce((value) => setSearchProduct(value), 500);
    
    const handleChangeTabActiveProduct = useCallback((index: number)=> {
        setTabActiveProduct(index);
    }, [tabActiveProduct]);
    
    const handleDeleteProducts = (selectedRows:any) =>{
        if(_.size(selectedRows) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Products deleted 🙌');
            const Products = _.filter(dataTableMarket, (item:any)=> {
                return selectedRows.indexOf(item) == -1;
            });  
            setDataTableMarket(Products);
         
        }else{
            customToastNotify('error','Products can\'t deleted 🙌');
        }  
    };

    const handleSelectOverview = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterOverviewSelect(value);
    };
    const handleSelectProductsStatus = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setProductsStatus(value);
    };
    const handleSelectProductActivity = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterProductActivitySelect(value);
    };
    const handleSelectProductViews = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterProductViewsSelect(value);
    };
    const handleSelectRows = (rows: any) => {
        console.log(rows);  
        setSelectedRow(rows);
    };
  
    const handleChangeStatusProducts = (selectedRow:any,status:string) =>{
        if(_.size(selectedRow) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Status change complete 🙌');
            const newMarket = _.map(dataTableMarket, (item:any)=> {
                if(selectedRow.indexOf(item)){
                    item.status=status;
                    return item;

                }else{
                    return item;
                }
            });  
            setDataTableMarket(newMarket);
        }else{
            customToastNotify('error','Status can\'t change 🙌');
        }  
    };
  
    const handleSearchProduct = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            debouncedSearchProduct(value);
        }
        else 
            setSearchProduct('');
    };

    const handleOpenModalStatus = () => {
        setOpenModalStatus(true);
    };

    const handleCloseModalStatus = () => {
        setOpenModalStatus(false);
    };

    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { products: { title: string | string[]; }; })=> {
                const title = item?.products?.title as string;
                return title?.toLowerCase()?.includes(search.trim().toLowerCase());
            })  : [];
        return result;
    };    

    useEffect(() => {
        setDataChartFaker(listChartDummyData);
    },[fillterOverviewSelect]);

    useEffect(() => {
        switch(fillterProductViewsSelect) {
            case 'Last 7 days': 
            // eslint-disable-next-line no-case-declarations
                setLabelsProductViews(['Mon', 'Tue', 'We', 'Th', 'Fr', 'Sa', 'Su']);
                break;
            case 'This month': 
            // eslint-disable-next-line no-case-declarations
                setLabelsProductViews(['1', '5', '11', '16', '20', '25', '30']);
                break;
            case 'All time': 
            // eslint-disable-next-line no-case-declarations
                setLabelsProductViews(['Dec ', 'Aug', 'Mar', 'Feb', 'Nov', 'Aug ', 'Oct']);
                break;
        }
    },[fillterProductViewsSelect]);

    useEffect(() => {
        setDataTableProductActivity(singleProductActivity);
    },[fillterProductActivitySelect]);

    useEffect(()=> {
        switch(tabActiveProduct) {
            case 0: 
                // eslint-disable-next-line no-case-declarations
                const findMarket = filterArrayData(dataTableMarketFaker, searchProduct);
                setDataTableMarket(findMarket);
                break;
            case 1: 
                // eslint-disable-next-line no-case-declarations
                const findTraffic = filterArrayData(dataTableMarketFaker, searchProduct);
                setDataTableMarket(findMarket);
                break;
            case 2: 
                // eslint-disable-next-line no-case-declarations
                const findViewers = filterArrayData(dataTableMarketFaker, searchProduct);
                setDataTableMarket(findMarket);
                break;
        }
    }, [searchProduct]);

    return (
        <DashboardComponent 
            dataTableMarket={dataTableMarket}
            dataChartFaker={dataChartFaker}
            dataTableProductActivity={dataTableProductActivity}
            handleChangeTabActiveProduct={handleChangeTabActiveProduct}
            handleChangeStatusProducts={handleChangeStatusProducts}
            handleSelectProductsStatus={handleSelectProductsStatus}
            handleDeleteProducts={handleDeleteProducts}
            handleSearchProduct={handleSearchProduct}
            handleSelectOverview={handleSelectOverview}
            handleSelectProductActivity={handleSelectProductActivity}
            handleSelectProductViews={handleSelectProductViews}
            handleSelectRows={handleSelectRows}
            handleOpenModalStatus={handleOpenModalStatus}
            handleCloseModalStatus={handleCloseModalStatus}
            searchProduct={searchProduct}
            selectedRow={selectedRow}
            fillterOverviewSelect={fillterOverviewSelect}
            fillterProductActivitySelect={fillterProductActivitySelect}
            fillterProductViewsSelect={fillterProductViewsSelect}
            labelsProductViews={labelsProductViews}
            openModalStatus={openModalStatus}
            productsStatus={productsStatus}
            tabActiveProduct={tabActiveProduct}
            title='Dashboard Page'
            titlePage={'Products dashboard'}/>
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);