interface IComment{
    image:string,
    name:string,
    comment:string,
    time:string,
}

interface IProduct{
    image:string,
    title:string,
    type:string,
}

export interface IProductComment{
    id: string,
    comment:IComment,
    product:IProduct
}