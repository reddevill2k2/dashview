import CommentsComponent from '@components/CommentsComponent';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import faker from '@faker-js/faker';
import _ from 'lodash';
import { COLORS } from '@constants/colors';
import { createFakerList,generateRandomListFromANumber } from '@utils/index';
import moment from 'moment';
import { imageProduct1, imageProduct10, imageProduct2, imageProduct3, imageProduct4, imageProduct5, imageProduct6, imageProduct7, imageProduct8, imageProduct9 } from '@constants/imageAssets';
import ScheduledComponent from '@components/ScheduledComponent';

const listImageProduct = [
    imageProduct1,
    imageProduct2,
    imageProduct3,
    imageProduct4,
    imageProduct5,
    imageProduct6,
    imageProduct7,
    imageProduct8,
    imageProduct9,
    imageProduct10,
];
const singleDataProduct = () =>{

    return{
        products:{ 
            title: faker.lorem.sentence(4),
            link: faker.internet.url(),
            previewsm:faker.helpers.arrayElement(listImageProduct),
        },
        price: `${_.round(_.random(0, 100, true), 0)}`,
        scheduledFor: `${moment(faker.date.past()).format('MMM D, YYYY')} at ${moment(faker.date.past()).format('h:mm A')}`,
    };
};
const ScheduledContainer = () => {
    const [searchProduct, setSearchProduct] = useState<string>('');
    const dataTableProductFaker =  useMemo(()=> createFakerList(singleDataProduct, 100), []);
    const [dataTableProducts, setDataTableProducts] = useState<any>(dataTableProductFaker);
    const handleSearchProduct = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            setSearchProduct(value);
        }
        else 
            setSearchProduct('');
    };
    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { products: { title: string | string[]; }; })=> {
                const title = item.products.title as string;
                return title?.toLowerCase()?.includes(search.trim().toLowerCase());
            })  : [];
        return result;
    };  
    useEffect(()=> {
        const findProducts = filterArrayData(dataTableProductFaker, searchProduct);
        setDataTableProducts(findProducts);
    }, [searchProduct]);
    return (
        <ScheduledComponent
            dataTableProducts={dataTableProducts}
            handleSearchProduct={handleSearchProduct}
            searchProduct={searchProduct}
            title='Sheduled Page'
            titlePage='Sheduled' />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ScheduledContainer);