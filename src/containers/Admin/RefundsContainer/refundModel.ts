export interface Product{
    title:string,
    description:string,
    image:string
}

export interface Status{
    status:boolean
}

export interface Date{
    date:string
}

export interface Customer{
    user_image:string,
    user_name:string
}

export interface Color{
    color:string
}

export interface ProductRefund{
    product:Product | undefined,
    status: Status | undefined,
    date: Date | undefined,
    customer: Customer | undefined,
    color:Color | undefined
}