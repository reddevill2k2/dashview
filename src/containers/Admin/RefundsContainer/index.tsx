import RefundsComponent from '@components/RefundsComponent';
import { COLORS } from '@constants/colors';
import { imageOverviewUser1, imageOverviewUser2, imageOverviewUser3, imageProduct1, imageProduct2, imageProduct3, imageProduct4, imageProduct5, imageProduct6 } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { createFakerList } from '@utils/';
import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { ProductRefund } from './refundModel';


const listColor = [COLORS.violetLight,COLORS.greenLight,COLORS.grey7,COLORS.grey4];

const listImageProduct = [
    imageProduct1,imageProduct2,imageProduct3,imageProduct4,imageProduct5,imageProduct6
];

const listImageUser = [
    imageOverviewUser1,imageOverviewUser2,imageOverviewUser3
];


const singleDataProductRefund = () =>{
    const productRefundObj : ProductRefund = {
        product: undefined,
        status: undefined,
        date: undefined,
        customer: undefined,
        color: undefined
    };

    productRefundObj.product = {
        title:faker.lorem.sentence(3),
        description:faker.lorem.sentence(2),
        image: faker.helpers.arrayElement(listImageProduct)
    };

    productRefundObj.status = {
        status:faker.datatype.boolean()
    };

    productRefundObj.date = {
        date:`${faker.datatype.number({
            'min': 1,
            'max': 29
        })} ${faker.date.month({ abbr: true })}`
    };

    productRefundObj.customer = {
        user_image:faker.helpers.arrayElement(listImageUser),
        user_name:faker.lorem.sentence(2)
    };

    productRefundObj.color = {color : faker.helpers.arrayElement(listColor)};
    return productRefundObj;

};


const RefundsContainer = () => {
    const dataTablePostFakerTabOpen:ProductRefund[] = useMemo(()=>createFakerList(singleDataProductRefund,100),[]);
    const dataTablePostFakerTabClose:ProductRefund[] = useMemo(()=>createFakerList(singleDataProductRefund,100),[]);
    const [currentTab ,setCurrentTab] = useState<number>(0);
    const [dataTable, setDataTable] = useState<ProductRefund[]>(dataTablePostFakerTabOpen);

    const onHandleChangeTab = (tabIndex: number) => {
        if(tabIndex === currentTab){
            return;
        }else{
            setDataTable(tabIndex === 0 ? dataTablePostFakerTabOpen : dataTablePostFakerTabClose);
            setCurrentTab(tabIndex === 0 ? 0 : 1);
        }
    };

    return (
        <RefundsComponent title='Refunds Page'
            dataTablePost = {dataTable}
            handleChangeTab = {onHandleChangeTab}
            currentTab = {currentTab}
            titlePage={'Refunds'} />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(RefundsContainer);