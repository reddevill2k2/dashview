import CustomerListComponent from '@components/CustomerListComponent';
import {
    messageNotification1,
    messageNotification2,
    messageNotification3,
    messageNotification4,
    messageNotification5
} from '@constants/imageAssets';
import { faker } from '@faker-js/faker';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { connect } from 'react-redux';
const listAvatar = [
    messageNotification1,
    messageNotification2,
    messageNotification3,
    messageNotification4,
    messageNotification5,
    messageNotification2,
    messageNotification1,
    messageNotification5,
    messageNotification3,
    messageNotification4,
];


const singleDataCustomer = ()=> {
    return {
        name: {
            fullName: faker.name.findName(),
            userName: `@${faker.internet.userName()}`,
            avatar: faker.helpers.arrayElement(listAvatar),
        },
        email: `${faker.internet.email()}`,
        purchase: `${_.random(1, 100, false)}`,
        lifetime: {
            quantity: `$${_.round(_.random(50, 500, true), 0)}`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 1).toFixed(1)}%`,  
        },
        comments: `${_.random(1, 100, false)}`,
        likes:`${_.random(1, 100, false)}`,
    };
};


const CustomerListContainer = () => {

    const [tabActiveCustomer, setTabActiveCustomer] = useState<number>(0);
    const [searchCustomer, setSearchCustomer] = useState<string>('');
    const [openModalFilter, setOpenModalFilter] = useState<boolean>(false);
    const [positionModal, setPositionModal] = useState<DOMRect>();
    const [progressBarPrice, setProgressBarPrice] = useState<number[]>([30, 50]);

    const refFilter = useRef<HTMLButtonElement>(null);

    useEffect(()=> {
        if(refFilter.current) {
            const positionButton = refFilter.current?.getBoundingClientRect();
            setPositionModal(positionButton);
        }
    }, [refFilter]);

    const dataTableActiveCustomersFaker =  useMemo(()=> createFakerList(singleDataCustomer, 100), []);
    const dataTableNewCustomersFaker =  useMemo(()=> createFakerList(singleDataCustomer, 100), []);


    const [dataTableActiveCustomers, setDataTableActiveCustomers] = useState<any>(dataTableActiveCustomersFaker);
    const [dataTableNewCustomers, setDataTableNewCustomers] = useState<any>(dataTableNewCustomersFaker);

    const handleChangeTabActiveCustomer = useCallback((index: number)=> {
        setTabActiveCustomer(index);
    }, [tabActiveCustomer]);

    const handleSearchCustomer = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            setSearchCustomer(value);
        }
        else 
            setSearchCustomer('');
    };

    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { name: { fullName: string | string[]; }; email: string | string[];})=> {
                const fullName = item?.name?.fullName as string;
                const email = item?.email as string;
                return fullName?.toLowerCase()?.includes(search.trim().toLowerCase()) 
                || email?.toLowerCase()?.includes(search.trim().toLowerCase());
            })  : [];
        return result;
    };    

    useEffect(()=> {
        switch(tabActiveCustomer) {
            case 0: 
                // eslint-disable-next-line no-case-declarations
                const findActiveCustomers = filterArrayData(dataTableActiveCustomersFaker, searchCustomer);
                setDataTableActiveCustomers(findActiveCustomers);
                break;
            case 1: 
                // eslint-disable-next-line no-case-declarations
                const findNewCustomers = filterArrayData(dataTableNewCustomersFaker, searchCustomer);
                setDataTableNewCustomers(findNewCustomers);
                break;

        }
    }, [searchCustomer]);

    const handleOpenModalFilter = () => {
        setOpenModalFilter(true);
    };

    const handleCloseModalFilter = () => {
        setOpenModalFilter(false);
    };

    const handleChangeProgressBarPrice = (event: Event, newValue: number | number[]) => {
        setProgressBarPrice(newValue as number[]);
    };

    const handleResetProgressBarPrice = () => {
        setProgressBarPrice([30, 50]);
    };

    return (
        <CustomerListComponent title='Customer List'
            tabActiveCustomer={tabActiveCustomer}
            handleChangeTabActiveCustomer={handleChangeTabActiveCustomer}
            dataTableActiveCustomers={dataTableActiveCustomers}
            dataTableNewCustomers={dataTableNewCustomers}
            handleSearchCustomer={handleSearchCustomer}
            searchCustomer={searchCustomer}
            titlePage='Customer List'
            openModalFilter={openModalFilter}
            handleOpenModalFilter={handleOpenModalFilter}
            handleCloseModalFilter={handleCloseModalFilter}
            refFilter={refFilter}
            positionModal={positionModal}
            handleChangeProgressBarPrice ={handleChangeProgressBarPrice}
            progressBarPrice={progressBarPrice}
            handleResetProgressBarPrice={handleResetProgressBarPrice}

        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerListContainer);