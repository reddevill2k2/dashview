import OverviewComponent from '@components/OverviewComponent';
import React from 'react';
import { connect } from 'react-redux';
import { imageOverviewUser1,imageOverviewUser2, imageOverviewUser3 } from '@constants/imageAssets';

const OverviewContainer = () => {
    const overviewUserDummyData = [
        {
            id: 1,
            url: imageOverviewUser1,
            name: 'Gladyce',
        },
        {
            id: 2,
            url: imageOverviewUser2,
            name: 'Elbel',
        },
        {
            id: 3,
            url: imageOverviewUser3,
            name: 'Joyce',
        },
    ];

    return (
        <OverviewComponent title='Overview Page'
            titlePage='Customers'
            overviewUserDummyData={overviewUserDummyData}/>
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(OverviewContainer);