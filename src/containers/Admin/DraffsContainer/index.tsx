import DraffsComponent from '@components/DraffsComponent';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import faker from '@faker-js/faker';
import _ from 'lodash';
import { COLORS } from '@constants/colors';
import { createFakerList,generateRandomListFromANumber } from '@utils/index';
import { imageProduct1, imageProduct10, imageProduct2, imageProduct3, imageProduct4, imageProduct5, imageProduct6, imageProduct7, imageProduct8, imageProduct9 } from '@constants/imageAssets';
import { v4 as uuidv4 } from 'uuid';
import { 
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9
} from '@constants/imageAssets';
import moment from 'moment';
import { customToastNotify } from '@helpers/ToastCustom';

const listImageProduct = [
    imageProduct1,
    imageProduct2,
    imageProduct3,
    imageProduct4,
    imageProduct5,
    imageProduct6,
    imageProduct7,
    imageProduct8,
    imageProduct9,
    imageProduct10,
];
const listImage = [
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9,
];

const singleDataProduct = () =>{
    return{
        id:uuidv4(),
        products:{ 
            title: faker.lorem.sentence(4),
            link: faker.internet.url(),
            previewsm:faker.helpers.arrayElement(listImageProduct),
            previewxl:faker.helpers.arrayElement(listImage),
        },
        price: `${_.round(_.random(0, 100, true), 0)}`,
        lastEdited: faker.date.past(),
    };
};
const DraffsContainer = () => {
    const dataTableProductFaker =  useMemo(()=> createFakerList(singleDataProduct, 100), []);
    const [dataTableProducts, setDataTableProducts] = useState<any>(dataTableProductFaker);
    const [tabActiveProduct, setTabActiveProduct] = useState<number>(0);
    const [isLoadingMore, setIsLoadingMore] = useState<boolean>(false);
    const [pagination, setPagination] = useState<number>(1);
    const [searchProduct, setSearchProduct] = useState<string>('');
    const [selectedRow, setSelectedRow] = useState<any>();
    const [dateSelected, setDateSelected] = useState<any>(new Date());
    const [selectedItem, setSelectedItem] = useState<any>();
    const debouncedSearchProduct = _.debounce((value) => setSearchProduct(value), 500);
    const [openModalRechedule, setOpenModalRechedule] = useState<boolean>(false);
    const [openModalSelectDate, setOpenModalSelectDate] = useState<boolean>(false);
    const [openModalSelectTime, setOpenModalSelectTime] = useState<boolean>(false);
    const handleChangeTabActiveProduct = useCallback((index: number)=> {
        setTabActiveProduct(index);
    }, [tabActiveProduct]);

    const handleSearchProduct = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            debouncedSearchProduct(value);
        }
        else 
            setSearchProduct('');
    };
    
    const handleOpenModalRechedule = () => {
        setOpenModalRechedule(true);
    };

    const handleCloseModalRechedule = () => {
        setOpenModalRechedule(false);
    };
    const handleOpenSelectDate = () => {
        setOpenModalSelectDate(true);
    };

    const handleCloseSelectDate = () => {
        setOpenModalSelectDate(false);
    };
    const handleOpenSelectTime = () => {
        setOpenModalSelectTime(true);
    };

    const handleCloseSelectTime = () => {
        setOpenModalSelectTime(false);
    };

    const handleChangeTabActive = (index: number)=> {
        tabActiveProduct(index);
        setPagination(1);
    };

    const handleChangePaginationProducts = async () => {
        await setIsLoadingMore(true);
        await setTimeout(()=> {
            setIsLoadingMore(false);
            setPagination((value)=> value + 1);
        }, 800);
    };
    const handleDeleteProducts = (selectedRows:any) =>{
        if(_.size(selectedRows) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Products deleted 🙌');
            const Products = _.remove(dataTableProducts, (item:any)=> {
                return selectedRows.indexOf(item) == -1;
            });  
            setDataTableProducts(Products);
           
        }else{
            customToastNotify('error','Products can\'t deleted 🙌');
        }  
    };


    const handlePublishProducts = (selectedRow:any) =>{
        if(_.size(selectedRow) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Products publish complete 🙌');
            const Products = _.remove(dataTableProducts, (item:any)=> {
                return selectedRow.indexOf(item) == -1;
            });  
            setDataTableProducts(Products);
        }else{
            customToastNotify('error','Products can\'t publish 🙌');
        }  
    };
  
    const handleSelectRows = (rows: any) => {
        setSelectedRow(rows);
    };
    const handleSetDate = (date: any) => {
        setDateSelected(date);
    };

    const handleSelectedItem = (item: any) => {
        setSelectedItem(item);
        handleSetDate(item.lastEdited);
    };

    const handlReschedule = () =>{
        if(selectedItem.lastEdited != dateSelected){
            const listData =_.map(dataTableProducts,(item: any) =>{
                if(item.id == selectedItem.id ){
                    item.lastEdited = dateSelected;
                    return item;
                }else{
                    return item;
                }
            });
            setDataTableProducts(listData);
            customToastNotify('success','Products change date complete 🙌');
        }
    };
    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { products: { title: string | string[]; }; })=> {
                const title = item?.products?.title as string;
                return title?.toLowerCase()?.includes(search.trim().toLowerCase());
            })  : [];
        return result;
    };    

    useEffect(()=> {
        switch(tabActiveProduct) {
            case 0: 
                // eslint-disable-next-line no-case-declarations
                const findProducts = filterArrayData(dataTableProductFaker, searchProduct);
                setDataTableProducts(findProducts);
                break;
            case 1: 
                // eslint-disable-next-line no-case-declarations
                const findProduct = filterArrayData(dataTableProductFaker , searchProduct);
                setDataTableProducts(findProduct);
                break;
        }
    }, [searchProduct]);
  
    return (
        <DraffsComponent title='Draffs Page'
            dataTableProducts={dataTableProducts}
            handleChangeTabActiveProduct={handleChangeTabActiveProduct}
            handleSearchProduct={handleSearchProduct}
            handleChangePaginationProducts={handleChangePaginationProducts}
            handleSelectRows={handleSelectRows}
            handleDeleteProducts={handleDeleteProducts}
            handlePublishProducts={handlePublishProducts}
            handleOpenModalRechedule={handleOpenModalRechedule}
            handleCloseModalRechedule={handleCloseModalRechedule}
            handleSelectedItem={handleSelectedItem}
            handleOpenSelectDate={handleOpenSelectDate}
            handleCloseSelectDate={handleCloseSelectDate}
            handleOpenSelectTime={handleOpenSelectTime}
            handleCloseSelectTime={handleCloseSelectTime}
            handleSetDate={handleSetDate}
            handlReschedule={handlReschedule}
            selectedRow={selectedRow}
            selectedItem={selectedItem}
            dateSelected={dateSelected}
            searchProduct={searchProduct}
            isLoadingMore={isLoadingMore}
            openModalRechedule={openModalRechedule}
            openModalSelectDate={openModalSelectDate}
            openModalSelectTime={openModalSelectTime}
            pagination={pagination}
            tabActiveProduct={tabActiveProduct}
            titlePage='Drafts' />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(DraffsContainer);