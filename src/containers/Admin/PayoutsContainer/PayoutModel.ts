export interface CurrentBalance{
    icon:string,
    title:string,
    price:string,
    bg:string,
}

export interface PayoutHistory{
    month:string,
    status:boolean,
    method:boolean,
    earning:string,
    amount:string,
    title:string
}