import PayoutsComponent from '@components/PayoutsComponent';
import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { iconActivityFilled, iconPieChar } from '@constants/imageAssets';
import { CurrentBalance, PayoutHistory } from './PayoutModel';
import { COLORS } from '@constants/colors';
import faker from '@faker-js/faker';
import { createFakerList } from '@utils/';


const currentBlanceList: CurrentBalance[] = [
    {
        icon:iconActivityFilled,
        title:'Current account blance',
        price:'128K',
        bg:COLORS.greenNgoc
    },
    {
        icon:iconPieChar,
        title:'Available for withdraw',
        price:'0.00',
        bg:COLORS.violetLight
    }
];

const PayoutHistoryData = ()=>{
    const currentPayoutHistory:PayoutHistory = {
        month: `${faker.date.month({ abbr:true })} ${faker.datatype.number({
            'min': 2015,
            'max': 2022
        })}`,
        status:faker.datatype.boolean(),
        method:faker.datatype.boolean(),
        earning: `$${faker.datatype.number({
            'min': 100000,
            'max': 999000
        }).toLocaleString(undefined,{ minimumFractionDigits : 2})}`,
        amount : `$${faker.datatype.number({
            'min': 100000,
            'max': 999000
        }).toLocaleString(undefined,{ minimumFractionDigits : 2})}`,
        title:'Payout history'
    };
    return currentPayoutHistory;
};

const PayoutsContainer = () => {
    const dataTableCurrentPayoutHistory:PayoutHistory[] = 
    useMemo(()=> createFakerList(PayoutHistoryData,100),[]);
    const [openModalPayouts, setOpenModalPayouts] = useState<boolean>(false);

    const handleOpenModalPayouts = () => {
        setOpenModalPayouts(true);
    };

    const handleCloseModalPayouts = () => {
        setOpenModalPayouts(false);
    };
    return (
        <PayoutsComponent title='Payouts Page'
            dataTableCurrentBalance={currentBlanceList}
            dataTableCurrentPayoutHistory={dataTableCurrentPayoutHistory}
            titlePage={'Payouts'}
            openModalPayouts={openModalPayouts}
            handleOpenModalPayouts={handleOpenModalPayouts}
            handleCloseModalPayouts={handleCloseModalPayouts}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps ,mapDispatchToProps)(PayoutsContainer);